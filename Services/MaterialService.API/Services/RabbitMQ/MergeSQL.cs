﻿using Master.Api.Helpers;
using MaterialService.API.Services.RabbitMQ.Subscriber.Oracle.Wip.Models;
using MaterialService.API.Services.RabbitMQ.Subscriber.Werum.FinishedGoodTransfer.Model;
using MaterialService.API.Services.RabbitMQ.Subscriber.Werum.MasterBox.Model;
using MaterialService.API.Services.RabbitMQ.Subscriber.Werum.SemiFinishedGoodTransfer.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace MaterialService.API.Services.RabbitMQ
{
    public class MergeSQL : IDisposable
    {

        public List<string> GetFieldType(Type type)
        {
            List<string> InsertFields = new List<string>();

            PropertyInfo[] propsFields = type.GetProperties();
            foreach (var propfields in propsFields)
            {
                var f = propfields.Name.Trim();

                if (propfields.PropertyType.Equals(typeof(decimal?)) || propfields.PropertyType.Equals(typeof(decimal)))
                {
                    InsertFields.Add("[" + f + "] decimal(18, 3)");
                    ErrorLog.WriteToLog("AppException", "[TipeData-decimal-not-null] - " + f);
                }
                //else
                if (propfields.PropertyType.Equals(typeof(long?)) || propfields.PropertyType.Equals(typeof(long)))
                {
                    InsertFields.Add("[" + f + "] [bigint]");
                    ErrorLog.WriteToLog("AppException", "[TipeData-bigint] - " + f);
                }
                //else
                if (propfields.PropertyType.Equals(typeof(Single?)) || propfields.PropertyType.Equals(typeof(Single)))
                {
                    InsertFields.Add("[" + f + "] [int]");
                    ErrorLog.WriteToLog("AppException", "[TipeData-single] - " + f);
                }
                // else
                if (propfields.PropertyType.Equals(typeof(int?)) || propfields.PropertyType.Equals(typeof(int)))
                {
                    InsertFields.Add("[" + f + "] [int]");
                    ErrorLog.WriteToLog("AppException", "[TipeData-int] - " + f);
                }
                // else

                if (propfields.PropertyType.Equals(typeof(string)))
                {
                    InsertFields.Add("[" + f + "] [varchar](max)");
                    ErrorLog.WriteToLog("AppException", "[TipeData-varchar] - " + f);
                }
                // else
                if (propfields.PropertyType.Equals(typeof(Nullable<DateTime>)))
                {
                    InsertFields.Add("[" + f + "] [datetime] ");
                    ErrorLog.WriteToLog("AppException", "[TipeData-datetime] - " + f);
                }
                if (propfields.PropertyType.Equals(typeof(bool?)) || propfields.PropertyType.Equals(typeof(bool)))
                {
                    InsertFields.Add("[" + f + "] [bit] ");
                    ErrorLog.WriteToLog("AppException", "[TipeData-bit] - " + f);
                }

                //if (propfields.PropertyType.Equals(typeof(List<WipMaterialIssueItemDetailRM>)))
                //{
                //    var n = "Status";
                //    InsertFields.Add("[" + n + "] [bit] ");
                //}

                // relation fields jika banyak model maka banyak if
                // EBridgingMasterMaterialAsociation, EBridgingMasterRoutingDetailStep, EbridgingMasterSpecificationTarget
                // EbridgingMasterSpecificationValRuleDetail, EbridgingMaterialSubtitutionDetail
                // EbridgingMasterOperationTroughtput, EbridginMasterOperationActivities
                //else
                //if (propfields.PropertyType.Equals(typeof(List<EbridgingRecipeStepMaterialAsociation>)))
                //{
                //    InsertFields.Add("[" + f + "] [varchar](max) ");
                //}
                //else
                //if (propfields.PropertyType.Equals(typeof(List<EBridgingMasterRoutingDetailStep>)))
                //{
                //    InsertFields.Add("[" + f + "] [varchar](max) ");
                //}
                //else
                //if (propfields.PropertyType.Equals(typeof(List<EbridgingMasterSpecificationTarget>)))
                //{
                //    InsertFields.Add("[" + f + "] [varchar](max) ");
                //}
                //else
                //if (propfields.PropertyType.Equals(typeof(List<EbridgingMasterSpecificationValRuleDetail>)))
                //{
                //    InsertFields.Add("[" + f + "] [varchar](max) ");
                //}
                //else
                //if (propfields.PropertyType.Equals(typeof(List<EbridgingMaterialSubtitutionDetail>)))
                //{
                //    InsertFields.Add("[" + f + "] [varchar](max) ");
                //}
                //else
                //if (propfields.PropertyType.Equals(typeof(List<EbridgingMasterOperationTroughtput>)))
                //{
                //    InsertFields.Add("[" + f + "] [varchar](max) ");
                //}
                //else
                //if (propfields.PropertyType.Equals(typeof(List<EbridgingMasterOperationActivities>)))
                //{
                //    InsertFields.Add("[" + f + "] [varchar](max) ");
                //}
                //else 
                //if (propfields.PropertyType.Equals(typeof(List<EbridgingRecipeStepMaterialAsociation>)))
                //{
                //    InsertFields.Add("[" + f + "] [varchar](max) ");
                //}
            }

            return InsertFields;
        }

        public List<string> GetFieldSQL(Type type)
        {
            List<string> Fields = new List<string>();

            PropertyInfo[] propsFields = type.GetProperties();
            foreach (var propfields in propsFields)
            {
                try
                {
                    var f = propfields.Name.Trim();

                    //if (propfields.PropertyType.Equals(typeof(decimal)))
                    //{
                    //    Fields.Add("[" + f + "]");
                    //    ErrorLog.WriteToLog("AppException", "[GetFieldSQ-decimal] - " + f);
                    //}
                    // else
                    if (propfields.PropertyType.Equals(typeof(decimal?)) || propfields.PropertyType.Equals(typeof(decimal)))
                    {
                        Fields.Add("[" + f + "]");
                        ErrorLog.WriteToLog("AppException", "[GetFieldSQ-decimal] - " + f);
                    }
                    else
                    if (propfields.PropertyType.Equals(typeof(Single?)) || propfields.PropertyType.Equals(typeof(Single)))
                    {
                        Fields.Add("[" + f + "]");
                        ErrorLog.WriteToLog("AppException", "[GetFieldSQ-single] - " + f);
                    }
                    else
                    if (propfields.PropertyType.Equals(typeof(long?)) || propfields.PropertyType.Equals(typeof(long)))
                    {
                        Fields.Add("[" + f + "]");
                        ErrorLog.WriteToLog("AppException", "[GetFieldSQ-long] - " + f);
                    }
                    else
                    if (propfields.PropertyType.Equals(typeof(int?)) || propfields.PropertyType.Equals(typeof(int)))
                    {
                        Fields.Add("[" + f + "]");
                        ErrorLog.WriteToLog("AppException", "[GetFieldSQ-int] - " + f);
                    }
                    else
                    if (propfields.PropertyType.Equals(typeof(string)))
                    {
                        Fields.Add("[" + f + "]");
                        ErrorLog.WriteToLog("AppException", "[GetFieldSQ-varchar] - " + f);
                    }
                    else
                    if (propfields.PropertyType.Equals(typeof(Nullable<DateTime>)))
                    {
                        Fields.Add("[" + f + "]");
                        ErrorLog.WriteToLog("AppException", "[GetFieldSQ-datetime] - " + f);
                    }
                    else
                    if (propfields.PropertyType.Equals(typeof(bool?)) || propfields.PropertyType.Equals(typeof(bool)))
                    {
                        Fields.Add("[" + f + "]");
                        ErrorLog.WriteToLog("AppException", "[GetFieldSQ-bool] - " + f);
                    }

                    //    //Fields.Add("[" + f + "]");
                    //}
                    //else
                    //if (propfields.PropertyType.Equals(typeof(List<EbridgingMasterSpecificationValRuleDetail>)))
                    //{
                    //    //Fields.Add("[" + f + "]");
                    //}
                    //else
                    //if (propfields.PropertyType.Equals(typeof(List<EbridgingMaterialSubtitutionDetail>)))
                    //{
                    //    //Fields.Add("[" + f + "]");
                    //}
                    //else
                    //if (propfields.PropertyType.Equals(typeof(List<EbridgingMasterOperationTroughtput>)))
                    //{
                    //    //Fields.Add("[" + f + "]");
                    //}
                    //else
                    //if (propfields.PropertyType.Equals(typeof(List<EbridgingMasterOperationActivities>)))
                    //{
                    //   // Fields.Add("[" + f + "]");
                    //}
                    //else
                    //if (propfields.PropertyType.Equals(typeof(List<EbridgingRecipeStepMaterialAsociation>)))
                    //{
                    //    //Fields.Add("[" + f + "]");
                    //}

                }
                catch (Exception ex)
                {
                    ErrorLog.WriteToLog("AppException", "[GetFieldSQL-Method] - " + ex.Message);
                }
            }

            return Fields;

        }

        public string GetNilai(PropertyInfo iprop, string item)
        {
            var result = "";
            try
            {
                var f = iprop.Name.Trim();
                ErrorLog.WriteToLog("AppException", "[GetNilai] - " + f);

                //if (iprop.PropertyType.Equals(typeof(decimal)))
                //{
                //    result = item;
                //}

                if (iprop.PropertyType.Equals(typeof(decimal?)) || iprop.PropertyType.Equals(typeof(decimal)))
                {
                    result = item;
                }
                //  else
                if (iprop.PropertyType.Equals(typeof(Single?)) || iprop.PropertyType.Equals(typeof(Single)))
                {
                    result = item;

                }
                //   else
                if (iprop.PropertyType.Equals(typeof(long?)) || iprop.PropertyType.Equals(typeof(long)))
                {
                    result = item;

                }
                //   else
                if (iprop.PropertyType.Equals(typeof(int?)) || iprop.PropertyType.Equals(typeof(int)))
                {
                    result = item;

                }
                //    else
                if (iprop.PropertyType.Equals(typeof(string)))
                {
                    string v = iprop.GetValue(item, null) as string;
                    if (string.IsNullOrEmpty(v))
                    {
                        result = item;
                    }
                    else
                    {
                        result = "'" + string.Empty + "'";
                    }
                    //if (iprop.GetValue(item).ToString().Length > 0)
                    //{
                    //    result = item;
                    //}
                    //else
                    //{
                    //    result = "'" + string.Empty + "'";
                    //}
                }
                //  else
                if (iprop.PropertyType.Equals(typeof(Nullable<DateTime>)))
                {
                    if (iprop.GetValue(item) != null)
                    {
                        result = item;
                    }
                    else
                    {
                        result = "null";
                    }

                }
                //   else
                if (((iprop.PropertyType.Equals(typeof(bool?))) || (iprop.PropertyType.Equals(typeof(bool)))))
                {
                    result = item.ToString();
                }
                //

            }
            catch (Exception ex)
            {
                ErrorLog.WriteToLog("AppException", "[GetNilai-Method] - " + ex.Message);
            }

            return result;
        }

        public string MergeRelasi(string NamaTableTemp, string TypeFields, string QueryInsert, string NamaTableTarget, string UpdateQuery, string InsertQuery)
        {
            var SQL = "  DECLARE " + NamaTableTemp + " " +
                      "  TABLE (" + TypeFields + " ) " +
                      " " + QueryInsert + " " +
                      "  MERGE INTO " + NamaTableTarget + " WITH (HOLDLOCK) AS TARGET " +
                      "  USING " + NamaTableTemp + " AS SOURCE " +
                      "	 ON SOURCE.RECID = TARGET.RecId " +
                      "  WHEN MATCHED THEN " +
                      "  " + UpdateQuery + " " +
                      "  WHEN NOT MATCHED BY TARGET THEN  " +
                      "  " + InsertQuery + ";";
            ErrorLog.WriteToLog("AppException", "[MergeRelasi] - " + SQL);
            return SQL;
        }

        public List<string> GetValueSQL<T>(ref T SourceModel, Type type, out string SQL, string[] KeyColumnName) where T : new()
        {
            var SQLMerge = "";

            List<string> ValueFields = new List<string>();
            List<string> ValueChilds = new List<string>();

            PropertyInfo[] propsFields = type.GetProperties();

            #region
            foreach (var prop in propsFields)
            {
                if (prop.PropertyType.Equals(typeof(decimal?)) || prop.PropertyType.Equals(typeof(decimal)))
                {
                    try
                    {
                        var key = prop.Name;
                        var val = prop.GetMethod.Invoke(SourceModel, null);
                        if (val != null)
                        {
                            ValueFields.Add("'" + prop.GetValue(SourceModel).ToString() + "'");
                        }
                        else ValueFields.Add("null");

                        //if (prop.GetValue(SourceModel) != null)
                        //{
                        //    ValueFields.Add("'" + prop.GetValue(SourceModel).ToString() + "'");
                        //}
                        //else
                        //{
                        //    ValueFields.Add("null");
                        //}
                        // ValueFields.Add(prop.GetValue(SourceModel).ToString());
                        //ErrorLog.WriteToLog("AppException", "[Decimal] - " + prop.Name);
                        //ErrorLog.WriteToLog("AppException", "[Decimal] - " + prop.Name + " val " + prop.GetValue(SourceModel).ToString());
                        //ValueFields.Add(prop.GetValue(SourceModel).ToString());

                        //string v = prop.GetValue(SourceModel, null) as string;
                        //if (string.IsNullOrEmpty(v))
                        //{
                        //    ValueFields.Add(null);

                        //}
                        //else
                        //{
                        //    ErrorLog.WriteToLog("AppException", "[decimal] - " + prop.Name + " val " + prop.GetValue(SourceModel).ToString());

                        //    ValueFields.Add(prop.GetValue(SourceModel).ToString());
                        //}
                    }
                    catch (Exception ex)
                    {
                        ErrorLog.WriteToLog("AppException", "[GetValueSQL-props-decimal] - " + ex.Message);
                    }
                }
                else
                if (prop.PropertyType.Equals(typeof(Single?)) || prop.PropertyType.Equals(typeof(Single)))
                {
                    try
                    {
                        var key = prop.Name;
                        var val = prop.GetMethod.Invoke(SourceModel, null);
                        if (val != null)
                        {
                            ValueFields.Add("'" + prop.GetValue(SourceModel).ToString() + "'");
                        }
                        else ValueFields.Add("null");
                        //ErrorLog.WriteToLog("AppException", "[Single] - " + prop.Name);
                        //ErrorLog.WriteToLog("AppException", "[single] - " + prop.Name + " val " + prop.GetValue(SourceModel).ToString());

                        //ValueFields.Add(prop.GetValue(SourceModel).ToString());
                        //string v = prop.GetValue(SourceModel, null) as string;
                        //if (string.IsNullOrEmpty(v))
                        //{
                        //    ValueFields.Add(null);

                        //}
                        //else
                        //{
                        //   // ErrorLog.WriteToLog("AppException", "[single] - " + prop.GetValue(SourceModel).ToString());
                        //    ErrorLog.WriteToLog("AppException", "[single] - " + prop.Name + " val " + prop.GetValue(SourceModel).ToString());

                        //    ValueFields.Add(prop.GetValue(SourceModel).ToString());
                        //}
                    }
                    catch (Exception ex)
                    {
                        ErrorLog.WriteToLog("AppException", "[GetValueSQL-props-single] - " + ex.Message);
                    }
                }
                else
                if (prop.PropertyType.Equals(typeof(long?)) || prop.PropertyType.Equals(typeof(long)))
                {
                    try
                    {
                        var key = prop.Name;
                        var val = prop.GetMethod.Invoke(SourceModel, null);
                        if (val != null)
                        {
                            ValueFields.Add("'" + prop.GetValue(SourceModel).ToString() + "'");
                        }
                        else ValueFields.Add("null");
                        //ErrorLog.WriteToLog("AppException", "[Long] - " + prop.Name);
                        //ErrorLog.WriteToLog("AppException", "[Long] - " + prop.Name + " val " + prop.GetValue(SourceModel).ToString());

                        //ValueFields.Add(prop.GetValue(SourceModel).ToString());


                        //string v = prop.GetValue(SourceModel, null) as string;
                        //if (string.IsNullOrEmpty(v))
                        //{
                        //    ValueFields.Add("null");
                        //    ErrorLog.WriteToLog("AppException", "[Long] - " + prop.Name + " val : null" );

                        //}
                        //else
                        //{
                        //    ErrorLog.WriteToLog("AppException", "[Long] - " + prop.Name + " val " + prop.GetValue(SourceModel).ToString());
                        //    ValueFields.Add(prop.GetValue(SourceModel).ToString());
                        //}
                    }
                    catch (Exception ex)
                    {
                        ErrorLog.WriteToLog("AppException", "[GetValueSQL-props-long] - " + ex.Message);
                    }
                }
                else
                if (prop.PropertyType.Equals(typeof(int?)) || prop.PropertyType.Equals(typeof(int)))
                {
                    try
                    {
                        var key = prop.Name;
                        var val = prop.GetMethod.Invoke(SourceModel, null);
                        if (val != null)
                        {
                            ValueFields.Add("'" + prop.GetValue(SourceModel).ToString() + "'");
                        }
                        else ValueFields.Add("null");

                        // ValueFields.Add(prop.GetValue(SourceModel).ToString());
                        //ErrorLog.WriteToLog("AppException", "[Int] - " + prop.Name);
                        //ErrorLog.WriteToLog("AppException", "[Int] - " + prop.Name + " val " + prop.GetValue(SourceModel).ToString());

                        //ValueFields.Add(prop.GetValue(SourceModel).ToString());

                        //string v = prop.GetValue(SourceModel, null) as string;
                        //Console.WriteLine(v);
                        //if (string.IsNullOrEmpty(v))
                        //{

                        //    ValueFields.Add(null);
                        //}
                        //else
                        //{
                        //    ErrorLog.WriteToLog("AppException", "[int] - " + prop.Name + " val " + prop.GetValue(SourceModel).ToString());

                        //    ValueFields.Add(prop.GetValue(SourceModel).ToString());
                        //}


                    }
                    catch (Exception ex)
                    {
                        ErrorLog.WriteToLog("AppException", "[GetValueSQL-props-int] - " + ex.Message.ToString());
                    }
                }
                else
                if (prop.PropertyType.Equals(typeof(string)))
                {
                    try
                    {
                        var key = prop.Name;
                        var val = prop.GetMethod.Invoke(SourceModel, null);
                        if (val != null)
                        {
                            ValueFields.Add("'" + prop.GetValue(SourceModel).ToString() + "'");
                        }
                        else ValueFields.Add("null");
                        //string v = prop.GetValue(SourceModel, null) as string;
                        //if (string.IsNullOrEmpty(v))
                        //{
                        //    ValueFields.Add("'" + string.Empty + "'");

                        //}
                        //else
                        //{
                        //    ValueFields.Add("'" + prop.GetValue(SourceModel).ToString() + "'");
                        //}
                    }
                    catch (Exception ex)
                    {
                        ErrorLog.WriteToLog("AppException", "[GetValueSQL-props-string] - " + ex.Message);
                    }
                }
                else
                if (prop.PropertyType.Equals(typeof(Nullable<DateTime>)))
                {
                    try
                    {
                        var key = prop.Name;
                        var val = prop.GetMethod.Invoke(SourceModel, null);
                        if (val != null)
                        {
                            ValueFields.Add("'" + prop.GetValue(SourceModel).ToString() + "'");
                        }
                        else ValueFields.Add("null");
                        //if (prop.GetValue(SourceModel) != null)
                        //{
                        //    ValueFields.Add("'" + prop.GetValue(SourceModel).ToString() + "'");
                        //}
                        //else
                        //{
                        //    ValueFields.Add("null");
                        //}
                    }
                    catch (Exception ex)
                    {
                        ErrorLog.WriteToLog("AppException", "[GetValueSQL-props-datetime] - " + ex.Message);
                    }
                }
                else
                if (prop.PropertyType.Equals(typeof(bool?)) || prop.PropertyType.Equals(typeof(bool)))
                {
                    try
                    {
                        //ValueFields.Add(prop.GetValue(SourceModel).ToString());
                        var key = prop.Name;
                        var val = prop.GetMethod.Invoke(SourceModel, null);
                        if (val != null)
                        {
                            ValueFields.Add("'" + prop.GetValue(SourceModel).ToString() + "'");
                        }
                        else ValueFields.Add("null");
                        //if (prop.GetValue(SourceModel) != null)
                        //{
                        //    ValueFields.Add(prop.GetValue(SourceModel).ToString());
                        //}
                        //else
                        //{
                        //    ValueFields.Add("null");
                        //}

                    }
                    catch (Exception ex)
                    {
                        ErrorLog.WriteToLog("AppException", "[GetValueSQL-props-bool] - " + ex.Message);
                    }
                }
                else
                if (prop.PropertyType.Equals(typeof(List<EbridgingFBppDetail>)))
                {
                    #region finish good -  BppDEtail
                    var key = "GetValueSQL- BppDEtail-props-";
                    //ValueFields.Add("'Relation model MaterialSubtitutionDetail'");
                    var NameTableTemp = "@TableBPPDetail";
                    var TargetTable = "tblW_BPP_Detail";

                    List<string> value = new List<string>();
                    List<string> InsertCommand = new List<string>();
                    List<string> InsertCommandMarge = new List<string>();

                    List<string> FieldsType = new List<string>();
                    List<string> Fields = new List<string>();

                    EbridgingFBppDetail m = new EbridgingFBppDetail();
                    Type _st = m.GetType();
                    FieldsType = GetFieldType(_st);
                    Fields = GetFieldSQL(_st);

                    var IsFields = "";
                    var IsValues = "";

                    IsFields = string.Join(",", Fields.ToArray());

                    var jsonString = DataChild(ref SourceModel, prop);
                    var Data = JsonConvert.DeserializeObject<List<EbridgingFBppDetail>>(jsonString);
                    if (Data != null)
                    {
                        foreach (var item in Data) // 10
                        {

                            Type i_Type = item.GetType();
                            IList<PropertyInfo> i_Props = new List<PropertyInfo>(i_Type.GetProperties());
                            var TotalFields = i_Props.Count;
                            var j = 1;
                            List<string> ItemInsertValues = new List<string>();

                            foreach (PropertyInfo iprop in i_Props)
                            {
                                // decinal, long, int, string, DateTime?, bool, objek
                                //  ErrorLog.WriteToLog("AppException", iprop.Name.ToString());
                                try
                                {
                                    //if (iprop.PropertyType.Equals(typeof(decimal?)) || iprop.PropertyType.Equals(typeof(decimal)))
                                    //{
                                    //    ItemInsertValues.Add(iprop.GetValue(item).ToString());
                                    //}
                                    //else
                                    //    if (iprop.PropertyType.Equals(typeof(Single?)))
                                    //{
                                    //    ItemInsertValues.Add(iprop.GetValue(item).ToString());
                                    //}
                                    //else
                                    //if (iprop.PropertyType.Equals(typeof(long?)))
                                    //{
                                    //    ItemInsertValues.Add(iprop.GetValue(item).ToString());
                                    //}
                                    //else
                                    //if (iprop.PropertyType.Equals(typeof(int?)))
                                    //{
                                    //    ItemInsertValues.Add(iprop.GetValue(item).ToString());
                                    //}
                                    //else
                                    //if (iprop.PropertyType.Equals(typeof(string)))
                                    //{
                                    //    try
                                    //    {
                                    //        string v = iprop.GetValue(item, null) as string;
                                    //        if (string.IsNullOrEmpty(v))
                                    //        {
                                    //            ItemInsertValues.Add("'" + string.Empty + "'");
                                    //        }
                                    //        else
                                    //        {
                                    //            ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");
                                    //        }
                                    //    }
                                    //    catch (Exception ex)
                                    //    {
                                    //        ErrorLog.WriteToLog("AppException", "[string] - " + ex.Message);
                                    //    }
                                    //}
                                    //else
                                    //if (iprop.PropertyType.Equals(typeof(Nullable<DateTime>)))
                                    //{
                                    //    if (iprop.GetValue(item) != null)
                                    //    {
                                    //        ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");
                                    //    }
                                    //    else
                                    //    {
                                    //        ItemInsertValues.Add("null");
                                    //    }
                                    //}
                                    //else
                                    //if (iprop.PropertyType.Equals(typeof(bool?)) || iprop.PropertyType.Equals(typeof(bool)))
                                    //{
                                    //    //ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");
                                    //    try
                                    //    {
                                    //        ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");

                                    //    }
                                    //    catch (Exception ex)
                                    //    {
                                    //        ErrorLog.WriteToLog("AppException", "[props] - " + ex.Message);
                                    //    }
                                    //}

                                    if (iprop.PropertyType.Equals(typeof(decimal?)) || iprop.PropertyType.Equals(typeof(decimal)))
                                    {
                                        try
                                        {
                                             
                                            var val = iprop.GetMethod.Invoke(SourceModel, null);
                                            if (val != null)
                                            {
                                                ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");
                                            }
                                            else ItemInsertValues.Add("null");

                                            //ItemInsertValues.Add(iprop.GetValue(item).ToString());
                                            //if (iprop.GetValue(item) != null)
                                            //{
                                            //    ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");
                                            //}
                                            //else
                                            //{
                                            //    ItemInsertValues.Add("null");
                                            //}
                                        }
                                        catch (Exception ex)
                                        {
                                            ErrorLog.WriteToLog("AppException", key + "-decimal] - " + ex.Message);
                                        }


                                    }


                                    // else
                                    if (iprop.PropertyType.Equals(typeof(Single?)) || iprop.PropertyType.Equals(typeof(Single)))
                                    {
                                        try
                                        {
                                             
                                            var val = iprop.GetMethod.Invoke(SourceModel, null);
                                            if (val != null)
                                            {
                                                ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");
                                            }
                                            else ItemInsertValues.Add("null");
                                            //ItemInsertValues.Add(iprop.GetValue(item).ToString());
                                            //if (iprop.GetValue(item) != null)
                                            //{
                                            //    ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");
                                            //}
                                            //else
                                            //{
                                            //    ItemInsertValues.Add("null");
                                            //}
                                        }
                                        catch (Exception ex)
                                        {
                                            ErrorLog.WriteToLog("AppException", key + "-single] - " + ex.Message);
                                        }

                                    }
                                    // else
                                    if (iprop.PropertyType.Equals(typeof(long?)) || iprop.PropertyType.Equals(typeof(long)))
                                    {
                                        try
                                        {
                                             
                                            var val = iprop.GetMethod.Invoke(SourceModel, null);
                                            if (val != null)
                                            {
                                                ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");
                                            }
                                            else ItemInsertValues.Add("null");
                                            //ItemInsertValues.Add(iprop.GetValue(item).ToString());
                                            //if (iprop.GetValue(item) != null)
                                            //{
                                            //    ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");
                                            //}
                                            //else
                                            //{
                                            //    ItemInsertValues.Add("null");
                                            //}
                                        }
                                        catch (Exception ex)
                                        {
                                            ErrorLog.WriteToLog("AppException", key + "-long] - " + ex.Message);
                                        }

                                    }
                                    //  else
                                    if (iprop.PropertyType.Equals(typeof(int?)) || iprop.PropertyType.Equals(typeof(int)))
                                    {
                                        try
                                        {
                                              
                                            var val = iprop.GetMethod.Invoke(SourceModel, null);
                                            if (val != null)
                                            {
                                                ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");
                                            }
                                            else ItemInsertValues.Add("null");
                                            //ItemInsertValues.Add(iprop.GetValue(item).ToString());
                                            //if (iprop.GetValue(item) != null)
                                            //{
                                            //    ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");
                                            //}
                                            //else
                                            //{
                                            //    ItemInsertValues.Add("null");
                                            //}
                                        }
                                        catch (Exception ex)
                                        {
                                            ErrorLog.WriteToLog("AppException", key + "-int] - " + ex.Message);
                                        }

                                    }
                                    //   else
                                    if (iprop.PropertyType.Equals(typeof(string)))
                                    {
                                        try
                                        {
                                            //string v = iprop.GetValue(item, null) as string;
                                            //if (string.IsNullOrEmpty(v))
                                            //{
                                            //    // ItemInsertValues.Add(v);
                                            //    ItemInsertValues.Add("'" + string.Empty + "'");
                                            //}
                                            //else
                                            //{
                                            //    ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");
                                            //}
                                             
                                            var val = iprop.GetMethod.Invoke(SourceModel, null);
                                            if (val != null)
                                            {
                                                ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");
                                            }
                                            else ItemInsertValues.Add("null");
                                        }
                                        catch (Exception ex)
                                        {
                                            ErrorLog.WriteToLog("AppException", key + "-single] - " + ex.Message);
                                        }
                                    }
                                    //  else
                                    if (iprop.PropertyType.Equals(typeof(Nullable<DateTime>)))
                                    {
                                        try
                                        {
                                            //if (iprop.GetValue(item) != null)
                                            //{
                                            //    ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");
                                            //}
                                            //else
                                            //{
                                            //    ItemInsertValues.Add("null");
                                            //}
                                             
                                            var val = iprop.GetMethod.Invoke(SourceModel, null);
                                            if (val != null)
                                            {
                                                ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");
                                            }
                                            else ItemInsertValues.Add("null");
                                        }
                                        catch (Exception ex)
                                        {
                                            ErrorLog.WriteToLog("AppException", key + "-datetime] - " + ex.Message);
                                        }
                                    }
                                    //  else
                                    if (((iprop.PropertyType.Equals(typeof(bool?))) || (iprop.PropertyType.Equals(typeof(bool)))))
                                    {
                                        //ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");

                                        try
                                        {
                                            //ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");
                                            //if (iprop.GetValue(SourceModel) != null)
                                            //{
                                            //    ValueFields.Add(prop.GetValue(SourceModel).ToString());
                                            //}
                                            //else
                                            //{
                                            //    ValueFields.Add("null");
                                            //}
                                             
                                            var val = iprop.GetMethod.Invoke(SourceModel, null);
                                            if (val != null)
                                            {
                                                ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");
                                            }
                                            else ItemInsertValues.Add("null");

                                        }
                                        catch (Exception ex)
                                        {
                                            ErrorLog.WriteToLog("AppException", key + "-bool] - " + ex.Message);
                                        }
                                    }


                                    if (j == TotalFields)
                                    {
                                        // var RealtionDeclareTable = "test";
                                        IsValues = string.Join(",", ItemInsertValues.ToArray());
                                        ErrorLog.WriteToLog("AppException", "99-insertVAleu" + InsertCommand.ToString());

                                        InsertCommand.Add(" INSERT INTO " + NameTableTemp + " (" + IsFields + ") VALUES ( " + IsValues + " ); ");
                                        ErrorLog.WriteToLog("AppException", "99-" + InsertCommand.ToString());

                                        InsertCommandMarge.Add(" INSERT (" + IsFields + ") VALUES ( " + IsValues + " ) ");
                                        ErrorLog.WriteToLog("AppException", "999-" + InsertCommand.ToString());
                                    }
                                    j++;
                                }
                                catch (Exception ex)
                                {
                                    ErrorLog.WriteToLog("AppException", "[MergeRelasi] - " + ex.Message);
                                }

                            }

                        }
                    }
                    // Console.WriteLine(InsertCommandMarge);
                    List<string> NFields = new List<string>();
                    FBppDetail l = new FBppDetail();
                    Type _lt = l.GetType();
                    NFields = GetFieldSQL(_lt);

                    var QueryCommandInsert = string.Join("", InsertCommand.ToArray());
                    ErrorLog.WriteToLog("AppException", QueryCommandInsert);
                    var InsertQuery = " INSERT (" + FormatMergeUpdateInsert(Fields, NFields, "") + ") VALUES (" + FormatMergeUpdateInsert(Fields, NFields, "INSERT") + " ) ";
                    var UpdateQuery = FormatMergeUpdateInsert(Fields, NFields, "UPDATE");

                    SQLMerge = "  DECLARE " + NameTableTemp + " " +
                                    "  TABLE (" + string.Join(",", FieldsType.ToArray()) + " ) " +
                                    " " + QueryCommandInsert + " " +

                                    "  MERGE INTO " + TargetTable + " WITH (HOLDLOCK) AS TARGET " +
                                    "  USING " + NameTableTemp + " AS SOURCE " +
                                    "	 ON SOURCE.SUBS_ID  = TARGET.SubsId " +
                                    "	 AND SOURCE.CODE = TARGET.Code " +
                                    "  WHEN MATCHED THEN " +
                                    "   UPDATE SET " + UpdateQuery + " " +
                                    "  WHEN NOT MATCHED BY TARGET THEN  " +
                                    "  " + InsertQuery + ";";
                    // "  SET IDENTITY_INSERT " + TargetTable + " OFF;";
                    ErrorLog.WriteToLog("AppException", "[4] - " + SQLMerge);
                    // Console.WriteLine(SQL);
                    #endregion
                }
                else
                if (prop.PropertyType.Equals(typeof(List<EbridgingBppDetail>)))
                {
                    #region semi finish good - BppDEtail
                    var key = "GetValueSQL- BppDEtail-props-";
                    //ValueFields.Add("'Relation model MaterialSubtitutionDetail'");
                    var NameTableTemp = "@TableBPPDetail";
                    var TargetTable = "tblW_BPP_Detail";

                    List<string> value = new List<string>();
                    List<string> InsertCommand = new List<string>();
                    List<string> InsertCommandMarge = new List<string>();

                    List<string> FieldsType = new List<string>();
                    List<string> Fields = new List<string>();

                    EbridgingFBppDetail m = new EbridgingFBppDetail();
                    Type _st = m.GetType();
                    FieldsType = GetFieldType(_st);
                    Fields = GetFieldSQL(_st);

                    var IsFields = "";
                    var IsValues = "";

                    IsFields = string.Join(",", Fields.ToArray());

                    var jsonString = DataChild(ref SourceModel, prop);
                    var Data = JsonConvert.DeserializeObject<List<EbridgingBppDetail>>(jsonString);
                    if (Data != null)
                    {
                        foreach (var item in Data) // 10
                        {

                            Type i_Type = item.GetType();
                            IList<PropertyInfo> i_Props = new List<PropertyInfo>(i_Type.GetProperties());
                            var TotalFields = i_Props.Count;
                            var j = 1;
                            List<string> ItemInsertValues = new List<string>();

                            foreach (PropertyInfo iprop in i_Props)
                            {
                                // decinal, long, int, string, DateTime?, bool, objek
                                //  ErrorLog.WriteToLog("AppException", iprop.Name.ToString());
                                try
                                {
                                    //if (iprop.PropertyType.Equals(typeof(decimal?)) || iprop.PropertyType.Equals(typeof(decimal)))
                                    //{
                                    //    ItemInsertValues.Add(iprop.GetValue(item).ToString());
                                    //}
                                    //else
                                    //    if (iprop.PropertyType.Equals(typeof(Single?)))
                                    //{
                                    //    ItemInsertValues.Add(iprop.GetValue(item).ToString());
                                    //}
                                    //else
                                    //if (iprop.PropertyType.Equals(typeof(long?)))
                                    //{
                                    //    ItemInsertValues.Add(iprop.GetValue(item).ToString());
                                    //}
                                    //else
                                    //if (iprop.PropertyType.Equals(typeof(int?)))
                                    //{
                                    //    ItemInsertValues.Add(iprop.GetValue(item).ToString());
                                    //}
                                    //else
                                    //if (iprop.PropertyType.Equals(typeof(string)))
                                    //{
                                    //    try
                                    //    {
                                    //        string v = iprop.GetValue(item, null) as string;
                                    //        if (string.IsNullOrEmpty(v))
                                    //        {
                                    //            ItemInsertValues.Add("'" + string.Empty + "'");
                                    //        }
                                    //        else
                                    //        {
                                    //            ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");
                                    //        }
                                    //    }
                                    //    catch (Exception ex)
                                    //    {
                                    //        ErrorLog.WriteToLog("AppException", "[string] - " + ex.Message);
                                    //    }
                                    //}
                                    //else
                                    //if (iprop.PropertyType.Equals(typeof(Nullable<DateTime>)))
                                    //{
                                    //    if (iprop.GetValue(item) != null)
                                    //    {
                                    //        ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");
                                    //    }
                                    //    else
                                    //    {
                                    //        ItemInsertValues.Add("null");
                                    //    }
                                    //}
                                    //else
                                    //if (iprop.PropertyType.Equals(typeof(bool?)) || iprop.PropertyType.Equals(typeof(bool)))
                                    //{
                                    //    //ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");
                                    //    try
                                    //    {
                                    //        ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");

                                    //    }
                                    //    catch (Exception ex)
                                    //    {
                                    //        ErrorLog.WriteToLog("AppException", "[props] - " + ex.Message);
                                    //    }
                                    //}

                                    if (iprop.PropertyType.Equals(typeof(decimal?)) || iprop.PropertyType.Equals(typeof(decimal)))
                                    {
                                        try
                                        {
                                            //ItemInsertValues.Add(iprop.GetValue(item).ToString());
                                            //if (iprop.GetValue(item) != null)
                                            //{
                                            //    ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");
                                            //}
                                            //else
                                            //{
                                            //    ItemInsertValues.Add("null");
                                            //}
                                             
                                            var val = iprop.GetMethod.Invoke(SourceModel, null);
                                            if (val != null)
                                            {
                                                ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");
                                            }
                                            else ItemInsertValues.Add("null");
                                        }
                                        catch (Exception ex)
                                        {
                                            ErrorLog.WriteToLog("AppException", key + "-bool] - " + ex.Message);
                                        }

                                        //string v = prop.GetValue(SourceModel, null) as string;
                                        //if (string.IsNullOrEmpty(v))
                                        //{
                                        //    ItemInsertValues.Add(null);
                                        //}
                                        //else
                                        //{
                                        //    ItemInsertValues.Add(iprop.GetValue(item).ToString());
                                        //}

                                    }


                                    // else
                                    if (iprop.PropertyType.Equals(typeof(Single?)) || iprop.PropertyType.Equals(typeof(Single)))
                                    {
                                        try
                                        {
                                            //ItemInsertValues.Add(iprop.GetValue(item).ToString());
                                            //if (iprop.GetValue(item) != null)
                                            //{
                                            //    ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");
                                            //}
                                            //else
                                            //{
                                            //    ItemInsertValues.Add("null");
                                            //}
                                             
                                            var val = iprop.GetMethod.Invoke(SourceModel, null);
                                            if (val != null)
                                            {
                                                ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");
                                            }
                                            else ItemInsertValues.Add("null");
                                        }
                                        catch (Exception ex)
                                        {
                                            ErrorLog.WriteToLog("AppException", key + "-single] - " + ex.Message);
                                        }
                                        //string v = prop.GetValue(SourceModel, null) as string;
                                        //if (string.IsNullOrEmpty(v))
                                        //{
                                        //    ItemInsertValues.Add(null);
                                        //}
                                        //else
                                        //{
                                        //    ItemInsertValues.Add(iprop.GetValue(item).ToString());
                                        //}
                                    }
                                    // else
                                    if (iprop.PropertyType.Equals(typeof(long?)) || iprop.PropertyType.Equals(typeof(long)))
                                    {
                                        try
                                        {
                                            //ItemInsertValues.Add(iprop.GetValue(item).ToString());
                                            //if (iprop.GetValue(item) != null)
                                            //{
                                            //    ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");
                                            //}
                                            //else
                                            //{
                                            //    ItemInsertValues.Add("null");
                                            //}
                                             
                                            var val = iprop.GetMethod.Invoke(SourceModel, null);
                                            if (val != null)
                                            {
                                                ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");
                                            }
                                            else ItemInsertValues.Add("null");
                                        }
                                        catch (Exception ex)
                                        {
                                            ErrorLog.WriteToLog("AppException", key + "-long] - " + ex.Message);
                                        }
                                        //string v = prop.GetValue(SourceModel, null) as string;
                                        //if (string.IsNullOrEmpty(v))
                                        //{
                                        //    ItemInsertValues.Add(null);
                                        //}
                                        //else
                                        //{
                                        //    ItemInsertValues.Add(iprop.GetValue(item).ToString());
                                        //}
                                    }
                                    //  else
                                    if (iprop.PropertyType.Equals(typeof(int?)) || iprop.PropertyType.Equals(typeof(int)))
                                    {
                                        try
                                        {
                                            //ItemInsertValues.Add(iprop.GetValue(item).ToString());
                                            //if (iprop.GetValue(item) != null)
                                            //{
                                            //    ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");
                                            //}
                                            //else
                                            //{
                                            //    ItemInsertValues.Add("null");
                                            //}
                                            // 
                                            //var val = iprop.GetMethod.Invoke(SourceModel, null);
                                            //if (val != null)
                                            //{
                                            //    ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");
                                            //}
                                            //else ItemInsertValues.Add("null");
                                        }
                                        catch (Exception ex)
                                        {
                                            ErrorLog.WriteToLog("AppException", key + "-int] - " + ex.Message);
                                        }
                                        //string v = prop.GetValue(SourceModel, null) as string;
                                        //if (string.IsNullOrEmpty(v))
                                        //{
                                        //    ItemInsertValues.Add(null);
                                        //}
                                        //else
                                        //{
                                        //    ItemInsertValues.Add(iprop.GetValue(item).ToString());
                                        //}
                                    }
                                    //   else
                                    if (iprop.PropertyType.Equals(typeof(string)))
                                    {
                                        try
                                        {
                                            //string v = iprop.GetValue(item, null) as string;
                                            //if (string.IsNullOrEmpty(v))
                                            //{
                                            //    // ItemInsertValues.Add(v);
                                            //    ItemInsertValues.Add("'" + string.Empty + "'");
                                            //}
                                            //else
                                            //{
                                            //    ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");
                                            //}
                                             
                                            var val = iprop.GetMethod.Invoke(SourceModel, null);
                                            if (val != null)
                                            {
                                                ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");
                                            }
                                            else ItemInsertValues.Add("null");
                                        }
                                        catch (Exception ex)
                                        {
                                            ErrorLog.WriteToLog("AppException", key + "-single] - " + ex.Message);
                                        }
                                    }
                                    //  else
                                    if (iprop.PropertyType.Equals(typeof(Nullable<DateTime>)))
                                    {
                                        try
                                        {
                                            //if (iprop.GetValue(item) != null)
                                            //{
                                            //    ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");
                                            //}
                                            //else
                                            //{
                                            //    ItemInsertValues.Add("null");
                                            //}
                                             
                                            var val = iprop.GetMethod.Invoke(SourceModel, null);
                                            if (val != null)
                                            {
                                                ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");
                                            }
                                            else ItemInsertValues.Add("null");
                                        }
                                        catch (Exception ex)
                                        {
                                            ErrorLog.WriteToLog("AppException", key + "-datetime] - " + ex.Message);
                                        }
                                    }
                                    //  else
                                    if (((iprop.PropertyType.Equals(typeof(bool?))) || (iprop.PropertyType.Equals(typeof(bool)))))
                                    {
                                        //ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");

                                        try
                                        {
                                            //ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");
                                            //if (iprop.GetValue(SourceModel) != null)
                                            //{
                                            //    ValueFields.Add(prop.GetValue(SourceModel).ToString());
                                            //}
                                            //else
                                            //{
                                            //    ValueFields.Add("null");
                                            //}
                                             
                                            var val = iprop.GetMethod.Invoke(SourceModel, null);
                                            if (val != null)
                                            {
                                                ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");
                                            }
                                            else ItemInsertValues.Add("null");

                                        }
                                        catch (Exception ex)
                                        {
                                            ErrorLog.WriteToLog("AppException", key + "-bool] - " + ex.Message);
                                        }
                                    }


                                    if (j == TotalFields)
                                    {
                                        // var RealtionDeclareTable = "test";
                                        IsValues = string.Join(",", ItemInsertValues.ToArray());
                                        ErrorLog.WriteToLog("AppException", "99-insertVAleu" + InsertCommand.ToString());

                                        InsertCommand.Add(" INSERT INTO " + NameTableTemp + " (" + IsFields + ") VALUES ( " + IsValues + " ); ");
                                        ErrorLog.WriteToLog("AppException", "99-" + InsertCommand.ToString());

                                        InsertCommandMarge.Add(" INSERT (" + IsFields + ") VALUES ( " + IsValues + " ) ");
                                        ErrorLog.WriteToLog("AppException", "999-" + InsertCommand.ToString());
                                    }
                                    j++;
                                }
                                catch (Exception ex)
                                {
                                    ErrorLog.WriteToLog("AppException", "[MergeRelasi] - " + ex.Message);
                                }

                            }

                        }
                    }
                    // Console.WriteLine(InsertCommandMarge);
                    List<string> NFields = new List<string>();
                    BppDetail l = new BppDetail();
                    Type _lt = l.GetType();
                    NFields = GetFieldSQL(_lt);

                    var QueryCommandInsert = string.Join("", InsertCommand.ToArray());
                    ErrorLog.WriteToLog("AppException", QueryCommandInsert);
                    var InsertQuery = " INSERT (" + FormatMergeUpdateInsert(Fields, NFields, "") + ") VALUES (" + FormatMergeUpdateInsert(Fields, NFields, "INSERT") + " ) ";
                    var UpdateQuery = FormatMergeUpdateInsert(Fields, NFields, "UPDATE");

                    SQLMerge = "  DECLARE " + NameTableTemp + " " +
                                    "  TABLE (" + string.Join(",", FieldsType.ToArray()) + " ) " +
                                    " " + QueryCommandInsert + " " +

                                    "  MERGE INTO " + TargetTable + " WITH (HOLDLOCK) AS TARGET " +
                                    "  USING " + NameTableTemp + " AS SOURCE " +
                                    "	 ON SOURCE.SUBS_ID  = TARGET.SubsId " +
                                    "	 AND SOURCE.CODE = TARGET.Code " +
                                    "  WHEN MATCHED THEN " +
                                    "   UPDATE SET " + UpdateQuery + " " +
                                    "  WHEN NOT MATCHED BY TARGET THEN  " +
                                    "  " + InsertQuery + ";";
                    // "  SET IDENTITY_INSERT " + TargetTable + " OFF;";
                    ErrorLog.WriteToLog("AppException", "[4] - " + SQLMerge);
                    // Console.WriteLine(SQL);
                    #endregion
                }
                else
                if (prop.PropertyType.Equals(typeof(List<FinishedGoodsCheckWeighingIngredient>)))
                {
                    #region BppDEtail
                    var key = "GetValueSQL- BppDEtail-props-";
                    //ValueFields.Add("'Relation model MaterialSubtitutionDetail'");
                    var NameTableTemp = "@TableFinishedGoodsCheckWeighingIngredient";
                    var TargetTable = "tblW_FinishedGoodsCheckWeighing_Ingredient";

                    List<string> value = new List<string>();
                    List<string> InsertCommand = new List<string>();
                    List<string> InsertCommandMarge = new List<string>();

                    List<string> FieldsType = new List<string>();
                    List<string> Fields = new List<string>();

                    EbridgingFBppDetail m = new EbridgingFBppDetail();
                    Type _st = m.GetType();
                    FieldsType = GetFieldType(_st);
                    Fields = GetFieldSQL(_st);

                    var IsFields = "";
                    var IsValues = "";

                    IsFields = string.Join(",", Fields.ToArray());
                    var jsonString = DataChild(ref SourceModel, prop);

                    var Data = JsonConvert.DeserializeObject<List<EbridgingFinishedGoodsCheckWeighingIngredient>>(jsonString);
                    if (Data != null)
                    {
                        foreach (var item in Data) // 10
                        {

                            Type i_Type = item.GetType();
                            IList<PropertyInfo> i_Props = new List<PropertyInfo>(i_Type.GetProperties());
                            var TotalFields = i_Props.Count;
                            var j = 1;
                            List<string> ItemInsertValues = new List<string>();

                            foreach (PropertyInfo iprop in i_Props)
                            {
                                // decinal, long, int, string, DateTime?, bool, objek
                                //  ErrorLog.WriteToLog("AppException", iprop.Name.ToString());
                                try
                                {
                                    //if (iprop.PropertyType.Equals(typeof(decimal?)) || iprop.PropertyType.Equals(typeof(decimal)))
                                    //{
                                    //    ItemInsertValues.Add(iprop.GetValue(item).ToString());
                                    //}
                                    //else
                                    //    if (iprop.PropertyType.Equals(typeof(Single?)))
                                    //{
                                    //    ItemInsertValues.Add(iprop.GetValue(item).ToString());
                                    //}
                                    //else
                                    //if (iprop.PropertyType.Equals(typeof(long?)))
                                    //{
                                    //    ItemInsertValues.Add(iprop.GetValue(item).ToString());
                                    //}
                                    //else
                                    //if (iprop.PropertyType.Equals(typeof(int?)))
                                    //{
                                    //    ItemInsertValues.Add(iprop.GetValue(item).ToString());
                                    //}
                                    //else
                                    //if (iprop.PropertyType.Equals(typeof(string)))
                                    //{
                                    //    try
                                    //    {
                                    //        string v = iprop.GetValue(item, null) as string;
                                    //        if (string.IsNullOrEmpty(v))
                                    //        {
                                    //            ItemInsertValues.Add("'" + string.Empty + "'");
                                    //        }
                                    //        else
                                    //        {
                                    //            ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");
                                    //        }
                                    //    }
                                    //    catch (Exception ex)
                                    //    {
                                    //        ErrorLog.WriteToLog("AppException", "[string] - " + ex.Message);
                                    //    }
                                    //}
                                    //else
                                    //if (iprop.PropertyType.Equals(typeof(Nullable<DateTime>)))
                                    //{
                                    //    if (iprop.GetValue(item) != null)
                                    //    {
                                    //        ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");
                                    //    }
                                    //    else
                                    //    {
                                    //        ItemInsertValues.Add("null");
                                    //    }
                                    //}
                                    //else
                                    //if (iprop.PropertyType.Equals(typeof(bool?)) || iprop.PropertyType.Equals(typeof(bool)))
                                    //{
                                    //    //ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");
                                    //    try
                                    //    {
                                    //        ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");

                                    //    }
                                    //    catch (Exception ex)
                                    //    {
                                    //        ErrorLog.WriteToLog("AppException", "[props] - " + ex.Message);
                                    //    }
                                    //}

                                    if (iprop.PropertyType.Equals(typeof(decimal?)) || iprop.PropertyType.Equals(typeof(decimal)))
                                    {
                                        try
                                        {
                                            //ItemInsertValues.Add(iprop.GetValue(item).ToString());
                                            //if (iprop.GetValue(item) != null)
                                            //{
                                            //    ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");
                                            //}
                                            //else
                                            //{
                                            //    ItemInsertValues.Add("null");
                                            //}
                                             
                                            var val = iprop.GetMethod.Invoke(SourceModel, null);
                                            if (val != null)
                                            {
                                                ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");
                                            }
                                            else ItemInsertValues.Add("null");
                                        }
                                        catch (Exception ex)
                                        {
                                            ErrorLog.WriteToLog("AppException", key + "-bool] - " + ex.Message);
                                        }

                                        //string v = prop.GetValue(SourceModel, null) as string;
                                        //if (string.IsNullOrEmpty(v))
                                        //{
                                        //    ItemInsertValues.Add(null);
                                        //}
                                        //else
                                        //{
                                        //    ItemInsertValues.Add(iprop.GetValue(item).ToString());
                                        //}

                                    }


                                    // else
                                    if (iprop.PropertyType.Equals(typeof(Single?)) || iprop.PropertyType.Equals(typeof(Single)))
                                    {
                                        try
                                        {
                                            //ItemInsertValues.Add(iprop.GetValue(item).ToString());
                                            //if (iprop.GetValue(item) != null)
                                            //{
                                            //    ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");
                                            //}
                                            //else
                                            //{
                                            //    ItemInsertValues.Add("null");
                                            //}
                                             
                                            var val = iprop.GetMethod.Invoke(SourceModel, null);
                                            if (val != null)
                                            {
                                                ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");
                                            }
                                            else ItemInsertValues.Add("null");
                                        }
                                        catch (Exception ex)
                                        {
                                            ErrorLog.WriteToLog("AppException", key + "-single] - " + ex.Message);
                                        }
                                        //string v = prop.GetValue(SourceModel, null) as string;
                                        //if (string.IsNullOrEmpty(v))
                                        //{
                                        //    ItemInsertValues.Add(null);
                                        //}
                                        //else
                                        //{
                                        //    ItemInsertValues.Add(iprop.GetValue(item).ToString());
                                        //}
                                    }
                                    // else
                                    if (iprop.PropertyType.Equals(typeof(long?)) || iprop.PropertyType.Equals(typeof(long)))
                                    {
                                        try
                                        {
                                            //ItemInsertValues.Add(iprop.GetValue(item).ToString());
                                            //if (iprop.GetValue(item) != null)
                                            //{
                                            //    ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");
                                            //}
                                            //else
                                            //{
                                            //    ItemInsertValues.Add("null");
                                            //}
                                             
                                            var val = iprop.GetMethod.Invoke(SourceModel, null);
                                            if (val != null)
                                            {
                                                ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");
                                            }
                                            else ItemInsertValues.Add("null");
                                        }
                                        catch (Exception ex)
                                        {
                                            ErrorLog.WriteToLog("AppException", key + "-long] - " + ex.Message);
                                        }
                                        //string v = prop.GetValue(SourceModel, null) as string;
                                        //if (string.IsNullOrEmpty(v))
                                        //{
                                        //    ItemInsertValues.Add(null);
                                        //}
                                        //else
                                        //{
                                        //    ItemInsertValues.Add(iprop.GetValue(item).ToString());
                                        //}
                                    }
                                    //  else
                                    if (iprop.PropertyType.Equals(typeof(int?)) || iprop.PropertyType.Equals(typeof(int)))
                                    {
                                        try
                                        {
                                            //ItemInsertValues.Add(iprop.GetValue(item).ToString());
                                            //if (iprop.GetValue(item) != null)
                                            //{
                                            //    ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");
                                            //}
                                            //else
                                            //{
                                            //    ItemInsertValues.Add("null");
                                            //}
                                             
                                            var val = iprop.GetMethod.Invoke(SourceModel, null);
                                            if (val != null)
                                            {
                                                ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");
                                            }
                                            else ItemInsertValues.Add("null");
                                        }
                                        catch (Exception ex)
                                        {
                                            ErrorLog.WriteToLog("AppException", key + "-int] - " + ex.Message);
                                        }
                                        //string v = prop.GetValue(SourceModel, null) as string;
                                        //if (string.IsNullOrEmpty(v))
                                        //{
                                        //    ItemInsertValues.Add(null);
                                        //}
                                        //else
                                        //{
                                        //    ItemInsertValues.Add(iprop.GetValue(item).ToString());
                                        //}
                                    }
                                    //   else
                                    if (iprop.PropertyType.Equals(typeof(string)))
                                    {
                                        try
                                        {
                                            //string v = iprop.GetValue(item, null) as string;
                                            //if (string.IsNullOrEmpty(v))
                                            //{
                                            //    // ItemInsertValues.Add(v);
                                            //    ItemInsertValues.Add("'" + string.Empty + "'");
                                            //}
                                            //else
                                            //{
                                            //    ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");
                                            //}
                                             
                                            var val = iprop.GetMethod.Invoke(SourceModel, null);
                                            if (val != null)
                                            {
                                                ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");
                                            }
                                            else ItemInsertValues.Add("null");
                                        }
                                        catch (Exception ex)
                                        {
                                            ErrorLog.WriteToLog("AppException", key + "-single] - " + ex.Message);
                                        }
                                    }
                                    //  else
                                    if (iprop.PropertyType.Equals(typeof(Nullable<DateTime>)))
                                    {
                                        try
                                        {
                                            //if (iprop.GetValue(item) != null)
                                            //{
                                            //    ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");
                                            //}
                                            //else
                                            //{
                                            //    ItemInsertValues.Add("null");
                                            //}
                                             
                                            var val = iprop.GetMethod.Invoke(SourceModel, null);
                                            if (val != null)
                                            {
                                                ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");
                                            }
                                            else ItemInsertValues.Add("null");
                                        }
                                        catch (Exception ex)
                                        {
                                            ErrorLog.WriteToLog("AppException", key + "-datetime] - " + ex.Message);
                                        }
                                    }
                                    //  else
                                    if (((iprop.PropertyType.Equals(typeof(bool?))) || (iprop.PropertyType.Equals(typeof(bool)))))
                                    {
                                        //ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");

                                        try
                                        {
                                            //ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");
                                            //if (iprop.GetValue(SourceModel) != null)
                                            //{
                                            //    ValueFields.Add(prop.GetValue(SourceModel).ToString());
                                            //}
                                            //else
                                            //{
                                            //    ValueFields.Add("null");
                                            //}
                                              
                                            var val = iprop.GetMethod.Invoke(SourceModel, null);
                                            if (val != null)
                                            {
                                                ItemInsertValues.Add("'" + iprop.GetValue(item).ToString() + "'");
                                            }
                                            else ItemInsertValues.Add("null");

                                        }
                                        catch (Exception ex)
                                        {
                                            ErrorLog.WriteToLog("AppException", key + "-bool] - " + ex.Message);
                                        }
                                    }


                                    if (j == TotalFields)
                                    {
                                        // var RealtionDeclareTable = "test";
                                        IsValues = string.Join(",", ItemInsertValues.ToArray());
                                        ErrorLog.WriteToLog("AppException", "99-insertVAleu" + InsertCommand.ToString());

                                        InsertCommand.Add(" INSERT INTO " + NameTableTemp + " (" + IsFields + ") VALUES ( " + IsValues + " ); ");
                                        ErrorLog.WriteToLog("AppException", "99-" + InsertCommand.ToString());

                                        InsertCommandMarge.Add(" INSERT (" + IsFields + ") VALUES ( " + IsValues + " ) ");
                                        ErrorLog.WriteToLog("AppException", "999-" + InsertCommand.ToString());
                                    }
                                    j++;
                                }
                                catch (Exception ex)
                                {
                                    ErrorLog.WriteToLog("AppException", "[MergeRelasi] - " + ex.Message);
                                }

                            }

                        }
                    }
                    // Console.WriteLine(InsertCommandMarge);
                    List<string> NFields = new List<string>();
                    FinishedGoodsCheckWeighingIngredient l = new FinishedGoodsCheckWeighingIngredient();
                    Type _lt = l.GetType();
                    NFields = GetFieldSQL(_lt);

                    var QueryCommandInsert = string.Join("", InsertCommand.ToArray());
                    ErrorLog.WriteToLog("AppException", QueryCommandInsert);
                    var InsertQuery = " INSERT (" + FormatMergeUpdateInsert(Fields, NFields, "") + ") VALUES (" + FormatMergeUpdateInsert(Fields, NFields, "INSERT") + " ) ";
                    var UpdateQuery = FormatMergeUpdateInsert(Fields, NFields, "UPDATE");

                    SQLMerge = "  DECLARE " + NameTableTemp + " " +
                                    "  TABLE (" + string.Join(",", FieldsType.ToArray()) + " ) " +
                                    " " + QueryCommandInsert + " " +
                                    "  MERGE INTO " + TargetTable + " WITH (HOLDLOCK) AS TARGET " +
                                    "  USING " + NameTableTemp + " AS SOURCE " +
                                    "	 ON SOURCE.EbrId  = TARGET.EbrId " +
                                    "	 AND SOURCE.Source = TARGET.Source " +
                                    "	 AND SOURCE.WoNumber = TARGET.WoNumber " +
                                    "	 AND SOURCE.Source = TARGET.Source " +
                                    "  WHEN MATCHED THEN " +
                                    "   UPDATE SET " + UpdateQuery + " " +
                                    "  WHEN NOT MATCHED BY TARGET THEN  " +
                                    "  " + InsertQuery + ";";
                    // "  SET IDENTITY_INSERT " + TargetTable + " OFF;";
                    ErrorLog.WriteToLog("AppException", "[4] - " + SQLMerge);
                    // Console.WriteLine(SQL);
                    #endregion
                }
                else
                if (prop.PropertyType.Equals(typeof(List<WipMaterialIssueItemDetailRM>)))
                {
                    #region WipMaterialIssueItemDetailRM
                    //ValueFields.Add("'Relation model MasterRecipeStepMaterialAsociation'");
                    //tblO_PackagingMaterialIssue_Item, tblO_PackagingMaterialIssue_Item_Detail

                    var NameTableTemp = "@TableRawMaterialIssueItemDetail";
                    var TargetTable = "tblO_RawMaterialIssue_Item_Detail";

                    List<string> value = new List<string>();
                    List<string> InsertCommand = new List<string>();
                    List<string> InsertCommandMarge = new List<string>();

                    List<string> FieldsType = new List<string>();
                    List<string> Fields = new List<string>();

                    WipMaterialIssueItemDetailRM m = new WipMaterialIssueItemDetailRM();
                    Type _st = m.GetType();
                    FieldsType = GetFieldType(_st);
                    Fields = GetFieldSQL(_st);

                    var IsFields = "";
                    var IsValues = "";

                    // IsFields = string.Join(",", Fields.ToArray());

                    //var jsonString = JsonConvert.SerializeObject(prop.GetValue(SourceModel));
                    //var jsonString = DataChild(ref SourceModel, prop);
                    IsFields = string.Join(",", Fields.ToArray());
                    string jsonString = JsonConvert.SerializeObject(prop.GetValue(SourceModel));


                    var Data = JsonConvert.DeserializeObject<List<WipMaterialIssueItemDetailRM>>(jsonString);
                    if (Data != null)
                    {
                        foreach (var item in Data) // 10
                        {
                            Type i_Type = item.GetType();
                            IList<PropertyInfo> i_Props = new List<PropertyInfo>(i_Type.GetProperties());
                            var TotalFields = i_Props.Count;
                            var j = 1;
                            List<string> ItemInsertValues = new List<string>();
                            foreach (PropertyInfo iprop in i_Props)
                            {

                                try
                                {
                                    if (iprop.PropertyType.Equals(typeof(decimal?)) || iprop.PropertyType.Equals(typeof(decimal)))
                                    {
                                         
                                        var val = iprop.GetMethod.Invoke(item, null);
                                        if (val != null)
                                        {
                                            ItemInsertValues.Add("'" + val.ToString() + "'");
                                        }
                                        else ItemInsertValues.Add("null");
                                    }
                                    else
                                    if (iprop.PropertyType.Equals(typeof(Single?)) || iprop.PropertyType.Equals(typeof(Single)))
                                    {
                                         
                                        var val = iprop.GetMethod.Invoke(item, null);
                                        if (val != null)
                                        {
                                            ItemInsertValues.Add("'" + val.ToString() + "'");
                                        }
                                        else ItemInsertValues.Add("null");
                                    }
                                    else
                                    if (iprop.PropertyType.Equals(typeof(long?)) || iprop.PropertyType.Equals(typeof(long)))
                                    {
                                        if (iprop.Name.Equals("HeaderRecId"))
                                        {
                                             
                                            var val = KeyColumnName[0];
                                            ItemInsertValues.Add(val);

                                        }
                                        else
                                        if (iprop.Name.Equals("HeaderId"))
                                        {
                                             
                                            //var val = iprop.GetMethod.Invoke(item, null);
                                            var val = KeyColumnName[0];
                                            if (val != null)
                                            {
                                                ItemInsertValues.Add(val);
                                            }
                                            else ItemInsertValues.Add("null");

                                        }
                                        else
                                        {
                                             
                                            var val = iprop.GetMethod.Invoke(item, null);
                                            if (val != null)
                                            {
                                                ItemInsertValues.Add("'" + val.ToString() + "'");
                                            }
                                            else ItemInsertValues.Add("null");

                                        }
                                    }
                                    else
                                    if (iprop.PropertyType.Equals(typeof(int?)) || iprop.PropertyType.Equals(typeof(int)))
                                    {
                                         
                                        var val = iprop.GetMethod.Invoke(item, null);
                                        if (val != null)
                                        {
                                            ItemInsertValues.Add("'" + val.ToString() + "'");
                                        }
                                        else ItemInsertValues.Add("null");

                                    }
                                    else
                                    if (iprop.PropertyType.Equals(typeof(string)))
                                    {
                                         

                                        var val = iprop.GetMethod.Invoke(item, null);
                                        if (val != null)
                                        {
                                            ItemInsertValues.Add("'" + val.ToString() + "'");
                                        }
                                        else ItemInsertValues.Add("null");

                                    }
                                    else
                                    if (iprop.PropertyType.Equals(typeof(Nullable<DateTime>)))
                                    {
                                         
                                        var val = iprop.GetMethod.Invoke(item, null);
                                        if (val != null)
                                        {
                                            ItemInsertValues.Add("'" + val.ToString() + "'");
                                        }
                                        else ItemInsertValues.Add("null");

                                    }
                                    else
                                    if (iprop.PropertyType.Equals(typeof(bool?)) || iprop.PropertyType.Equals(typeof(bool)))
                                    {
                                         
                                        var val = iprop.GetMethod.Invoke(item, null);
                                        if (val != null)
                                        {
                                            ItemInsertValues.Add("'" + val.ToString() + "'");
                                        }
                                        else ItemInsertValues.Add("null");

                                    }



                                    if (j == TotalFields)
                                    {
                                        IsValues = string.Join(",", ItemInsertValues.ToArray());

                                        InsertCommand.Add(" INSERT INTO " + NameTableTemp + " (" + IsFields + ") VALUES ( " + IsValues + " ); ");
                                        InsertCommandMarge.Add(" INSERT (" + IsFields + ") VALUES ( " + IsValues + " ) ");
                                    }

                                }
                                catch (Exception ex)
                                {
                                    ErrorLog.WriteToLog("WipMaterialIssueItemDetailPMAppException", "[GetValueSQL] - " + iprop.Name + " - " + ex.Message);
                                }

                                j++;

                            }
                        }
                    }
                    List<string> NFields = new List<string>();
                    WipMaterialIssueItemDetailRM l = new WipMaterialIssueItemDetailRM();
                    Type _lt = l.GetType();
                    NFields = GetFieldSQL(_lt);

                    var QueryCommandInsert = string.Join("", InsertCommand.ToArray());
                    var InsertQuery = " INSERT (" + FormatMergeUpdateInsert(Fields, NFields, "") + ") VALUES (" + FormatMergeUpdateInsert(Fields, NFields, "INSERT") + " ) ";
                    var UpdateQuery = FormatMergeUpdateInsert(Fields, NFields, "UPDATE");

                    SQLMerge = "  DECLARE " + NameTableTemp + " " +
                                   "  TABLE (" + string.Join(",", FieldsType.ToArray()) + " ) " +
                                   " " + QueryCommandInsert + " " +
                                   "  MERGE INTO " + TargetTable + " WITH (HOLDLOCK) AS TARGET " +
                                   "  USING " + NameTableTemp + " AS SOURCE " +
                                   "  ON SOURCE.HeaderId  = TARGET.HeaderId " +
                                   "  AND SOURCE.HeaderRecId = TARGET.HeaderRecId  " +
                                   "  AND SOURCE.ItemCode = TARGET.ItemCode " +
                                   "  AND SOURCE.ItemDescription = TARGET.ItemDescription "+
                                   "  AND SOURCE.RecId = TARGET.RecId " +
                                   "  WHEN MATCHED THEN " +
                                   "   UPDATE SET " + UpdateQuery + " " +
                                   "  WHEN NOT MATCHED BY TARGET THEN  " +
                                   "  " + InsertQuery + ";" +
                                   "  SET IDENTITY_INSERT " + TargetTable + " OFF;";
                    ErrorLog.WriteToLog("AppException", "[1] - " + SQLMerge);
                    #endregion
                }
                else
                if (prop.PropertyType.Equals(typeof(List<WipMaterialIssueAdditionalDetailRM>)))
                {
                    #region WipMaterialIssueAdditionalDetailRM
                    //ValueFields.Add("'Relation model MasterRecipeStepMaterialAsociation'");
                    //tblO_PackagingMaterialIssue_Item, tblO_PackagingMaterialIssue_Item_Detail

                    var NameTableTemp = "@TableRawMaterialIssueItemAdditionalDetail";
                    var TargetTable = "tblO_RawMaterialIssue_Additional_Detail";

                    List<string> value = new List<string>();
                    List<string> InsertCommand = new List<string>();
                    List<string> InsertCommandMarge = new List<string>();

                    List<string> FieldsType = new List<string>();
                    List<string> Fields = new List<string>();

                    WipMaterialIssueAdditionalDetailRM m = new WipMaterialIssueAdditionalDetailRM();
                    Type _st = m.GetType();
                    FieldsType = GetFieldType(_st);
                    Fields = GetFieldSQL(_st);

                    var IsFields = "";
                    var IsValues = "";

                    //var jsonString = DataChild(ref SourceModel, prop);
                    IsFields = string.Join(",", Fields.ToArray());
                    string jsonString = JsonConvert.SerializeObject(prop.GetValue(SourceModel));


                    Console.WriteLine(prop.GetValue(SourceModel));
                    var Data = JsonConvert.DeserializeObject<List<WipMaterialIssueAdditionalDetailRM>>(jsonString);
                    if (Data != null)
                    {
                        foreach (var item in Data) // 10
                        {
                            Type i_Type = item.GetType();
                            IList<PropertyInfo> i_Props = new List<PropertyInfo>(i_Type.GetProperties());
                            var TotalFields = i_Props.Count;
                            var j = 1;
                            List<string> ItemInsertValues = new List<string>();
                            foreach (PropertyInfo iprop in i_Props)
                            {
                                try
                                {
                                    if (iprop.PropertyType.Equals(typeof(decimal?)) || iprop.PropertyType.Equals(typeof(decimal)))
                                    {
                                         
                                        var val = iprop.GetMethod.Invoke(item, null);
                                        if (val != null)
                                        {
                                            ItemInsertValues.Add("'" + val.ToString() + "'");
                                        }
                                        else ItemInsertValues.Add("null");
                                    }
                                    else
                                    if (iprop.PropertyType.Equals(typeof(Single?)) || iprop.PropertyType.Equals(typeof(Single)))
                                    {
                                         
                                        var val = iprop.GetMethod.Invoke(item, null);
                                        if (val != null)
                                        {
                                            ItemInsertValues.Add("'" + val.ToString() + "'");
                                        }
                                        else ItemInsertValues.Add("null");
                                    }
                                    else
                                    if (iprop.PropertyType.Equals(typeof(long?)) || iprop.PropertyType.Equals(typeof(long)))
                                    {
                                        if (iprop.Name.Equals("HeaderRecId"))
                                        {
                                             
                                            var val = KeyColumnName[0];
                                            ItemInsertValues.Add(val);

                                        }
                                        else
                                        if (iprop.Name.Equals("HeaderId"))
                                        {
                                             
                                            //var val = iprop.GetMethod.Invoke(item, null);
                                            var val = KeyColumnName[0];
                                            if (val != null)
                                            {
                                                ItemInsertValues.Add(val);
                                            }
                                            else ItemInsertValues.Add("null");

                                        }
                                        else
                                        {
                                             
                                            var val = iprop.GetMethod.Invoke(item, null);
                                            if (val != null)
                                            {
                                                ItemInsertValues.Add("'" + val.ToString() + "'");
                                            }
                                            else ItemInsertValues.Add("null");

                                        }
                                    }
                                    else
                                    if (iprop.PropertyType.Equals(typeof(int?)) || iprop.PropertyType.Equals(typeof(int)))
                                    {
                                         
                                        var val = iprop.GetMethod.Invoke(item, null);
                                        if (val != null)
                                        {
                                            ItemInsertValues.Add("'" + val.ToString() + "'");
                                        }
                                        else ItemInsertValues.Add("null");

                                    }
                                    else
                                    if (iprop.PropertyType.Equals(typeof(string)))
                                    {
                                         

                                        var val = iprop.GetMethod.Invoke(item, null);
                                        if (val != null)
                                        {
                                            ItemInsertValues.Add("'" + val.ToString() + "'");
                                        }
                                        else ItemInsertValues.Add("null");

                                    }
                                    else
                                    if (iprop.PropertyType.Equals(typeof(Nullable<DateTime>)))
                                    {
                                         
                                        var val = iprop.GetMethod.Invoke(item, null);
                                        if (val != null)
                                        {
                                            ItemInsertValues.Add("'" + val.ToString() + "'");
                                        }
                                        else ItemInsertValues.Add("null");

                                    }
                                    else
                                    if (iprop.PropertyType.Equals(typeof(bool?)) || iprop.PropertyType.Equals(typeof(bool)))
                                    {
                                         
                                        var val = iprop.GetMethod.Invoke(item, null);
                                        if (val != null)
                                        {
                                            ItemInsertValues.Add("'" + val.ToString() + "'");
                                        }
                                        else ItemInsertValues.Add("null");

                                    }



                                    if (j == TotalFields)
                                    {
                                        IsValues = string.Join(",", ItemInsertValues.ToArray());

                                        InsertCommand.Add(" INSERT INTO " + NameTableTemp + " (" + IsFields + ") VALUES ( " + IsValues + " ); ");
                                        InsertCommandMarge.Add(" INSERT (" + IsFields + ") VALUES ( " + IsValues + " ) ");
                                    }

                                }
                                catch (Exception ex)
                                {
                                    ErrorLog.WriteToLog("WipMaterialIssueItemDetailPMAppException", "[GetValueSQL] - " + iprop.Name + " - " + ex.Message);
                                }

                                j++;
                            }
                        }
                    }
                    List<string> NFields = new List<string>();
                    WipMaterialIssueAdditionalDetailRM l = new WipMaterialIssueAdditionalDetailRM();
                    Type _lt = l.GetType();
                    NFields = GetFieldSQL(_lt);

                    var QueryCommandInsert = string.Join("", InsertCommand.ToArray());
                    var InsertQuery = " INSERT (" + FormatMergeUpdateInsert(Fields, NFields, "") + ") VALUES (" + FormatMergeUpdateInsert(Fields, NFields, "INSERT") + " ) ";
                    var UpdateQuery = FormatMergeUpdateInsert(Fields, NFields, "UPDATE");

                    SQLMerge = "  DECLARE " + NameTableTemp + " " +
                                   "  TABLE (" + string.Join(",", FieldsType.ToArray()) + " ) " +
                                   " " + QueryCommandInsert + " " +
                                   "  MERGE INTO " + TargetTable + " WITH (HOLDLOCK) AS TARGET " +
                                   "  USING " + NameTableTemp + " AS SOURCE " +
                                   "  ON SOURCE.HeaderId  = TARGET.HeaderId " +
                                   "  AND SOURCE.HeaderRecId = TARGET.HeaderRecId " +
                                   "  AND SOURCE.ItemCode = TARGET.ItemCode " +
                                   "  WHEN MATCHED THEN " +
                                   "   UPDATE SET " + UpdateQuery + " " +
                                   "  WHEN NOT MATCHED BY TARGET THEN  " +
                                   "  " + InsertQuery + ";" +
                                   "  SET IDENTITY_INSERT " + TargetTable + " OFF;";
                    ErrorLog.WriteToLog("AppException", "[4] - " + SQLMerge);
                    #endregion
                }
                else
                if (prop.PropertyType.Equals(typeof(List<WipMaterialIssueReturnDetailRM>)))
                {
                    #region WipMaterialIssueRetrunRM
                    //ValueFields.Add("'Relation model MasterRecipeStepMaterialAsociation'");
                    //tblO_PackagingMaterialIssue_Item, tblO_PackagingMaterialIssue_Item_Detail

                    var NameTableTemp = "@TableRawMaterialIssueReturnDetail";
                    var TargetTable = "tblO_RawMaterialIssue_Return_Detail";

                    List<string> value = new List<string>();
                    List<string> InsertCommand = new List<string>();
                    List<string> InsertCommandMarge = new List<string>();

                    List<string> FieldsType = new List<string>();
                    List<string> Fields = new List<string>();

                    WipMaterialIssueReturnDetailRM m = new WipMaterialIssueReturnDetailRM();
                    Type _st = m.GetType();
                    FieldsType = GetFieldType(_st);
                    Fields = GetFieldSQL(_st);

                    var IsFields = "";
                    var IsValues = "";

                    IsFields = string.Join(",", Fields.ToArray());

                    //var jsonString = DataChild(ref SourceModel, prop);
                    IsFields = string.Join(",", Fields.ToArray());
                    string jsonString = JsonConvert.SerializeObject(prop.GetValue(SourceModel));


                    Console.WriteLine(prop.GetValue(SourceModel));
                    var Data = JsonConvert.DeserializeObject<List<WipMaterialIssueReturnDetailRM>>(jsonString);
                    if (Data != null)
                    {
                        foreach (var item in Data) // 10
                        {
                            Type i_Type = item.GetType();
                            IList<PropertyInfo> i_Props = new List<PropertyInfo>(i_Type.GetProperties());
                            var TotalFields = i_Props.Count;
                            var j = 1;
                            List<string> ItemInsertValues = new List<string>();
                            foreach (PropertyInfo iprop in i_Props)
                            {
                                try
                                {
                                    if (iprop.PropertyType.Equals(typeof(decimal?)) || iprop.PropertyType.Equals(typeof(decimal)))
                                    {
                                         
                                        var val = iprop.GetMethod.Invoke(item, null);
                                        if (val != null)
                                        {
                                            ItemInsertValues.Add("'" + val.ToString() + "'");
                                        }
                                        else ItemInsertValues.Add("null");
                                    }
                                    else
                                    if (iprop.PropertyType.Equals(typeof(Single?)) || iprop.PropertyType.Equals(typeof(Single)))
                                    {
                                         
                                        var val = iprop.GetMethod.Invoke(item, null);
                                        if (val != null)
                                        {
                                            ItemInsertValues.Add("'" + val.ToString() + "'");
                                        }
                                        else ItemInsertValues.Add("null");
                                    }
                                    else
                                    if (iprop.PropertyType.Equals(typeof(long?)) || iprop.PropertyType.Equals(typeof(long)))
                                    {
                                        if (iprop.Name.Equals("HeaderRecId"))
                                        {
                                             
                                            var val = KeyColumnName[0];
                                            ItemInsertValues.Add(val);

                                        }
                                        else
                                        if (iprop.Name.Equals("HeaderId"))
                                        {
                                             
                                            //var val = iprop.GetMethod.Invoke(item, null);
                                            var val = KeyColumnName[0];
                                            if (val != null)
                                            {
                                                ItemInsertValues.Add(val);
                                            }
                                            else ItemInsertValues.Add("null");

                                        }
                                        else
                                        {
                                             
                                            var val = iprop.GetMethod.Invoke(item, null);
                                            if (val != null)
                                            {
                                                ItemInsertValues.Add("'" + val.ToString() + "'");
                                            }
                                            else ItemInsertValues.Add("null");

                                        }
                                    }
                                    else
                                    if (iprop.PropertyType.Equals(typeof(int?)) || iprop.PropertyType.Equals(typeof(int)))
                                    {
                                         
                                        var val = iprop.GetMethod.Invoke(item, null);
                                        if (val != null)
                                        {
                                            ItemInsertValues.Add("'" + val.ToString() + "'");
                                        }
                                        else ItemInsertValues.Add("null");

                                    }
                                    else
                                    if (iprop.PropertyType.Equals(typeof(string)))
                                    {
                                         

                                        var val = iprop.GetMethod.Invoke(item, null);
                                        if (val != null)
                                        {
                                            ItemInsertValues.Add("'" + val.ToString() + "'");
                                        }
                                        else ItemInsertValues.Add("null");

                                    }
                                    else
                                    if (iprop.PropertyType.Equals(typeof(Nullable<DateTime>)))
                                    {
                                         
                                        var val = iprop.GetMethod.Invoke(item, null);
                                        if (val != null)
                                        {
                                            ItemInsertValues.Add("'" + val.ToString() + "'");
                                        }
                                        else ItemInsertValues.Add("null");

                                    }
                                    else
                                    if (iprop.PropertyType.Equals(typeof(bool?)) || iprop.PropertyType.Equals(typeof(bool)))
                                    {
                                         
                                        var val = iprop.GetMethod.Invoke(item, null);
                                        if (val != null)
                                        {
                                            ItemInsertValues.Add("'" + val.ToString() + "'");
                                        }
                                        else ItemInsertValues.Add("null");

                                    }



                                    if (j == TotalFields)
                                    {
                                        IsValues = string.Join(",", ItemInsertValues.ToArray());

                                        InsertCommand.Add(" INSERT INTO " + NameTableTemp + " (" + IsFields + ") VALUES ( " + IsValues + " ); ");
                                        InsertCommandMarge.Add(" INSERT (" + IsFields + ") VALUES ( " + IsValues + " ) ");
                                    }

                                }
                                catch (Exception ex)
                                {
                                    ErrorLog.WriteToLog("WipMaterialIssueItemDetailPMAppException", "[GetValueSQL] - " + iprop.Name + " - " + ex.Message);
                                }

                                j++;
                            }
                        }
                    }
                    List<string> NFields = new List<string>();
                    WipMaterialIssueReturnDetailRM l = new WipMaterialIssueReturnDetailRM();
                    Type _lt = l.GetType();
                    NFields = GetFieldSQL(_lt);

                    var QueryCommandInsert = string.Join("", InsertCommand.ToArray());
                    var InsertQuery = " INSERT (" + FormatMergeUpdateInsert(Fields, NFields, "") + ") VALUES (" + FormatMergeUpdateInsert(Fields, NFields, "INSERT") + " ) ";
                    var UpdateQuery = FormatMergeUpdateInsert(Fields, NFields, "UPDATE");

                    SQLMerge = "  DECLARE " + NameTableTemp + " " +
                                  "  TABLE (" + string.Join(",", FieldsType.ToArray()) + " ) " +
                                  " " + QueryCommandInsert + " " +
                                  "  MERGE INTO " + TargetTable + " WITH (HOLDLOCK) AS TARGET " +
                                  "  USING " + NameTableTemp + " AS SOURCE " +
                                  "  ON SOURCE.HeaderId  = TARGET.HeaderId " +
                                  "  AND SOURCE.HeaderRecId = TARGET.HeaderRecId " +
                                  "  AND SOURCE.ItemCode = TARGET.ItemCode " +
                                  "  WHEN MATCHED THEN " +
                                  "   UPDATE SET " + UpdateQuery + " " +
                                  "  WHEN NOT MATCHED BY TARGET THEN  " +
                                  "  " + InsertQuery + ";" +
                                  "  SET IDENTITY_INSERT " + TargetTable + " OFF;";
                    ErrorLog.WriteToLog("AppException", "[4] - " + SQLMerge);
                    #endregion
                }
                else
                //PM
                if (prop.PropertyType.Equals(typeof(List<WipMaterialIssueItemDetailPM>)))
                {
                    #region WipMaterialIssueItemDetailPM

                    var NameTableTemp = "@TablePackagingMaterialIssueItemDetail";
                    var TargetTable = "tblO_PackagingMaterialIssue_Item_Detail";

                    List<string> value = new List<string>();
                    List<string> InsertCommand = new List<string>();
                    List<string> InsertCommandMarge = new List<string>();

                    List<string> FieldsType = new List<string>();
                    List<string> Fields = new List<string>();

                    WipMaterialIssueItemDetailPM m = new WipMaterialIssueItemDetailPM();
                    Type _st = m.GetType();
                    FieldsType = GetFieldType(_st);
                    Fields = GetFieldSQL(_st);

                    var IsFields = "";
                    var IsValues = "";

                    IsFields = string.Join(",", Fields.ToArray());
                    string jsonString = JsonConvert.SerializeObject(prop.GetValue(SourceModel));

                    var Data = JsonConvert.DeserializeObject<List<WipMaterialIssueItemDetailPM>>(jsonString);
                    foreach (var item in Data) // 10
                    {
                        Type i_Type = item.GetType();
                        IList<PropertyInfo> i_Props = new List<PropertyInfo>(i_Type.GetProperties());
                        var TotalFields = i_Props.Count;
                        var j = 1;
                        List<string> ItemInsertValues = new List<string>();

                        foreach (PropertyInfo iprop in i_Props)
                        {
                            try
                            {
                                if (iprop.PropertyType.Equals(typeof(decimal?)) || iprop.PropertyType.Equals(typeof(decimal)))
                                {
                                     
                                    var val = iprop.GetMethod.Invoke(item, null);
                                    if (val != null)
                                    {
                                        ItemInsertValues.Add("'" + val.ToString() + "'");
                                    }
                                    else ItemInsertValues.Add("null");
                                }
                                else
                                if (iprop.PropertyType.Equals(typeof(Single?)) || iprop.PropertyType.Equals(typeof(Single)))
                                {
                                     
                                    var val = iprop.GetMethod.Invoke(item, null);
                                    if (val != null)
                                    {
                                        ItemInsertValues.Add("'" + val.ToString() + "'");
                                    }
                                    else ItemInsertValues.Add("null");
                                }
                                else
                                if (iprop.PropertyType.Equals(typeof(long?)) || iprop.PropertyType.Equals(typeof(long)))
                                {
                                    //if (iprop.Name.Equals("HeaderRecId"))
                                    //{
                                    //     
                                    //    var val = KeyColumnName[0];
                                    //    ItemInsertValues.Add(val);

                                    //} else
                                    if (iprop.Name.Equals("HeaderId"))
                                    {
                                         
                                        //var val = iprop.GetMethod.Invoke(item, null);
                                        var val = KeyColumnName[0];
                                        if (val != null)
                                        {
                                            ItemInsertValues.Add(val);
                                        }
                                        else ItemInsertValues.Add("null");

                                    }
                                    else
                                    {
                                         
                                        var val = iprop.GetMethod.Invoke(item, null);
                                        if (val != null)
                                        {
                                            ItemInsertValues.Add("'" + val.ToString() + "'");
                                        }
                                        else ItemInsertValues.Add("null");

                                    }
                                }
                                else
                                if (iprop.PropertyType.Equals(typeof(int?)) || iprop.PropertyType.Equals(typeof(int)))
                                {
                                     
                                    var val = iprop.GetMethod.Invoke(item, null);
                                    if (val != null)
                                    {
                                        ItemInsertValues.Add("'" + val.ToString() + "'");
                                    }
                                    else ItemInsertValues.Add("null");

                                }
                                else
                                if (iprop.PropertyType.Equals(typeof(string)))
                                {
                                     

                                    var val = iprop.GetMethod.Invoke(item, null);
                                    if (val != null)
                                    {
                                        ItemInsertValues.Add("'" + val.ToString() + "'");
                                    }
                                    else ItemInsertValues.Add("null");

                                }
                                else
                                if (iprop.PropertyType.Equals(typeof(Nullable<DateTime>)))
                                {
                                     
                                    var val = iprop.GetMethod.Invoke(item, null);
                                    if (val != null)
                                    {
                                        ItemInsertValues.Add("'" + val.ToString() + "'");
                                    }
                                    else ItemInsertValues.Add("null");

                                }
                                else
                                if (iprop.PropertyType.Equals(typeof(bool?)) || iprop.PropertyType.Equals(typeof(bool)))
                                {
                                     
                                    var val = iprop.GetMethod.Invoke(item, null);
                                    if (val != null)
                                    {
                                        ItemInsertValues.Add("'" + val.ToString() + "'");
                                    }
                                    else ItemInsertValues.Add("null");

                                }



                                if (j == TotalFields)
                                {
                                    IsValues = string.Join(",", ItemInsertValues.ToArray());

                                    InsertCommand.Add(" INSERT INTO " + NameTableTemp + " (" + IsFields + ") VALUES ( " + IsValues + " ); ");
                                    InsertCommandMarge.Add(" INSERT (" + IsFields + ") VALUES ( " + IsValues + " ) ");
                                }

                            }
                            catch (Exception ex)
                            {
                                ErrorLog.WriteToLog("WipMaterialIssueItemDetailPMAppException", "[GetValueSQL] - " + iprop.Name + " - " + ex.Message);
                            }

                            j++;
                        }

                    }

                    List<string> NFields = new List<string>();
                    WipMaterialIssueItemDetailPM l = new WipMaterialIssueItemDetailPM();
                    Type _lt = l.GetType();
                    NFields = GetFieldSQL(_lt);

                    var QueryCommandInsert = string.Join("", InsertCommand.ToArray());
                    var InsertQuery = " INSERT (" + FormatMergeUpdateInsert(Fields, NFields, "") + ") VALUES (" + FormatMergeUpdateInsert(Fields, NFields, "INSERT") + " ) ";
                    var UpdateQuery = FormatMergeUpdateInsert(Fields, NFields, "UPDATE");

                    SQLMerge = "  DECLARE " + NameTableTemp + " " +
                                  "  TABLE (" + string.Join(",", FieldsType.ToArray()) + " ) " +
                                  " " + QueryCommandInsert + " " +
                                  "  MERGE INTO " + TargetTable + " WITH (HOLDLOCK) AS TARGET " +
                                  "  USING " + NameTableTemp + " AS SOURCE " +
                                  "  ON SOURCE.HeaderId  = TARGET.HeaderId " +
                                  "  AND SOURCE.HeaderRecId = TARGET.HeaderRecId " +
                                  "  AND SOURCE.ItemCode = TARGET.ItemCode " +
                                  "  WHEN MATCHED THEN " +
                                  "  UPDATE SET " + UpdateQuery + " " +
                                  "  WHEN NOT MATCHED BY TARGET THEN  " +
                                  "  " + InsertQuery + ";" +
                                  "  SET IDENTITY_INSERT " + TargetTable + " OFF;";
                    ErrorLog.WriteToLog("WipMaterialIssueItemDetailPM", "[GetValueSQL] - " + SQLMerge);

                    #endregion
                }
                else
                if (prop.PropertyType.Equals(typeof(List<WipMaterialIssueAdditionalDetailPM>)))
                {
                    #region WipMaterialIssueAdditionalDetailPM
                    //ValueFields.Add("'Relation model MasterRecipeStepMaterialAsociation'");
                    //tblO_PackagingMaterialIssue_Item, tblO_PackagingMaterialIssue_Item_Detail

                    var NameTableTemp = "@TablePackagingMaterialIssueAdditionalDetail";
                    var TargetTable = "tblO_PackagingMaterialIssue_Additional_Detail";

                    List<string> value = new List<string>();
                    List<string> InsertCommand = new List<string>();
                    List<string> InsertCommandMarge = new List<string>();

                    List<string> FieldsType = new List<string>();
                    List<string> Fields = new List<string>();

                    WipMaterialIssueAdditionalDetailPM m = new WipMaterialIssueAdditionalDetailPM();
                    Type _st = m.GetType();
                    FieldsType = GetFieldType(_st);
                    Fields = GetFieldSQL(_st);

                    var IsFields = "";
                    var IsValues = "";

                    //var jsonString = DataChild(ref SourceModel, prop);
                    IsFields = string.Join(",", Fields.ToArray());
                    var jsonString = DataChild(ref SourceModel, prop);

                    Console.WriteLine(prop.GetValue(SourceModel));


                    Console.WriteLine(prop.GetValue(SourceModel));
                    var Data = JsonConvert.DeserializeObject<List<WipMaterialIssueAdditionalDetailPM>>(jsonString);
                    if (Data != null)
                    {
                        foreach (var item in Data) // 10
                        {
                            Type i_Type = item.GetType();
                            IList<PropertyInfo> i_Props = new List<PropertyInfo>(i_Type.GetProperties());
                            var TotalFields = i_Props.Count;
                            var j = 1;
                            List<string> ItemInsertValues = new List<string>();
                            foreach (PropertyInfo iprop in i_Props)
                            {
                                try
                                {
                                    if (iprop.PropertyType.Equals(typeof(decimal?)) || iprop.PropertyType.Equals(typeof(decimal)))
                                    {
                                         
                                        var val = iprop.GetMethod.Invoke(item, null);
                                        if (val != null)
                                        {
                                            ItemInsertValues.Add("'" + val.ToString() + "'");
                                        }
                                        else ItemInsertValues.Add("null");
                                    }
                                    else
                                    if (iprop.PropertyType.Equals(typeof(Single?)) || iprop.PropertyType.Equals(typeof(Single)))
                                    {
                                         
                                        var val = iprop.GetMethod.Invoke(item, null);
                                        if (val != null)
                                        {
                                            ItemInsertValues.Add("'" + val.ToString() + "'");
                                        }
                                        else ItemInsertValues.Add("null");
                                    }
                                    else
                                    if (iprop.PropertyType.Equals(typeof(long?)) || iprop.PropertyType.Equals(typeof(long)))
                                    {
                                        if (iprop.Name.Equals("HeaderRecId"))
                                        {
                                             
                                            var val = KeyColumnName[0];
                                            ItemInsertValues.Add(val);

                                        }
                                        else
                                        if (iprop.Name.Equals("HeaderId"))
                                        {
                                             
                                            //var val = iprop.GetMethod.Invoke(item, null);
                                            var val = KeyColumnName[0];
                                            if (val != null)
                                            {
                                                ItemInsertValues.Add(val);
                                            }
                                            else ItemInsertValues.Add("null");

                                        }
                                        else
                                        {
                                             
                                            var val = iprop.GetMethod.Invoke(item, null);
                                            if (val != null)
                                            {
                                                ItemInsertValues.Add("'" + val.ToString() + "'");
                                            }
                                            else ItemInsertValues.Add("null");

                                        }
                                    }
                                    else
                                    if (iprop.PropertyType.Equals(typeof(int?)) || iprop.PropertyType.Equals(typeof(int)))
                                    {
                                         
                                        var val = iprop.GetMethod.Invoke(item, null);
                                        if (val != null)
                                        {
                                            ItemInsertValues.Add("'" + val.ToString() + "'");
                                        }
                                        else ItemInsertValues.Add("null");

                                    }
                                    else
                                    if (iprop.PropertyType.Equals(typeof(string)))
                                    {
                                         

                                        var val = iprop.GetMethod.Invoke(item, null);
                                        if (val != null)
                                        {
                                            ItemInsertValues.Add("'" + val.ToString() + "'");
                                        }
                                        else ItemInsertValues.Add("null");

                                    }
                                    else
                                    if (iprop.PropertyType.Equals(typeof(Nullable<DateTime>)))
                                    {
                                         
                                        var val = iprop.GetMethod.Invoke(item, null);
                                        if (val != null)
                                        {
                                            ItemInsertValues.Add("'" + val.ToString() + "'");
                                        }
                                        else ItemInsertValues.Add("null");

                                    }
                                    else
                                    if (iprop.PropertyType.Equals(typeof(bool?)) || iprop.PropertyType.Equals(typeof(bool)))
                                    {
                                         
                                        var val = iprop.GetMethod.Invoke(item, null);
                                        if (val != null)
                                        {
                                            ItemInsertValues.Add("'" + val.ToString() + "'");
                                        }
                                        else ItemInsertValues.Add("null");

                                    }



                                    if (j == TotalFields)
                                    {
                                        IsValues = string.Join(",", ItemInsertValues.ToArray());

                                        InsertCommand.Add(" INSERT INTO " + NameTableTemp + " (" + IsFields + ") VALUES ( " + IsValues + " ); ");
                                        InsertCommandMarge.Add(" INSERT (" + IsFields + ") VALUES ( " + IsValues + " ) ");
                                    }

                                }
                                catch (Exception ex)
                                {
                                    ErrorLog.WriteToLog("WipMaterialIssueItemDetailPMAppException", "[GetValueSQL] - " + iprop.Name + " - " + ex.Message);
                                }

                                j++;
                            }
                        }
                    }
                    List<string> NFields = new List<string>();
                    WipMaterialIssueAdditionalDetailPM l = new WipMaterialIssueAdditionalDetailPM();
                    Type _lt = l.GetType();
                    NFields = GetFieldSQL(_lt);

                    var QueryCommandInsert = string.Join("", InsertCommand.ToArray());
                    var InsertQuery = " INSERT (" + FormatMergeUpdateInsert(Fields, NFields, "") + ") VALUES (" + FormatMergeUpdateInsert(Fields, NFields, "INSERT") + " ) ";
                    var UpdateQuery = FormatMergeUpdateInsert(Fields, NFields, "UPDATE");

                    SQLMerge = "  DECLARE " + NameTableTemp + " " +
                                   "  TABLE (" + string.Join(",", FieldsType.ToArray()) + " ) " +
                                   " " + QueryCommandInsert + " " +
                                   "  MERGE INTO " + TargetTable + " WITH (HOLDLOCK) AS TARGET " +
                                   "  USING " + NameTableTemp + " AS SOURCE " +
                                   "  ON SOURCE.HeaderId  = TARGET.HeaderId " +
                                   "  AND SOURCE.HeaderRecId = TARGET.HeaderRecId " +
                                   "  AND SOURCE.ItemCode = TARGET.ItemCode " +
                                   "  WHEN MATCHED THEN " +
                                   "   UPDATE SET " + UpdateQuery + " " +
                                   "  WHEN NOT MATCHED BY TARGET THEN  " +
                                   "  " + InsertQuery + ";" +
                                   "  SET IDENTITY_INSERT " + TargetTable + " OFF;";
                    ErrorLog.WriteToLog("AppException", "[4] - " + SQLMerge);
                    #endregion
                }
                else
                if (prop.PropertyType.Equals(typeof(List<WipMaterialIssueReturnDetailPM>)))
                {
                    #region WipMaterialIssueReturnDetailPM
                    //ValueFields.Add("'Relation model MasterRecipeStepMaterialAsociation'");
                    //tblO_PackagingMaterialIssue_Item, tblO_PackagingMaterialIssue_Item_Detail

                    var NameTableTemp = "@TablePackagingMaterialIssueReturnDetail";
                    var TargetTable = "tblO_PackagingMaterialIssue_Return_Detail";

                    List<string> value = new List<string>();
                    List<string> InsertCommand = new List<string>();
                    List<string> InsertCommandMarge = new List<string>();

                    List<string> FieldsType = new List<string>();
                    List<string> Fields = new List<string>();

                    WipMaterialIssueReturnDetailPM m = new WipMaterialIssueReturnDetailPM();
                    Type _st = m.GetType();
                    FieldsType = GetFieldType(_st);
                    Fields = GetFieldSQL(_st);

                    var IsFields = "";
                    var IsValues = "";

                    IsFields = string.Join(",", Fields.ToArray());

                    //var jsonString = DataChild(ref SourceModel, prop);
                    var jsonString = DataChild(ref SourceModel, prop);
                    Console.WriteLine(prop.GetValue(SourceModel));


                    Console.WriteLine(prop.GetValue(SourceModel));
                    var Data = JsonConvert.DeserializeObject<List<WipMaterialIssueReturnDetailPM>>(jsonString);
                    if (Data != null)
                    {
                        foreach (var item in Data) // 10
                        {
                            Type i_Type = item.GetType();
                            IList<PropertyInfo> i_Props = new List<PropertyInfo>(i_Type.GetProperties());
                            var TotalFields = i_Props.Count;
                            var j = 1;
                            List<string> ItemInsertValues = new List<string>();
                            foreach (PropertyInfo iprop in i_Props)
                            {
                                try
                                {
                                    if (iprop.PropertyType.Equals(typeof(decimal?)) || iprop.PropertyType.Equals(typeof(decimal)))
                                    {
                                         
                                        var val = iprop.GetMethod.Invoke(item, null);
                                        if (val != null)
                                        {
                                            ItemInsertValues.Add("'" + val.ToString() + "'");
                                        }
                                        else ItemInsertValues.Add("null");
                                    }
                                    else
                                    if (iprop.PropertyType.Equals(typeof(Single?)) || iprop.PropertyType.Equals(typeof(Single)))
                                    {
                                         
                                        var val = iprop.GetMethod.Invoke(item, null);
                                        if (val != null)
                                        {
                                            ItemInsertValues.Add("'" + val.ToString() + "'");
                                        }
                                        else ItemInsertValues.Add("null");
                                    }
                                    else
                                    if (iprop.PropertyType.Equals(typeof(long?)) || iprop.PropertyType.Equals(typeof(long)))
                                    {
                                        if (iprop.Name.Equals("HeaderRecId"))
                                        {
                                             
                                            var val = KeyColumnName[0];
                                            ItemInsertValues.Add(val);

                                        }
                                        else
                                        if (iprop.Name.Equals("HeaderId"))
                                        {
                                             
                                            //var val = iprop.GetMethod.Invoke(item, null);
                                            var val = KeyColumnName[0];
                                            if (val != null)
                                            {
                                                ItemInsertValues.Add(val);
                                            }
                                            else ItemInsertValues.Add("null");

                                        }
                                        else
                                        {
                                             
                                            var val = iprop.GetMethod.Invoke(item, null);
                                            if (val != null)
                                            {
                                                ItemInsertValues.Add("'" + val.ToString() + "'");
                                            }
                                            else ItemInsertValues.Add("null");

                                        }
                                    }
                                    else
                                    if (iprop.PropertyType.Equals(typeof(int?)) || iprop.PropertyType.Equals(typeof(int)))
                                    {
                                         
                                        var val = iprop.GetMethod.Invoke(item, null);
                                        if (val != null)
                                        {
                                            ItemInsertValues.Add("'" + val.ToString() + "'");
                                        }
                                        else ItemInsertValues.Add("null");

                                    }
                                    else
                                    if (iprop.PropertyType.Equals(typeof(string)))
                                    {
                                         

                                        var val = iprop.GetMethod.Invoke(item, null);
                                        if (val != null)
                                        {
                                            ItemInsertValues.Add("'" + val.ToString() + "'");
                                        }
                                        else ItemInsertValues.Add("null");

                                    }
                                    else
                                    if (iprop.PropertyType.Equals(typeof(Nullable<DateTime>)))
                                    {
                                         
                                        var val = iprop.GetMethod.Invoke(item, null);
                                        if (val != null)
                                        {
                                            ItemInsertValues.Add("'" + val.ToString() + "'");
                                        }
                                        else ItemInsertValues.Add("null");

                                    }
                                    else
                                    if (iprop.PropertyType.Equals(typeof(bool?)) || iprop.PropertyType.Equals(typeof(bool)))
                                    {
                                         
                                        var val = iprop.GetMethod.Invoke(item, null);
                                        if (val != null)
                                        {
                                            ItemInsertValues.Add("'" + val.ToString() + "'");
                                        }
                                        else ItemInsertValues.Add("null");

                                    }



                                    if (j == TotalFields)
                                    {
                                        IsValues = string.Join(",", ItemInsertValues.ToArray());

                                        InsertCommand.Add(" INSERT INTO " + NameTableTemp + " (" + IsFields + ") VALUES ( " + IsValues + " ); ");
                                        InsertCommandMarge.Add(" INSERT (" + IsFields + ") VALUES ( " + IsValues + " ) ");
                                    }

                                }
                                catch (Exception ex)
                                {
                                    ErrorLog.WriteToLog("WipMaterialIssueItemDetailPMAppException", "[GetValueSQL] - " + iprop.Name + " - " + ex.Message);
                                }

                                j++;
                            }
                        }
                    }
                    List<string> NFields = new List<string>();
                    WipMaterialIssueReturnDetailPM l = new WipMaterialIssueReturnDetailPM();
                    Type _lt = l.GetType();
                    NFields = GetFieldSQL(_lt);

                    var QueryCommandInsert = string.Join("", InsertCommand.ToArray());
                    var InsertQuery = " INSERT (" + FormatMergeUpdateInsert(Fields, NFields, "") + ") VALUES (" + FormatMergeUpdateInsert(Fields, NFields, "INSERT") + " ) ";
                    var UpdateQuery = FormatMergeUpdateInsert(Fields, NFields, "UPDATE");


                    SQLMerge = "  DECLARE " + NameTableTemp + " " +
                                   "  TABLE (" + string.Join(",", FieldsType.ToArray()) + " ) " +
                                   " " + QueryCommandInsert + " " +
                                   "  MERGE INTO " + TargetTable + " WITH (HOLDLOCK) AS TARGET " +
                                   "  USING " + NameTableTemp + " AS SOURCE " +
                                   "  ON SOURCE.HeaderId  = TARGET.HeaderId " +
                                   "  AND SOURCE.HeaderRecId = TARGET.HeaderRecId " +
                                   "  AND SOURCE.ItemCode = TARGET.ItemCode " +
                                   "  WHEN MATCHED THEN " +
                                   "   UPDATE SET " + UpdateQuery + " " +
                                   "  WHEN NOT MATCHED BY TARGET THEN  " +
                                   "  " + InsertQuery + ";" +
                                   "  SET IDENTITY_INSERT " + TargetTable + " OFF;";
                    ErrorLog.WriteToLog("AppException", "[4] - " + SQLMerge);
                    #endregion
                }
                else
                if (prop.PropertyType.Equals(typeof(List<WipMaterialIssueSecondaryDetailPM>)))
                {
                    #region WipMaterialIssueSecondaryDetailPM
                    var NameTableTemp = "@TablePackagingMaterialIssueSecondaryDetail";
                    var TargetTable = "tblO_PackagingMaterialIssue_Secondary_Detail";

                    List<string> value = new List<string>();
                    List<string> InsertCommand = new List<string>();
                    List<string> InsertCommandMarge = new List<string>();

                    List<string> FieldsType = new List<string>();
                    List<string> Fields = new List<string>();

                    WipMaterialIssueSecondaryDetailPM m = new WipMaterialIssueSecondaryDetailPM();
                    Type _st = m.GetType();
                    FieldsType = GetFieldType(_st);
                    Fields = GetFieldSQL(_st);

                    var IsFields = "";
                    var IsValues = "";

                    IsFields = string.Join(",", Fields.ToArray());

                    var jsonString = DataChild(ref SourceModel, prop);
                    Console.WriteLine(prop.GetValue(SourceModel));

                    var Data = JsonConvert.DeserializeObject<List<WipMaterialIssueSecondaryDetailPM>>(jsonString);
                    if (Data != null)
                    {
                        foreach (var item in Data) // 10
                        {
                            Type i_Type = item.GetType();
                            IList<PropertyInfo> i_Props = new List<PropertyInfo>(i_Type.GetProperties());
                            var TotalFields = i_Props.Count;
                            var j = 1;
                            List<string> ItemInsertValues = new List<string>();
                            foreach (PropertyInfo iprop in i_Props)
                            {
                                try
                                {
                                    if (iprop.PropertyType.Equals(typeof(decimal?)) || iprop.PropertyType.Equals(typeof(decimal)))
                                    {
                                         
                                        var val = iprop.GetMethod.Invoke(item, null);
                                        if (val != null)
                                        {
                                            ItemInsertValues.Add("'" + val.ToString() + "'");
                                        }
                                        else ItemInsertValues.Add("null");
                                    }
                                    else
                                    if (iprop.PropertyType.Equals(typeof(Single?)) || iprop.PropertyType.Equals(typeof(Single)))
                                    {
                                         
                                        var val = iprop.GetMethod.Invoke(item, null);
                                        if (val != null)
                                        {
                                            ItemInsertValues.Add("'" + val.ToString() + "'");
                                        }
                                        else ItemInsertValues.Add("null");
                                    }
                                    else
                                    if (iprop.PropertyType.Equals(typeof(long?)) || iprop.PropertyType.Equals(typeof(long)))
                                    {
                                        if (iprop.Name.Equals("HeaderRecId"))
                                        {
                                             
                                            var val = KeyColumnName[0];
                                            ItemInsertValues.Add(val);

                                        }
                                        else
                                        if (iprop.Name.Equals("HeaderId"))
                                        {
                                             
                                            //var val = iprop.GetMethod.Invoke(item, null);
                                            var val = KeyColumnName[0];
                                            if (val != null)
                                            {
                                                ItemInsertValues.Add(val);
                                            }
                                            else ItemInsertValues.Add("null");

                                        }
                                        else
                                        {
                                             
                                            var val = iprop.GetMethod.Invoke(item, null);
                                            if (val != null)
                                            {
                                                ItemInsertValues.Add("'" + val.ToString() + "'");
                                            }
                                            else ItemInsertValues.Add("null");

                                        }
                                    }
                                    else
                                    if (iprop.PropertyType.Equals(typeof(int?)) || iprop.PropertyType.Equals(typeof(int)))
                                    {
                                         
                                        var val = iprop.GetMethod.Invoke(item, null);
                                        if (val != null)
                                        {
                                            ItemInsertValues.Add("'" + val.ToString() + "'");
                                        }
                                        else ItemInsertValues.Add("null");

                                    }
                                    else
                                    if (iprop.PropertyType.Equals(typeof(string)))
                                    {
                                         

                                        var val = iprop.GetMethod.Invoke(item, null);
                                        if (val != null)
                                        {
                                            ItemInsertValues.Add("'" + val.ToString() + "'");
                                        }
                                        else ItemInsertValues.Add("null");

                                    }
                                    else
                                    if (iprop.PropertyType.Equals(typeof(Nullable<DateTime>)))
                                    {
                                         
                                        var val = iprop.GetMethod.Invoke(item, null);
                                        if (val != null)
                                        {
                                            ItemInsertValues.Add("'" + val.ToString() + "'");
                                        }
                                        else ItemInsertValues.Add("null");

                                    }
                                    else
                                    if (iprop.PropertyType.Equals(typeof(bool?)) || iprop.PropertyType.Equals(typeof(bool)))
                                    {
                                         
                                        var val = iprop.GetMethod.Invoke(item, null);
                                        if (val != null)
                                        {
                                            ItemInsertValues.Add("'" + val.ToString() + "'");
                                        }
                                        else ItemInsertValues.Add("null");

                                    }



                                    if (j == TotalFields)
                                    {
                                        IsValues = string.Join(",", ItemInsertValues.ToArray());

                                        InsertCommand.Add(" INSERT INTO " + NameTableTemp + " (" + IsFields + ") VALUES ( " + IsValues + " ); ");
                                        InsertCommandMarge.Add(" INSERT (" + IsFields + ") VALUES ( " + IsValues + " ) ");
                                    }

                                }
                                catch (Exception ex)
                                {
                                    ErrorLog.WriteToLog("WipMaterialIssueItemDetailPMAppException", "[GetValueSQL] - " + iprop.Name + " - " + ex.Message);
                                }

                                j++;
                            }
                        }
                    }
                    List<string> NFields = new List<string>();
                    WipMaterialIssueSecondaryDetailPM l = new WipMaterialIssueSecondaryDetailPM();
                    Type _lt = l.GetType();
                    NFields = GetFieldSQL(_lt);

                    var QueryCommandInsert = string.Join("", InsertCommand.ToArray());
                    var InsertQuery = " INSERT (" + FormatMergeUpdateInsert(Fields, NFields, "") + ") VALUES (" + FormatMergeUpdateInsert(Fields, NFields, "INSERT") + " ) ";
                    var UpdateQuery = FormatMergeUpdateInsert(Fields, NFields, "UPDATE");


                    SQLMerge = "  DECLARE " + NameTableTemp + " " +
                                   "  TABLE (" + string.Join(",", FieldsType.ToArray()) + " ) " +
                                   " " + QueryCommandInsert + " " +
                                   "  MERGE INTO " + TargetTable + " WITH (HOLDLOCK) AS TARGET " +
                                   "  USING " + NameTableTemp + " AS SOURCE " +
                                   "  ON SOURCE.HeaderId  = TARGET.HeaderId " +
                                   "  AND SOURCE.HeaderRecId = TARGET.HeaderRecId " +
                                   "  AND SOURCE.ItemCode = TARGET.ItemCode " +
                                   "  WHEN MATCHED THEN " +
                                   "   UPDATE SET " + UpdateQuery + " " +
                                   "  WHEN NOT MATCHED BY TARGET THEN  " +
                                   "  " + InsertQuery + ";" +
                                   "  SET IDENTITY_INSERT " + TargetTable + " OFF;";
                    ErrorLog.WriteToLog("AppException", "[4] - " + SQLMerge);
                    #endregion
                }


            }
            #endregion

            string combinedString = string.Join(",", ValueFields);
            ErrorLog.WriteToLog("AppException", "[ValueFields] - " + combinedString);

            SQL = SQLMerge;
            return ValueFields;
        }

        public string FormatMergeUpdateInsert(List<string> FieldSource, List<string> FieldTarget, string key)
        {
            var TARGET = "TARGET";
            var SOURCE = "SOURCE";
            var SQL = "";

            List<string> IsTargetFields = new List<string>();
            List<string> IsUpdatedFields = new List<string>();
            List<string> IsInsertFields = new List<string>();

            for (int i = 0; i < FieldSource.Count; i++)
            {
                if (key.Equals("UPDATE"))
                {
                    //if (!FieldSource[i].Equals("[TransactId]"))
                    //{
                    //    IsTargetFields.Add(FieldTarget[i]);
                    //    IsUpdatedFields.Add("" + TARGET + "." + FieldTarget[i] + " = " + SOURCE + "." + FieldSource[i]);
                    //    IsInsertFields.Add(" " + SOURCE + "." + FieldSource[i]);
                    //}
                    //else
                    //if (!FieldSource[i].Equals("TransactId"))
                    //{
                    //    IsTargetFields.Add(FieldTarget[i]);
                    //    IsUpdatedFields.Add("" + TARGET + "." + FieldTarget[i] + " = " + SOURCE + "." + FieldSource[i]);
                    //    IsInsertFields.Add(" " + SOURCE + "." + FieldSource[i]);
                    //}


                    //if (FieldSource[i].Equals("[ProductCode]") || FieldSource[i].Equals("[ProductName]"))
                    //{

                    //}
                    //else
                    if (FieldSource[i].Equals("[TransactId]"))
                    {

                    }
                    else
                    {
                        IsTargetFields.Add(FieldTarget[i]);
                        IsUpdatedFields.Add("" + TARGET + "." + FieldTarget[i] + " = " + SOURCE + "." + FieldSource[i]);
                        IsInsertFields.Add(" " + SOURCE + "." + FieldSource[i]);
                    }
                }
                else
                {
                    IsTargetFields.Add(FieldTarget[i]);
                    IsUpdatedFields.Add("" + TARGET + "." + FieldTarget[i] + " = " + SOURCE + "." + FieldSource[i]);
                    IsInsertFields.Add(" " + SOURCE + "." + FieldSource[i]);
                }
            }

            if (key == "INSERT")
            {
                SQL = string.Join(",", IsInsertFields.ToArray());
            }
            else
            if (key == "UPDATE")
            {
                SQL = string.Join(",", IsUpdatedFields.ToArray());
            }
            else
            {
                SQL = string.Join(",", IsTargetFields.ToArray());
            }

            IsTargetFields.Clear();
            IsUpdatedFields.Clear();
            IsInsertFields.Clear();

            return SQL;
        }

        public string DataChild<T>(ref T SourceModel, PropertyInfo prop) where T : new()
        {
            string jsonString = JsonConvert.SerializeObject(prop.GetValue(SourceModel));
            return jsonString;
        }

        public string MergeSQLFormat(Type type, List<string> FieldsType, List<string> FieldsInsert, string NamaTableTemp, string NamaTableTarget, string InsertNoRalasi, string QueryInsert)
        {
            //var MergeWhere = " ON SOURCE.TransactId = TARGET.TransactId ";
            var MergeWhere = " ON SOURCE.RecId = TARGET.RecId ";
            string GETID = "";
            string OutQuery = "";
            var InsertParam = "";
            var Param = "";

            var TARGET = "TARGET";
            var SOURCE = "SOURCE";

            List<string> IsTargetFields = new List<string>();
            List<string> IsUpdatedFields = new List<string>();
            List<string> IsInsertFields = new List<string>();

            PropertyInfo[] propsTargetFields = type.GetProperties();
            var i = 0;

            foreach (var propfields in propsTargetFields)
            {
                try
                {
                    var f = propfields.Name.Trim();
                    ErrorLog.WriteToLog("AppException", "[kolomnya] - " + f + " [type] " + propfields.PropertyType);
                    if (propfields.PropertyType.Equals(typeof(decimal?)) || propfields.PropertyType.Equals(typeof(decimal)))
                    {
                        IsTargetFields.Add("[" + f + "]");
                        IsUpdatedFields.Add("" + TARGET + ".[" + f + "] = " + SOURCE + "." + FieldsInsert[i]);
                        IsInsertFields.Add(" " + SOURCE + "." + FieldsInsert[i]);

                        ErrorLog.WriteToLog("AppException", "[local-decimal] - " + f);
                    }
                    else
                    if (propfields.PropertyType.Equals(typeof(long?)) || propfields.PropertyType.Equals(typeof(long)))
                    {

                        IsTargetFields.Add("[" + f + "]");

                        if (!FieldsInsert[i].Equals("[TransactId]") && (!FieldsInsert[i].Equals("[EbrId]")))
                        {


                            IsUpdatedFields.Add("" + TARGET + ".[" + f + "] = " + SOURCE + "." + FieldsInsert[i]);
                            ErrorLog.WriteToLog("AppException", "[local-long] - " + f);


                        }

                        IsInsertFields.Add(" " + SOURCE + "." + FieldsInsert[i]);


                    }
                    else
                    if (propfields.PropertyType.Equals(typeof(Single?)) || propfields.PropertyType.Equals(typeof(Single)))
                    {
                        //IsTargetFields.Add("[" + f + "]");
                        //IsUpdatedFields.Add("" + TARGET + ".[" + f + "] = " + SOURCE + "." + FieldsInsert[i]);
                        //IsInsertFields.Add(" " + SOURCE + "." + FieldsInsert[i]);

                        IsTargetFields.Add("[" + f + "]");

                        if (!FieldsInsert[i].Equals("[TransactId]"))
                        {

                            IsUpdatedFields.Add("" + TARGET + ".[" + f + "] = " + SOURCE + "." + FieldsInsert[i]);
                            ErrorLog.WriteToLog("AppException", "[local-int] - " + f);
                        }
                        //if (!FieldsInsert[i].Equals("[EbrId]"))
                        //{

                        //    IsUpdatedFields.Add("" + TARGET + ".[" + f + "] = " + SOURCE + "." + FieldsInsert[i]);
                        //    ErrorLog.WriteToLog("AppException", "[local-int] - " + f);
                        //}

                        IsInsertFields.Add(" " + SOURCE + "." + FieldsInsert[i]);


                        ErrorLog.WriteToLog("AppException", "[local-single] - " + f);
                    }
                    else
                    if (propfields.PropertyType.Equals(typeof(int?)) || propfields.PropertyType.Equals(typeof(int)))
                    {
                        //if (!FieldsInsert[i].Equals("[TransactId]"))
                        //{

                        //    IsTargetFields.Add("[" + f + "]");
                        //    IsUpdatedFields.Add("" + TARGET + ".[" + f + "] = " + SOURCE + "." + FieldsInsert[i]);
                        //    IsInsertFields.Add(" " + SOURCE + "." + FieldsInsert[i]);
                        //    ErrorLog.WriteToLog("AppException", "[local-int] - " + f);
                        //}

                        IsTargetFields.Add("[" + f + "]");

                        if (!FieldsInsert[i].Equals("[TransactId]"))
                        {

                            IsUpdatedFields.Add("" + TARGET + ".[" + f + "] = " + SOURCE + "." + FieldsInsert[i]);
                            ErrorLog.WriteToLog("AppException", "[local-int] - " + f);
                        }
                        //  if (!FieldsInsert[i].Equals("[EbrId]"))
                        //{

                        //    IsUpdatedFields.Add("" + TARGET + ".[" + f + "] = " + SOURCE + "." + FieldsInsert[i]);
                        //    ErrorLog.WriteToLog("AppException", "[local-int] - " + f);
                        //}

                        IsInsertFields.Add(" " + SOURCE + "." + FieldsInsert[i]);

                        ErrorLog.WriteToLog("AppException", "[local-int] - " + f);
                    }
                    else
                    if (propfields.PropertyType.Equals(typeof(string)))
                    {
                        if (f.Equals("ProductCode"))
                        {
                            //IsUpdatedFields.Add("" + TARGET + ".[" + f + "] = " + SOURCE + "." + FieldsInsert[i]);
                            //ErrorLog.WriteToLog("AppException", "[local-int] - " + f);
                            IsTargetFields.Add("[" + f + "]");
                            IsInsertFields.Add(" " + SOURCE + "." + FieldsInsert[i]);
                        } 
                        else
                        if (f.Equals("ProductName"))
                        {
                            // IsUpdatedFields.Add("" + TARGET + ".[" + f + "] = " + SOURCE + "." + FieldsInsert[i]);
                            // ErrorLog.WriteToLog("AppException", "[local-int] - " + f);
                            IsTargetFields.Add("[" + f + "]");
                            IsInsertFields.Add(" " + SOURCE + "." + FieldsInsert[i]);
                        }
                        else 
                        if (f.Equals("InvetoryPlanningMethod"))
                        {
                            IsTargetFields.Add("[" + f + "]");
                            IsUpdatedFields.Add("" + TARGET + ".[" + f + "] = " + SOURCE + "." + FieldsInsert[i]);
                            IsInsertFields.Add(" " + SOURCE + "." + FieldsInsert[i]);
                        }
                        else
                        {
                            IsTargetFields.Add("[" + f + "]");
                            IsUpdatedFields.Add("" + TARGET + ".[" + f + "] = " + SOURCE + "." + FieldsInsert[i]);
                            IsInsertFields.Add(" " + SOURCE + "." + FieldsInsert[i]);
                        }
                    }
                    else
                    if (propfields.PropertyType.Equals(typeof(Nullable<DateTime>)))
                    {
                        IsTargetFields.Add("[" + f + "]");
                        IsUpdatedFields.Add("" + TARGET + ".[" + f + "] = " + SOURCE + "." + FieldsInsert[i]);
                        IsInsertFields.Add(" " + SOURCE + "." + FieldsInsert[i]);

                        ErrorLog.WriteToLog("AppException", "[local-datetime] - " + f);
                    }
                    else
                    if (propfields.PropertyType.Equals(typeof(bool?)) || propfields.PropertyType.Equals(typeof(bool)))
                    {
                        if (f.Equals("IsFirstTimeDownloading"))
                        {
                            //IsUpdatedFields.Add("" + TARGET + ".[" + f + "] = " + SOURCE + "." + FieldsInsert[i]);
                            //ErrorLog.WriteToLog("AppException", "[local-int] - " + f);
                            IsTargetFields.Add("[" + f + "]");
                            IsInsertFields.Add(" " + SOURCE + "." + FieldsInsert[i]);
                        }
                        else
                        {
                            IsTargetFields.Add("[" + f + "]");
                            IsUpdatedFields.Add("" + TARGET + ".[" + f + "] = " + SOURCE + "." + FieldsInsert[i]);
                            IsInsertFields.Add(" " + SOURCE + "." + FieldsInsert[i]);
                            ErrorLog.WriteToLog("AppException", "[local-bool] - " + f);
                        }
                    }
                    else
                    if (propfields.PropertyType.Equals(typeof(List<EbridgingBppDetail>)))
                    {
                        // parent
                        MergeWhere = $@" ON SOURCE.WoNumber = TARGET.WoNumber AND SOURCE.Source = TARGET.Source AND SOURCE.EbrId = TARGET.EbrId ";
                    }
                    else
                    if (propfields.PropertyType.Equals(typeof(List<EbridgingFBppDetail>)))
                    {
                        // parent
                        MergeWhere = $@" ON SOURCE.WoNumber = TARGET.WoNumber AND SOURCE.Source = TARGET.Source AND SOURCE.EbrId = TARGET.EbrId ";
                    }
                    else
                    if (propfields.PropertyType.Equals(typeof(List<EbridgingFinishedGoodsCheckWeighingIngredient>)))
                    {
                        // parent
                        MergeWhere = $@" ON SOURCE.WoNumber = TARGET.WoNumber AND SOURCE.Source = TARGET.Source AND SOURCE.EbrId = TARGET.EbrId AND Source.BatchNumber = TARGET.BatchNumber ";

                    }
                    else
                    if (propfields.PropertyType.Equals(typeof(List<EbridgingFinishedGoodsCheckWeighingIngredient>)))
                    {
                        // parent
                        MergeWhere = $@" ON SOURCE.WoNumber = TARGET.WoNumber  AND SOURCE.Source = TARGET.Source AND SOURCE.EbrId = TARGET.EbrId AND Source.BatchNumber = TARGET.BatchNumber ";

                    }
                    // wip RM
                    else
                    if (propfields.PropertyType.Equals(typeof(List<WipMaterialIssueItemDetailRM>)))
                    {
                        //var n = "Status";
                        //IsTargetFields.Add("[" + n + "]");
                        //IsInsertFields.Add(" " + SOURCE + "." + n);
                        //Param = ",2";
                        MergeWhere = $@" ON SOURCE.Id = TARGET.Id  ";
                        //AND SOURCE.EbrId = TARGET.EbrId  AND Source.BatchNumber = TARGET.BatchNumber AND SOURCE.ProductCode = TARGET.ProductCode ";

                        GETID = "  OUTPUT $action AS ActionType, Inserted.TransactId AS HeaderId INTO @archive; " +
                                "  DECLARE @act varchar(255);  " +
                                "  DECLARE @Id int; " +
                                "  SELECT @act = ActionType,  @Id = HeaderId FROM  @archive WHERE  ActionType IN ( 'DELETE', 'UPDATE' ); " +
                                "  IF (@Act = 'UPDATE') " +
                                "  BEGIN " +
                                "      SET @HeaderId = @Id; " +
                                "  END " +
                                "  ELSE BEGIN " +
                                "      SELECT @HeaderId = SCOPE_IDENTITY()  " +
                                "  END; ";
                        //  SELECT @Id; ";
                    }
                    else
                    if (propfields.PropertyType.Equals(typeof(List<WipMaterialIssueAdditionalDetailRM>)))
                    {
                        //var n = "Status";
                        //IsTargetFields.Add("[" + n + "]");
                        //IsInsertFields.Add(" " + SOURCE + "." + n);
                        //Param = ",2";
                        MergeWhere = $@" ON SOURCE.Id = TARGET.Id  ";
                        //AND SOURCE.EbrId = TARGET.EbrId  AND Source.BatchNumber = TARGET.BatchNumber AND SOURCE.ProductCode = TARGET.ProductCode ";

                        GETID = "  OUTPUT $action AS ActionType, Inserted.TransactId AS HeaderId INTO @archive; " +
                                "  DECLARE @act varchar(255);  " +
                                "  DECLARE @Id int; " +
                                "  SELECT @act = ActionType,  @Id = HeaderId FROM  @archive WHERE  ActionType IN ( 'DELETE', 'UPDATE' ); " +
                                "  IF (@Act = 'UPDATE') " +
                                "  BEGIN " +
                                "      SET @HeaderId = @Id; " +
                                "  END " +
                                "  ELSE BEGIN " +
                                "      SELECT @HeaderId = SCOPE_IDENTITY()  " +
                                "  END; ";
                        //  SELECT @Id; ";
                    }
                    else
                    if (propfields.PropertyType.Equals(typeof(List<WipMaterialIssueReturnDetailRM>)))
                    {
                        //var n = "Status";
                        //IsTargetFields.Add("[" + n + "]");
                        //IsInsertFields.Add(" " + SOURCE + "." + n);
                        //Param = ",2";
                        MergeWhere = $@" ON SOURCE.Id = TARGET.Id  ";
                        //AND SOURCE.EbrId = TARGET.EbrId  AND Source.BatchNumber = TARGET.BatchNumber AND SOURCE.ProductCode = TARGET.ProductCode ";

                        GETID = "  OUTPUT $action AS ActionType, Inserted.TransactId AS HeaderId INTO @archive; " +
                                "  DECLARE @act varchar(255);  " +
                                "  DECLARE @Id int; " +
                                "  SELECT @act = ActionType,  @Id = HeaderId FROM  @archive WHERE  ActionType IN ( 'DELETE', 'UPDATE' ); " +
                                "  IF (@Act = 'UPDATE') " +
                                "  BEGIN " +
                                "      SET @HeaderId = @Id; " +
                                "  END " +
                                "  ELSE BEGIN " +
                                "      SELECT @HeaderId = SCOPE_IDENTITY()  " +
                                "  END; ";
                        //  SELECT @Id; ";
                    }
                    // wip PM
                    else
                    if (propfields.PropertyType.Equals(typeof(List<WipMaterialIssueItemDetailPM>)))
                    {
                        //var n = "Status";
                        //IsTargetFields.Add("[" + n + "]");
                        //IsInsertFields.Add(" " + SOURCE + "." + n);
                        //Param = ",2";
                        MergeWhere = $@" ON SOURCE.Id = TARGET.Id  ";
                        //AND SOURCE.EbrId = TARGET.EbrId  AND Source.BatchNumber = TARGET.BatchNumber AND SOURCE.ProductCode = TARGET.ProductCode ";

                        GETID = "  OUTPUT $action AS ActionType, Inserted.TransactId AS HeaderId INTO @archive; " +
                                "  DECLARE @act varchar(255);  " +
                                "  DECLARE @Id int; " +
                                "  SELECT @act = ActionType,  @Id = HeaderId FROM  @archive WHERE  ActionType IN ( 'DELETE', 'UPDATE' ); " +
                                "  IF (@Act = 'UPDATE') " +
                                "  BEGIN " +
                                "      SET @HeaderId = @Id; " +
                                "  END " +
                                "  ELSE BEGIN " +
                                "      SELECT @HeaderId = SCOPE_IDENTITY()  " +
                                "  END; ";
                        //  SELECT @Id; ";
                    }
                    else
                    if (propfields.PropertyType.Equals(typeof(List<WipMaterialIssueAdditionalDetailPM>)))
                    {
                        //var n = "Status";
                        //IsTargetFields.Add("[" + n + "]");
                        //IsInsertFields.Add(" " + SOURCE + "." + n);
                        //Param = ",2";
                        MergeWhere = $@" ON SOURCE.Id = TARGET.Id  ";
                        //AND SOURCE.EbrId = TARGET.EbrId  AND Source.BatchNumber = TARGET.BatchNumber AND SOURCE.ProductCode = TARGET.ProductCode ";

                        GETID = "  OUTPUT $action AS ActionType, Inserted.TransactId AS HeaderId INTO @archive; " +
                               "  DECLARE @act varchar(255);  " +
                               "  DECLARE @Id int; " +
                               "  SELECT @act = ActionType,  @Id = HeaderId FROM  @archive WHERE  ActionType IN ( 'DELETE', 'UPDATE' ); " +
                               "  IF (@Act = 'UPDATE') " +
                               "  BEGIN " +
                               "      SET @HeaderId = @Id; " +
                               "  END " +
                               "  ELSE BEGIN " +
                               "      SELECT @HeaderId = SCOPE_IDENTITY()  " +
                               "  END; ";
                    }
                    else
                    if (propfields.PropertyType.Equals(typeof(List<WipMaterialIssueReturnDetailPM>)))
                    {
                        //var n = "Status";
                        //IsTargetFields.Add("[" + n + "]");
                        //IsInsertFields.Add(" " + SOURCE + "." + n);
                        //Param = ",2";
                        MergeWhere = $@" ON SOURCE.Id = TARGET.Id  ";
                        //AND SOURCE.EbrId = TARGET.EbrId  AND Source.BatchNumber = TARGET.BatchNumber AND SOURCE.ProductCode = TARGET.ProductCode ";

                        GETID = "  OUTPUT $action AS ActionType, Inserted.TransactId AS HeaderId INTO @archive; " +
                               "  DECLARE @act varchar(255);  " +
                               "  DECLARE @Id int; " +
                               "  SELECT @act = ActionType,  @Id = HeaderId FROM  @archive WHERE  ActionType IN ( 'DELETE', 'UPDATE' ); " +
                               "  IF (@Act = 'UPDATE') " +
                               "  BEGIN " +
                               "      SET @HeaderId = @Id; " +
                               "  END " +
                               "  ELSE BEGIN " +
                               "      SELECT @HeaderId = SCOPE_IDENTITY()  " +
                               "  END; ";

                    }
                    else
                    if (propfields.PropertyType.Equals(typeof(List<WipMaterialIssueSecondaryDetailPM>)))
                    {
                        //var n = "Status";
                        //IsTargetFields.Add("[" + n + "]");
                        //IsInsertFields.Add(" " + SOURCE + "." + n);
                        //Param = ",2";
                        MergeWhere = $@" ON SOURCE.Id = TARGET.Id  ";
                        //AND SOURCE.EbrId = TARGET.EbrId  AND Source.BatchNumber = TARGET.BatchNumber AND SOURCE.ProductCode = TARGET.ProductCode ";

                        GETID = "  OUTPUT $action AS ActionType, Inserted.TransactId AS HeaderId INTO @archive; " +
                                "  DECLARE @act varchar(255);  " +
                                "  DECLARE @Id int; " +
                                "  SELECT @act = ActionType,  @Id = HeaderId FROM  @archive WHERE  ActionType IN ( 'DELETE', 'UPDATE' ); " +
                                "  IF (@Act = 'UPDATE') " +
                                "  BEGIN " +
                                "      SET @HeaderId = @Id; " +
                                "  END " +
                                "  ELSE BEGIN " +
                                "      SELECT @HeaderId = SCOPE_IDENTITY()  " +
                                "  END; ";
                        //  SELECT @Id; ";
                    }

                    if (Param == "")
                    {
                        InsertParam = InsertNoRalasi;
                    }
                    else
                    {
                        InsertParam = InsertNoRalasi + Param;
                    }
                }
                catch (Exception ex)
                {
                    ErrorLog.WriteToLog("AppException", "[MergeSQLFormat] - " + ex.Message);
                }
                i++;
            }

            InsertParam = InsertNoRalasi;

            var InsertQuery = " INSERT (" + string.Join(",", IsTargetFields.ToArray()) + ") VALUES ( " + InsertParam + " ) ";
            var UpdateQuery = " UPDATE SET " + string.Join(",", IsUpdatedFields.ToArray());

            var SQLMerge = " DECLARE @archive TABLE " +
                              " ( " +
                              "   ActionType VARCHAR(50), " +
                              "   HeaderId int " +
                              " ); " +
                              " DECLARE @HeaderId int; " +
                              " SET IDENTITY_INSERT " + NamaTableTarget + " OFF " +
                              "  DECLARE " + NamaTableTemp + " " +
                              "  TABLE (" + string.Join(",", FieldsType.ToArray()) + " ) " +
                              "  INSERT INTO " + NamaTableTemp + " (" + string.Join(",", FieldsInsert.ToArray()) + ")" +
                              "  VALUES ( " + QueryInsert + " ) " +
                              // "  SET IDENTITY_INSERT " + NamaTableTarget + " ON;" +
                              "  MERGE INTO " + NamaTableTarget + " WITH (HOLDLOCK) AS TARGET " +
                              "  USING " + NamaTableTemp + " AS SOURCE " +
                                 MergeWhere +
                              "  WHEN MATCHED THEN " +
                              "  " + UpdateQuery + " " +
                              "  WHEN NOT MATCHED BY TARGET THEN  " +
                              "  " + InsertQuery + "" +
                              " " + GETID + " " +
                              "  SET IDENTITY_INSERT " + NamaTableTarget + " OFF; ";


            ErrorLog.WriteToLog("AppException", "[MergeSQLFormat] - " + SQLMerge);
            //Console.Write(SQLMerge);
            IsTargetFields.Clear();
            IsUpdatedFields.Clear();
            IsInsertFields.Clear();

            return SQLMerge;
        }

        public string MergeSQLFormatValueRelasi(
               string UniqKey,
               Type type,
               List<string> FieldsType,
               List<string> FieldsInsert, string NamaTableTemp, string NamaTableTarget,
               string ValueInsert,
               string QueryInsert
        )
        {
            var TARGET = "TARGET";
            var SOURCE = "SOURCE";

            List<string> IsTargetFields = new List<string>();
            List<string> IsUpdatedFields = new List<string>();
            List<string> IsInsertFields = new List<string>();

            PropertyInfo[] propsTargetFields = type.GetProperties();
            var i = 0;
            foreach (var propfields in propsTargetFields)
            {
                try
                {
                    var f = propfields.Name.Trim();
                    if (propfields.PropertyType.Equals(typeof(decimal?)) || propfields.PropertyType.Equals(typeof(decimal)))
                    {
                        IsTargetFields.Add("[" + f + "]");
                        IsUpdatedFields.Add("" + TARGET + ".[" + f + "] = " + SOURCE + "." + FieldsInsert[i]);
                        IsInsertFields.Add(" " + SOURCE + "." + FieldsInsert[i]);
                    }
                    else
                    if (propfields.PropertyType.Equals(typeof(long?)) || propfields.PropertyType.Equals(typeof(long)))
                    {
                        {
                            IsTargetFields.Add("[" + f + "]");
                            IsInsertFields.Add(" " + SOURCE + "." + FieldsInsert[i]);

                            if (FieldsInsert[i] != "[RECID]")
                            {
                                IsUpdatedFields.Add("" + TARGET + ".[" + f + "] = " + SOURCE + "." + FieldsInsert[i]);
                            }
                            else if (FieldsInsert[i] != "[RecId]")
                            {
                                IsUpdatedFields.Add("" + TARGET + ".[" + f + "] = " + SOURCE + "." + FieldsInsert[i]);
                            }

                        }
                        //else
                        if (propfields.PropertyType.Equals(typeof(Single?)) || propfields.PropertyType.Equals(typeof(Single)))
                        {
                            IsTargetFields.Add("[" + f + "]");
                            IsInsertFields.Add(" " + SOURCE + "." + FieldsInsert[i]);

                            if (FieldsInsert[i] != "[RECID]")
                            {
                                IsUpdatedFields.Add("" + TARGET + ".[" + f + "] = " + SOURCE + "." + FieldsInsert[i]);
                            }
                            else if (FieldsInsert[i] != "[RecId]")
                            {
                                IsUpdatedFields.Add("" + TARGET + ".[" + f + "] = " + SOURCE + "." + FieldsInsert[i]);
                            }

                        }
                        else
                        if (propfields.PropertyType.Equals(typeof(int?)) || propfields.PropertyType.Equals(typeof(int)))
                        {
                            IsTargetFields.Add("[" + f + "]");
                            IsUpdatedFields.Add("" + TARGET + ".[" + f + "] = " + SOURCE + "." + FieldsInsert[i]);
                            IsInsertFields.Add(" " + SOURCE + "." + FieldsInsert[i]);
                        }
                        else
                        if (propfields.PropertyType.Equals(typeof(string)))
                        {
                            if (f.Equals("InvetoryPlanningMethod"))
                            {
                                IsTargetFields.Add("[" + f + "]");
                                IsUpdatedFields.Add("" + TARGET + ".[" + f + "] = " + SOURCE + "." + FieldsInsert[i]);
                                IsInsertFields.Add(" " + SOURCE + "." + FieldsInsert[i]);
                            }
                            else
                            {
                                IsTargetFields.Add("[" + f + "]");
                                IsUpdatedFields.Add("" + TARGET + ".[" + f + "] = " + SOURCE + "." + FieldsInsert[i]);
                                IsInsertFields.Add(" " + SOURCE + "." + FieldsInsert[i]);
                            }
                        }
                        else
                        if (propfields.PropertyType.Equals(typeof(Nullable<DateTime>)))
                        {
                            IsTargetFields.Add("[" + f + "]");
                            IsUpdatedFields.Add("" + TARGET + ".[" + f + "] = " + SOURCE + "." + FieldsInsert[i]);
                            IsInsertFields.Add(" " + SOURCE + "." + FieldsInsert[i]);
                        }
                        else
                        if (propfields.PropertyType.Equals(typeof(bool?)) || propfields.PropertyType.Equals(typeof(bool)))
                        {
                            IsTargetFields.Add("[" + f + "]");
                            IsUpdatedFields.Add("" + TARGET + ".[" + f + "] = " + SOURCE + "." + FieldsInsert[i]);
                            IsInsertFields.Add(" " + SOURCE + "." + FieldsInsert[i]);
                        }
                        // relation fields jika banyak model maka banyak if
                        //else
                        // if (propfields.PropertyType.Equals(typeof(List<EbridgingMaterialSubtitutionDetail>)))
                        //{

                        //}

                        ////else
                        //if (propfields.PropertyType.Equals(typeof(List<EbridgingRecipeStepMaterialAsociation>)))
                        //{
                        //    //IsTargetFields.Add("[" + f + "]");
                        //    //IsUpdatedFields.Add("" + TARGET + ".[" + f + "] = " + SOURCE + "." + FieldsInsert[i]);
                        //    //IsInsertFields.Add(" " + SOURCE + "." + FieldsInsert[i]);
                        //}
                        //else
                        //if (propfields.PropertyType.Equals(typeof(List<EBridgingMasterRoutingDetailStep>)))
                        //{
                        //    //IsTargetFields.Add("[" + f + "]");
                        //    //IsUpdatedFields.Add("" + TARGET + ".[" + f + "] = " + SOURCE + "." + FieldsInsert[i]);
                        //    //IsInsertFields.Add(" " + SOURCE + "." + FieldsInsert[i]);
                        //}
                        //else
                        //if (propfields.PropertyType.Equals(typeof(List<EbridgingMasterSpecificationTarget>)))
                        //{
                        //    //IsTargetFields.Add("[" + f + "]");
                        //    //IsUpdatedFields.Add("" + TARGET + ".[" + f + "] = " + SOURCE + "." + FieldsInsert[i]);
                        //    //IsInsertFields.Add(" " + SOURCE + "." + FieldsInsert[i]);
                        //}
                        //else
                        //if (propfields.PropertyType.Equals(typeof(List<EbridgingMasterSpecificationValRuleDetail>)))
                        //{
                        //    //IsTargetFields.Add("[" + f + "]");
                        //    //IsUpdatedFields.Add("" + TARGET + ".[" + f + "] = " + SOURCE + "." + FieldsInsert[i]);
                        //    //IsInsertFields.Add(" " + SOURCE + "." + FieldsInsert[i]);
                        //}
                        //else
                        //if (propfields.PropertyType.Equals(typeof(List<EbridgingMaterialSubtitutionDetail>)))
                        //{
                        //    //IsTargetFields.Add("[" + f + "]");
                        //    //IsUpdatedFields.Add("" + TARGET + ".[" + f + "] = " + SOURCE + "." + FieldsInsert[i]);
                        //    //IsInsertFields.Add(" " + SOURCE + "." + FieldsInsert[i]);
                        //}
                        //else
                        //if (propfields.PropertyType.Equals(typeof(List<EbridgingMasterOperationTroughtput>)))
                        //{
                        //    //IsTargetFields.Add("[" + f + "]");
                        //    //IsUpdatedFields.Add("" + TARGET + ".[" + f + "] = " + SOURCE + "." + FieldsInsert[i]);
                        //    //IsInsertFields.Add(" " + SOURCE + "." + FieldsInsert[i]);
                        //}
                        //else
                        //if (propfields.PropertyType.Equals(typeof(List<EbridgingMasterOperationActivities>)))
                        //{
                        //    //IsTargetFields.Add("[" + f + "]");
                        //    //IsUpdatedFields.Add("" + TARGET + ".[" + f + "] = " + SOURCE + "." + FieldsInsert[i]);
                        //    //IsInsertFields.Add(" " + SOURCE + "." + FieldsInsert[i]);
                        //}
                        //else
                        //if (propfields.PropertyType.Equals(typeof(List<EbridgingRecipeStepMaterialAsociation>)))
                        //{
                        //    //IsTargetFields.Add("[" + f + "]");
                        //    //IsUpdatedFields.Add("" + TARGET + ".[" + f + "] = " + SOURCE + "." + FieldsInsert[i]);
                        //    //IsInsertFields.Add(" " + SOURCE + "." + FieldsInsert[i]);
                        //}
                        //else
                        //if (propfields.PropertyType.Equals(typeof(List<EbridgingRecipeStepMaterialAsociation>)))
                        //{
                        //    //IsTargetFields.Add("[" + f + "]");
                        //    //IsUpdatedFields.Add("" + TARGET + ".[" + f + "] = " + SOURCE + "." + FieldsInsert[i]);
                        //    //IsInsertFields.Add(" " + SOURCE + "." + FieldsInsert[i]);
                        //}
                    }
                }
                catch (Exception ex)
                {
                    //Console.WriteLine(ex.Message);
                    ErrorLog.WriteToLog("AppException", "[MergeSQLFormatValueRelasi] - " + ex.Message);
                }
                i++;
            }

            var InsertQuery = " INSERT (" + string.Join(",", IsTargetFields.ToArray()) + ") VALUES ( " + ValueInsert + " ) ";
            var UpdateQuery = " UPDATE SET " + string.Join(",", IsUpdatedFields.ToArray());

            var SQLMerge = "  DECLARE " + NamaTableTemp + " " +
                            "  TABLE (" + string.Join(",", FieldsType.ToArray()) + " ) " +
                            "  INSERT INTO " + NamaTableTemp + " (" + string.Join(",", FieldsInsert.ToArray()) + ") VALUES ( " + QueryInsert + " ) " +
                            //   "  SET IDENTITY_INSERT " + NamaTableTarget + " OFF;" +
                            "  MERGE INTO " + NamaTableTarget + " WITH (HOLDLOCK) AS TARGET " +
                            "  USING " + NamaTableTemp + " AS SOURCE " +
                            "  ON " + UniqKey + " " +
                            //  "	 ON SOURCE.RECID = TARGET.RecId " +
                            "  WHEN MATCHED THEN " +
                            "  " + UpdateQuery + " " +
                            "  WHEN NOT MATCHED BY TARGET THEN  " +
                            "  " + InsertQuery + ";";
            //   "  SET IDENTITY_INSERT " + NamaTableTarget + " ON;";
            ErrorLog.WriteToLog("AppException", "[MergeSQLFormatValueRelasi] - " + SQLMerge);

            IsTargetFields.Clear();
            IsUpdatedFields.Clear();
            IsInsertFields.Clear();

            return SQLMerge;
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
