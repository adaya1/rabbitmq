﻿namespace MaterialService.API.Services.RabbitMQ.Publishers.SendJobList.Models
{
    public enum EnumJoblist : short
    {
        Create,
        Done,
    }

}
