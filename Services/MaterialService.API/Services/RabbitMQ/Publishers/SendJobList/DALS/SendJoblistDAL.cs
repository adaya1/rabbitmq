﻿using Master.Api.Helpers;
using MaterialService.Api.Services.RabbitMQ.Publishers.SendJobList.Interfaces;
using MaterialService.Api.Services.RabbitMQ.Publishers.SendJobList.Models;
using MaterialService.API.Helpers;
using Messagebus;
using MessageBus;
using Newtonsoft.Json.Linq;
using System;
using System.Threading.Tasks;

namespace MaterialService.Api.Services.RabbitMQ.Publishers.SendJobList.DALS
{
    public class SendJoblistDAL : ISendJoblist
    {
        public async Task<BaseResponse> publishCreateAsync(SendJoblistModels m)
        {
            BaseResponse result = new BaseResponse();
            try
            {
                IBus _busControl = RabbitHutch.CreateBus();
                var jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(m);
                var withoutSlash = JToken.Parse(jsonString);
                Console.WriteLine(withoutSlash);
                ErrorLog.WriteToLog("AppException", "["+ExChanges.MaterialJobListCreate+"]-"+withoutSlash);
                await _busControl.SendAsync(ExChanges.MaterialJobListCreate, withoutSlash);
                _busControl.Dispose();
                result.success = true;
                result.message = "Publish to RabitMQ";
            }
            catch
            {
                throw;
            }
            return result;
        }

        public async Task<BaseResponse> publishDoneAsync(SendJoblistModels m)
        {
            BaseResponse result = new BaseResponse();
            try
            {
                IBus _busControl = RabbitHutch.CreateBus();
                var jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(m);
                var withoutSlash = JToken.Parse(jsonString);
                await _busControl.SendAsync(ExChanges.MaterialJobListDone, withoutSlash);
                ErrorLog.WriteToLog("AppException", "[" + ExChanges.MaterialJobListDone + "]-" + withoutSlash);
                _busControl.Dispose();
                result.success = true;
                result.message = "Publish to RabitMQ";
            }
            catch
            {
                throw;
            }
            return result;
        }
    }
}
