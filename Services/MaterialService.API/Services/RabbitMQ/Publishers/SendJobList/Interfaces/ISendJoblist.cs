﻿
using MaterialService.Api.Services.RabbitMQ.Publishers.SendJobList.Models;
using MaterialService.API.Helpers;
using System.Threading.Tasks;

namespace MaterialService.Api.Services.RabbitMQ.Publishers.SendJobList.Interfaces
{
    public interface ISendJoblist
    {
        Task<BaseResponse> publishCreateAsync(SendJoblistModels m);

        Task<BaseResponse> publishDoneAsync(SendJoblistModels m);
    }
}
