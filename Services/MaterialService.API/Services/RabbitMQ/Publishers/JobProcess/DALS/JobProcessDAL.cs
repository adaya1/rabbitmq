﻿using MaterialService.Api.Services.RabbitMQ.Publishers.JobProcess.Interfaces;
using MaterialService.Api.Services.RabbitMQ.Publishers.SendJobList.Interfaces;
using MaterialService.Api.Services.RabbitMQ.Publishers.SendJobList.Models;
using MaterialService.API.Helpers;
using MaterialService.API.Helpers.Dapper;
using MaterialService.API.Services.Models.Base;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;

namespace MaterialService.Api.Services.RabbitMQ.Publishers.JobProcess.DALS
{
    public class JobProcessDAL : IJobProcess
    {
        private readonly IDapper _dapper;
        private readonly IConfiguration configuration;
        private readonly ISendJoblist _SendJoblist;

        public JobProcessDAL(ISendJoblist sendJoblist, IDapper dapper, IConfiguration _configuration)
        {
            _dapper = dapper;
            _SendJoblist = sendJoblist;
         
            configuration = _configuration;
        }


        public async Task<BaseResponse> PublishTest(BppBaseModel m)
        {
            BaseResponse JobResult = new BaseResponse();
            try
            {
                
                SendJoblistModels JBC = new SendJoblistModels();
                JBC.Message = m.Message;
                _ = await _SendJoblist.publishCreateAsync(JBC);
              
            }
            catch
            {
                throw new Exception("Publisher Test");
            }
            return JobResult;
        }
    }
}
