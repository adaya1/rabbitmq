﻿using MaterialService.Api.Services.RabbitMQ.Publishers.SendJobList.Models;
using MaterialService.API.Helpers;
using MaterialService.API.Services.Models.Base;
using MaterialService.API.Services.RabbitMQ.Subscriber.Werum.SemiFinishedGoodTransfer.Model;
using System.Threading.Tasks;

namespace MaterialService.Api.Services.RabbitMQ.Publishers.JobProcess.Interfaces
{
    public interface IJobProcess
    {
        Task<BaseResponse> PublishTest(BppBaseModel m);
    }
}
