﻿using MaterialService.API.Helpers;
using MaterialService.API.Services.RabbitMQ.Subscriber.Oracle.Wip.Models;
using System.Threading;
using System.Threading.Tasks;

namespace MaterialService.API.Services.RabbitMQ.Subscriber.Oracle.Wip.Interfaces
{
    public interface IWip
    {
        Task DoWork(CancellationToken stoppingToken);

        Task<BaseResponse> DidJob(WipIssueModel SourceModel);

        //
        Task<BaseResponse> TestWIP(WipIssueModel SourceModel);

    }
}
