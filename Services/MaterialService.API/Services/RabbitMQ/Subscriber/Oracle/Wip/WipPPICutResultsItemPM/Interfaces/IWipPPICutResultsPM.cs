﻿using MaterialService.API.Helpers;
using MaterialService.API.Services.Ebridging.EWIP.Model;
using System.Threading;
using System.Threading.Tasks;

namespace MaterialService.API.Services.RabbitMQ.Subscriber.Ebridging.Wip.WipPPICutResultsPM.Interfaces
{
    public interface IWipPPICutResultsPM
    {
        Task DoWork(CancellationToken stoppingToken);

        Task<BaseResponse> DidJob(WipModels SourceModel);
    }
}
