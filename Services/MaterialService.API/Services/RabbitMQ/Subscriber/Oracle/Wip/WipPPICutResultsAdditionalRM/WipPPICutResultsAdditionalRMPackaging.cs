﻿using MaterialService.API.Services.RabbitMQ.Subscriber.Ebridging.Wip.WipPPICutResultsAdditionalRM.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace MaterialService.API.Services.RabbitMQ.Subscriber.Ebridging.Wip.WipPPICutResultsAdditionalRM
{
    public class WipPPICutResultsAdditionalRMPackaging : BackgroundService
    {
        private readonly ILogger<WipPPICutResultsAdditionalRMPackaging> _logger;
        //private readonly IBus _busControl;
        // private readonly IMbrOlah _mbrOlah;
        public IServiceProvider _Services;


        public WipPPICutResultsAdditionalRMPackaging(IServiceProvider services, ILogger<WipPPICutResultsAdditionalRMPackaging> logger)
        {
            _logger = logger;
            // _busControl = RabbitHutch.CreateBus();
            _Services = services;

        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation(
                "Consume Scoped Service Hosted Service running.");

            await DoWork(stoppingToken);
        }

        private async Task DoWork(CancellationToken stoppingToken)
        {
            _logger.LogInformation(
                "Consume Scoped Service Hosted Service is working.");

            using (var scope = _Services.CreateScope())
            {
                var scopedProcessingService =
                    scope.ServiceProvider
                        .GetRequiredService<IWipPPICutResultsAdditionalRM>();

                await scopedProcessingService.DoWork(stoppingToken);
            }
        }

        public override async Task StopAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation(
                "Consume Scoped Service Hosted Service is stopping.");

            await base.StopAsync(stoppingToken);
        }

        
    }
}