﻿using Master.Api.Helpers;
using MaterialService.API.Helpers;
using MaterialService.API.Helpers.Dapper;
using MaterialService.API.Services.Ebridging.EWIP.Model;
using MaterialService.API.Services.RabbitMQ.Subscriber.Ebridging.Wip.WipPPICutResultsAdditionalRM.Interfaces;
using Messagebus;
using MessageBus;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace MaterialService.API.Services.RabbitMQ.Subscriber.Ebridging.Wip.WipPPICutResultsAdditionalRM.DALS
{
    public class WipPPICutResultsAdditionalRMProcessingServices : IWipPPICutResultsAdditionalRM
    {

        private readonly ILogger _logger;
        private readonly IBus _busControl;
        private readonly IDapper _dapper;

        private readonly string aErrorKey = "RBMQ WWipPPICutResultsAdditionalRMProcessingServices";
        private readonly string aDoWork = "DoWork";
        private readonly string aDiJob = "DidJob";
        private string _SQLDeclare;
        private string _SQLMerge;

        private readonly int defaultReceiveDelay;

        public WipPPICutResultsAdditionalRMProcessingServices(ILogger<WipPPICutResultsAdditionalRMProcessingServices> logger, IDapper dapper)
        {
            _logger = logger;
            _busControl = RabbitHutch.CreateBus();
            _dapper = dapper;
            defaultReceiveDelay = 1000;
        }

        public async Task DoWork(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                Console.WriteLine(Queue.OracleWIPIssue);
                await _busControl.ReceiveAsync<WipModels>(Queue.OracleWIPIssue, x =>
                {
                    try
                    {
                        _ = Task.Run(() => { _ = DidJob(x); }, stoppingToken);
                    }
                    catch (Exception ex)
                    {
                        ErrorLog.WriteToLog("AppException", "[" + aDoWork + " " + aErrorKey + "] " + x);
                    }
                });

                await Task.Delay(defaultReceiveDelay, stoppingToken);
            }

            _busControl.Dispose();
        }

        public async Task<BaseResponse> DidJob(WipModels SourceModel)
        {
            BaseResponse result = new BaseResponse();
            try
            {
                //tblO_RawMaterialIssue_Item, tblO_RawMaterialIssue_Item_Detail
                //var NameTable = "@TableMasterSpecificationHeader";
                //var TargetTable = "tblO_Spec_Header";
                //string SQLDeclare;

                //MasterSpecificationHeader Tg = new MasterSpecificationHeader();
                //Type tf_target = Tg.GetType();
                //CreateDeclareMergeOneMany(SourceModel, tf_target, NameTable, TargetTable, out SQLDeclare);
                var SQL = " SELECT @@VERSION ";

                //_SQLDeclare = SQLDeclare;
                int affectedRows;

                // result.data = 
                affectedRows = await Task.FromResult(_dapper.Execute(SQL));
                if (affectedRows > 0)
                {
                    ErrorLog.WriteToLog("AppException", "[" + aDiJob + " " + aErrorKey + "] - Berhasil " + _SQLDeclare);
                }
                else
                {
                    ErrorLog.WriteToLog("AppException", "[" + aDiJob + " " + aErrorKey + "] - Failed " + _SQLDeclare);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteToLog("AppException", ex.Message);
            }
            finally
            {
                _dapper.Dispose();
            }
            return result;


        }

        //private void CreateDeclareMergeOneMany(EbridgingMasterSpecificationHeader SourceModel, Type tf_target, string DeclareTable, string TargetTable, out string SQLMerge)
        //{
        //    try
        //    {
        //        var SQLMasterRecipeStepMaterialAsociation = "";

        //        List<string> FieldsType = new List<string>();
        //        List<string> Fields = new List<string>();
        //        List<string> InsertValues = new List<string>();

        //        MergeSQL _ms = new MergeSQL();
        //        Type _st = SourceModel.GetType();

        //        FieldsType = _ms.GetFieldType(_st);
        //        Fields = _ms.GetFieldSQL(_st);
        //        InsertValues = _ms.GetValueSQL(ref SourceModel, _st, out SQLMasterRecipeStepMaterialAsociation);
        //        Console.WriteLine(SQLMasterRecipeStepMaterialAsociation);

        //        var QueryRangeInsert = InsertValues.Where((value, index) => index >= 0 && index < InsertValues.Count).ToArray();
        //        var ValueNoRelasi = InsertValues.Where((value, index) => index >= 0 && index < InsertValues.Count).ToArray();

        //        //var QueryRangeInsert = InsertValues.Where((value, index) => index >= 0 && index < InsertValues.Count - 1).ToArray();
        //        //var ValueNoRelasi = InsertValues.Where((value, index) => index >= 0 && index < InsertValues.Count - 1).ToArray();


        //        var QueryInsert = string.Join(",", InsertValues.ToArray());
        //        Console.WriteLine(QueryInsert);
        //        var InsertNoRelasi = string.Join(",", QueryRangeInsert);

        //        var SQLRECIPEHEADER = _ms.MergeSQLFormat(tf_target, FieldsType, Fields, DeclareTable, TargetTable, InsertNoRelasi, QueryInsert);

        //        SQLMerge = SQLRECIPEHEADER + " " + SQLMasterRecipeStepMaterialAsociation;

        //        FieldsType.Clear();
        //        Fields.Clear();
        //        InsertValues.Clear();

        //        ErrorLog.WriteToLog("AppException", "[" + aDiJob + " " + aErrorKey + "] - " + SQLMerge);

        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}

    }
}
