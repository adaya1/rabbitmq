﻿using MaterialService.API.Services.RabbitMQ.Subscriber.Ebridging.Wip.WipPPICutResultsReturnPM.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace MaterialService.API.Services.RabbitMQ.Subscriber.Ebridging.Wip.WipPPICutResultsReturnPM
{
    public class WipPPICutResultsRMPackaging : BackgroundService
    {
        private readonly ILogger<WipPPICutResultsRMPackaging> _logger;
        
        public IServiceProvider _Services;

        public WipPPICutResultsRMPackaging(IServiceProvider services, ILogger<WipPPICutResultsRMPackaging> logger)
        {
            _logger = logger;
            _Services = services;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation(
                "Consume Scoped Service Hosted Service running.");

            await DoWork(stoppingToken);
        }

        private async Task DoWork(CancellationToken stoppingToken)
        {
            _logger.LogInformation(
                "Consume Scoped Service Hosted Service is working.");

            using (var scope = _Services.CreateScope())
            {
                var scopedProcessingService =
                    scope.ServiceProvider
                        .GetRequiredService<IWipPPICutResultsReturnPM>();

                await scopedProcessingService.DoWork(stoppingToken);
            }
        }

        public override async Task StopAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation(
                "Consume Scoped Service Hosted Service is stopping.");

            await base.StopAsync(stoppingToken);
        }
    }
}