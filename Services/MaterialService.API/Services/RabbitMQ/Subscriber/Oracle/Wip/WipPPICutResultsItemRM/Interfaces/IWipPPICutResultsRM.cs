﻿using MaterialService.API.Helpers;
using MaterialService.API.Services.Ebridging.EWIP.Model;
using System.Threading;
using System.Threading.Tasks;

namespace MaterialService.API.Services.RabbitMQ.Subscriber.Ebridging.Wip.WipPPICutResultsRM.Interfaces
{
    public interface IWipPPICutResultsRM
    {
        Task DoWork(CancellationToken stoppingToken);

        Task<BaseResponse> DidJob(WipModels SourceModel);
    }
}
