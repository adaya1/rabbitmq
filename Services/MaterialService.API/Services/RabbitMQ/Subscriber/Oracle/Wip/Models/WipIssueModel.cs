﻿using System;
using System.Collections.Generic;

namespace MaterialService.API.Services.RabbitMQ.Subscriber.Oracle.Wip.Models
{
    public class WipIssueModel
    {
        public int RECID { get; set; }
        public DateTime? RECTIMESTAMP { get; set; }
        public int? RECSTATUS { get; set; }
        public string TIMESTAMP { get; set; }
        public string ID { get; set; }
        public string TYPE { get; set; }
        public string SOURCE { get; set; }
        public int? ORGANIZATION_ID { get; set; }
        public string SCHEDULE_NUMBER { get; set; }
        public DateTime? UPDATE_DATE_BBK { get; set; }
        public string RECIPE { get; set; }
        public string BATCH_NO { get; set; }
        public DateTime? PRINT_DATE { get; set; }
        public string USER_PRINTED { get; set; }
        
        public List<WipIssueDetailModel> WIP_ISSUE_PPI_DETAIL_SCH { get; set; }

    }
}


//{
//    "RECID": 0,
//  "RECTIMESTAMP": "2023-09-08T19:07:51.6065588+07:00",
//  "RECSTATUS": 0,
//  "TIMESTAMP": "20230908190735-1182374",
//  "ID": "4202309081206396719",
//  "TYPE": "WIP_ISSUE_KP",
//  "SOURCE": "ERP-KLB",
//  "ORGANIZATION_ID": 87,
//  "SCHEDULE_NUMBER": "23007356",
//  "UPDATE_DATE_BBK": "2023-08-24T15:09:48",
//  "RECIPE": "TPRGR 05-11",
//  "BATCH_NO": "KTPRGR34647",
//  "PRINT_DATE": "2023-09-08T00:00:00",
//  "USER_PRINTED": "KLB.SERVICES",
//  "WIP_ISSUE_PPI_DETAIL_SCH": [
//    {
//        "RECID": 456,
//      "RECTIMESTAMP": "2023-09-08T19:07:51.6099476+07:00",
//      "RECSTATUS": 0,
//      "WIP_ISSUE_ID": 45,
//      "NO": 4,
//      "ITEM_CODE": "1KDP14230",
//      "ITEM_DESCRIPTION": "DOOS PROMAG TAB / 3X10",
//      "JUMLAH_LOT": 1,
//      "PLAN_QTY": 40800,
//      "WIP_PLAN_QTY": 40800,
//      "QUANTITY": 0,
//      "UOM_SCHEDULE": "EA",
//      "BATCH_NO": null,
//      "QUANTITY_BATCH": 40800,
//      "SUM_QUANTITY_BATCH": 0,
//      "ROUNDING": 0,
//      "EXP_DATE": "2023-12-21T00:00:00",
//      "LPN_NO": "",
//      "SERVICE_WORKER": "",
//      "SCALLING_CODE": "",
//      "WEIGHER": "",
//      "COORDINATOR": "",
//      "HANDOVER_TIME": "",
//      "CHECKED_BY_WHS_SPV": "",
//      "CHECKED_BY_PROD_COOR": "",
//      "DELIVERED_BY": "",
//      "RECEIVED_BY": ""
//    },
//    {
//        "RECID": 457,
//      "RECTIMESTAMP": "2023-09-08T19:07:51.6148428+07:00",
//      "RECSTATUS": 0,
//      "WIP_ISSUE_ID": 45,
//      "NO": 5,
//      "ITEM_CODE": "1WWP08520",
//      "ITEM_DESCRIPTION": "PL OVERWRAP BOPP HSL21/186",
//      "JUMLAH_LOT": 1,
//      "PLAN_QTY": 2980,
//      "WIP_PLAN_QTY": 2980,
//      "QUANTITY": 0,
//      "UOM_SCHEDULE": "M",
//      "BATCH_NO": null,
//      "QUANTITY_BATCH": 3000,
//      "SUM_QUANTITY_BATCH": 0,
//      "ROUNDING": 0,
//      "EXP_DATE": "2023-11-25T00:00:00",
//      "LPN_NO": "",
//      "SERVICE_WORKER": "",
//      "SCALLING_CODE": "",
//      "WEIGHER": "",
//      "COORDINATOR": "",
//      "HANDOVER_TIME": "",
//      "CHECKED_BY_WHS_SPV": "",
//      "CHECKED_BY_PROD_COOR": "",
//      "DELIVERED_BY": "",
//      "RECEIVED_BY": ""
//    },
//    {
//        "RECID": 458,
//      "RECTIMESTAMP": "2023-09-08T19:07:51.6174313+07:00",
//      "RECSTATUS": 0,
//      "WIP_ISSUE_ID": 45,
//      "NO": 6,
//      "ITEM_CODE": "1KDX11880",
//      "ITEM_DESCRIPTION": "MB PROMAG / 264 X 288 X 359 MM",
//      "JUMLAH_LOT": 1,
//      "PLAN_QTY": 224,
//      "WIP_PLAN_QTY": 224,
//      "QUANTITY": 0,
//      "UOM_SCHEDULE": "EA",
//      "BATCH_NO": null,
//      "QUANTITY_BATCH": 224,
//      "SUM_QUANTITY_BATCH": 0,
//      "ROUNDING": 0,
//      "EXP_DATE": "2024-06-16T00:00:00",
//      "LPN_NO": "",
//      "SERVICE_WORKER": "",
//      "SCALLING_CODE": "",
//      "WEIGHER": "",
//      "COORDINATOR": "",
//      "HANDOVER_TIME": "",
//      "CHECKED_BY_WHS_SPV": "",
//      "CHECKED_BY_PROD_COOR": "",
//      "DELIVERED_BY": "",
//      "RECEIVED_BY": ""
//    }
//  ]
//}