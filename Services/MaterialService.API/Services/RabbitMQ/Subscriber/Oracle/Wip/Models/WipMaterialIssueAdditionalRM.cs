﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace MaterialService.API.Services.RabbitMQ.Subscriber.Oracle.Wip.Models
{
    public class WipMaterialIssueAdditionalRM
    {

        //public long? TransactId { get; set; }
        public DateTime? TransactCreateDate { get; set; }
        public DateTime? TransactUpdateDate { get; set; }
        public long? RecId { get; set; }
        public DateTime? RecTimestamp { get; set; }
        public int? RecStatus { get; set; }
        public string Type { get; set; }
        public string Source { get; set; }
        public string Id { get; set; }
        [JsonIgnore]
        public long? EbrId { get; set; }
        public string WoNumber { get; set; }
        public string BatchNumber { get; set; }
        [JsonIgnore]
        public string ProductCode { get; set; }
        [JsonIgnore]
        public string ProductName { get; set; }
        public int? OrganizationId { get; set; }
        public DateTime? UpdateDateBBK { get; set; }
        public string Recipe { get; set; }
        public DateTime? PrintDate { get; set; }
        public string UserPrinted { get; set; }
        public string Timestamp { get; set; }
        public int? LastApprovalSequence { get; set; }
        public int? Status { get; set; }
        public bool? IsFirstTimeDownloading { get; set; }

        public List<WipMaterialIssueAdditionalDetailRM> WIP_ISSUE_PPI_DETAIL_SCH { get; set; }

    }

}
