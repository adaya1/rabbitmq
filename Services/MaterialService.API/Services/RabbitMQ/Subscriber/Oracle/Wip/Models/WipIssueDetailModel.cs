﻿using System;

namespace MaterialService.API.Services.RabbitMQ.Subscriber.Oracle.Wip.Models
{
    public class WipIssueDetailModel
    {
        public int? RECID { get; set; }
        public DateTime? RECTIMESTAMP { get; set; }
        public int? RECSTATUS { get; set; }
        public int? WIP_ISSUE_ID { get; set; }
        public int? NO { get; set; }
        public string ITEM_CODE { get; set; }
        public string ITEM_DESCRIPTION { get; set; }
        public int? JUMLAH_LOT { get; set; }
        public double? PLAN_QTY { get; set; }
        public double? WIP_PLAN_QTY { get; set; }
        public decimal? QUANTITY { get; set; }
        public string UOM_SCHEDULE { get; set; }
        public string BATCH_NO { get; set; }
        public double? QUANTITY_BATCH { get; set; }
        public decimal? SUM_QUANTITY_BATCH { get; set; }
        public decimal? ROUNDING { get; set; }
        public DateTime? EXP_DATE { get; set; }
        public string LPN_NO { get; set; }
        public string SERVICE_WORKER { get; set; }
        public string SCALLING_CODE { get; set; }
        public string WEIGHER { get; set; }
        public string COORDINATOR { get; set; }
        public DateTime? HANDOVER_TIME { get; set; }
        public string CHECKED_BY_WHS_SPV { get; set; }
        public string CHECKED_BY_PROD_COOR { get; set; }
        public string DELIVERED_BY { get; set; }
        public string RECEIVED_BY { get; set; }

//         "RECID": 456,
//      "RECTIMESTAMP": "2023-09-08T19:07:51.6099476+07:00",
//      "RECSTATUS": 0,
//      "WIP_ISSUE_ID": 45,
//      "NO": 4,
//      "ITEM_CODE": "1KDP14230",
//      "ITEM_DESCRIPTION": "DOOS PROMAG TAB / 3X10",
//      "JUMLAH_LOT": 1,
//      "PLAN_QTY": 40800,
//      "WIP_PLAN_QTY": 40800,
//      "QUANTITY": 0,
//      "UOM_SCHEDULE": "EA",
//      "BATCH_NO": null,
//      "QUANTITY_BATCH": 40800,
//      "SUM_QUANTITY_BATCH": 0,
//      "ROUNDING": 0,
//      "EXP_DATE": "2023-12-21T00:00:00",
//      "LPN_NO": "",
//      "SERVICE_WORKER": "",
//      "SCALLING_CODE": "",
//      "WEIGHER": "",
//      "COORDINATOR": "",
//      "HANDOVER_TIME": "",
//      "CHECKED_BY_WHS_SPV": "",
//      "CHECKED_BY_PROD_COOR": "",
//      "DELIVERED_BY": "",
//      "RECEIVED_BY": ""
    }
}
