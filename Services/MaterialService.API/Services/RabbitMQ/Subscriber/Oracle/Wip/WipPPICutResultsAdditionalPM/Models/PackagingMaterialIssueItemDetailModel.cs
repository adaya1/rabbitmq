﻿using System;

namespace MaterialService.API.Services.RabbitMQ.Subscriber.Ebridging.Wip.WipPPICutResultsAdditionalPM.Models
{
    public class PackagingMaterialIssueItemDetailModel
    {
        public int? RECID { get; set; }
        public DateTime? RECTIMESTAMP { get; set; }
        public int? RECSTATUS { get; set; }
        public int? WIP_ISSUE_ID { get; set; }
        public int? NO { get; set; }
        public string ITEM_CODE { get; set; }
        public string ITEM_DESCRIPTION { get; set; }
        public int? JUMLAH_LOT { get; set; }
        public double? PLAN_QTY { get; set; }
        public double? WIP_PLAN_QTY { get; set; }
        public int? QUANTITY { get; set; }
        public string UOM_SCHEDULE { get; set; }
        public string BATCH_NO { get; set; }
        public double? QUANTITY_BATCH { get; set; }
        public int? SUM_QUANTITY_BATCH { get; set; }
        public int? ROUNDING { get; set; }
        public DateTime? EXP_DATE { get; set; }
        public string LPN_NO { get; set; }
        public string SERVICE_WORKER { get; set; }
        public string SCALLING_CODE { get; set; }
        public string WEIGHER { get; set; }
        public string COORDINATOR { get; set; }
        public string HANDOVER_TIME { get; set; }
        public string CHECKED_BY_WHS_SPV { get; set; }
        public string CHECKED_BY_PROD_COOR { get; set; }
        public string DELIVERED_BY { get; set; }
        public string RECEIVED_BY { get; set; }
    }
}
