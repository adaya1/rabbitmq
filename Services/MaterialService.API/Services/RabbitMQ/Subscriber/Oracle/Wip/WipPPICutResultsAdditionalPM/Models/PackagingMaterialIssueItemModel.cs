﻿using System;
using System.Collections.Generic;

namespace MaterialService.API.Services.RabbitMQ.Subscriber.Ebridging.Wip.WipPPICutResultsAdditionalPM.Models
{
    public class PackagingMaterialIssueItemModel
    {
        public int RECID { get; set; }
        public DateTime? RECTIMESTAMP { get; set; }
        public int? RECSTATUS { get; set; }
        public string TIMESTAMP { get; set; }
        public string ID { get; set; }
        public string TYPE { get; set; }
        public string SOURCE { get; set; }
        public int? ORGANIZATION_ID { get; set; }
        public string SCHEDULE_NUMBER { get; set; }
        public DateTime? UPDATE_DATE_BBK { get; set; }
        public string RECIPE { get; set; }
        public string BATCH_NO { get; set; }
        public DateTime? PRINT_DATE { get; set; }
        public string USER_PRINTED { get; set; }

        public List<PackagingMaterialIssueItemDetailModel> WIP_ISSUE_PPI_DETAIL_SCH { get; set; }
    }
}
