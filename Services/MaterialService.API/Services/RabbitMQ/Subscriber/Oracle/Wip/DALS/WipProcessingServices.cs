﻿using Master.Api.Helpers;
using MaterialService.API.Helpers;
using MaterialService.API.Helpers.Dapper;
using MaterialService.API.Services.RabbitMQ.Subscriber.Oracle.Wip.Interfaces;
using MaterialService.API.Services.RabbitMQ.Subscriber.Oracle.Wip.Models;
using Messagebus;
using MessageBus;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MaterialService.API.Services.RabbitMQ.Subscriber.Oracle.Wip.DALS
{
    public class WipProcessingServices : IWip
    {
        private readonly ILogger _logger;
        private readonly IBus _busControl;
        private readonly IDapper _dapper;

        private readonly string aErrorKey = "Wip";
        private readonly string aDoWork = "DoWork";
        private readonly string aDiJob = "DidJob";
        private string _SQLDeclare;
        private string _SQLMerge;

        private readonly int defaultReceiveDelay;

        public WipProcessingServices(ILogger<WipProcessingServices> logger, IDapper dapper)
        {
            _logger = logger;
            _busControl = RabbitHutch.CreateBus();
            _dapper = dapper;
            defaultReceiveDelay = 10000;
        }

        public async Task<BaseResponse> DidJob(WipIssueModel SourceModel)
        {
            BaseResponse result = new BaseResponse();
            try
            {
                // Hasil Potong PPI (WIP Issue) RM
                string ID = SourceModel.ID;
                Console.WriteLine("Proces ID :" + ID);
                string SubID = ID.Substring(0, 1);

                #region Material item RM 1
                if (SubID == "1")
                {
                    string ITEM = "";

                    string SubItemType = ID.Substring(0, 2);
                    if (SubItemType == "11")
                    {
                        ITEM = "BAHAN NON COATING";
                    }
                    else
                    {
                        ITEM = "BAHAN COATING";
                    }

                    WipMaterialIssueItemRM _Model = new WipMaterialIssueItemRM();

                    _Model.RecId = SourceModel.RECID;
                    _Model.RecTimestamp = SourceModel.RECTIMESTAMP;
                    _Model.RecStatus = SourceModel.RECSTATUS;
                    _Model.Timestamp = SourceModel.TIMESTAMP;
                    _Model.Id = SourceModel.ID;
                    _Model.Type = SourceModel.TYPE;
                    _Model.Source = SourceModel.SOURCE;
                    _Model.OrganizationId = SourceModel.ORGANIZATION_ID;
                    _Model.PrintDate = SourceModel.PRINT_DATE;
                    _Model.UserPrinted = SourceModel.USER_PRINTED;
                    _Model.UpdateDateBBK = SourceModel.UPDATE_DATE_BBK;
                    _Model.TransactCreateDate = SourceModel.RECTIMESTAMP;
                    _Model.TransactUpdateDate = SourceModel.RECTIMESTAMP;
                    _Model.Status = 2;
                    _Model.Recipe = SourceModel.RECIPE;
                    _Model.BatchNumber = SourceModel.BATCH_NO;
                    _Model.WoNumber = SourceModel.SCHEDULE_NUMBER;
                    _Model.BatchNumber = SourceModel.BATCH_NO;
                    _Model.IsFirstTimeDownloading = true;

                    //_Model.ProductCode = null;
                    //_Model.ProductName = null;
                    //_Model.EbrId = null;

                    List<WipMaterialIssueItemDetailRM> WIP_ISSUE_PPI_DETAIL_SCH = new List<WipMaterialIssueItemDetailRM>();
                    if (SourceModel.WIP_ISSUE_PPI_DETAIL_SCH.Count > 0)
                    {
                        foreach (var i in SourceModel.WIP_ISSUE_PPI_DETAIL_SCH)
                        {
                            WipMaterialIssueItemDetailRM Detail = new WipMaterialIssueItemDetailRM();
                            Detail.RecId = (long)i.RECID;
                            Detail.RecTimestamp = i.RECTIMESTAMP;
                            Detail.RecStatus = (short?)i.RECSTATUS;
                            Detail.TransactCreateDate = i.RECTIMESTAMP;
                            Detail.TransactUpdateDate = i.RECTIMESTAMP;
                            Detail.ItemCode = i.ITEM_CODE;
                            Detail.ItemDescription = i.ITEM_DESCRIPTION;
                            Detail.JumlahLot = i.JUMLAH_LOT;
                            Detail.PlanQTY = (decimal?)i.PLAN_QTY;
                            Detail.WipPlanQTY = (decimal?)i.WIP_PLAN_QTY;
                            Detail.Quantity = (decimal?)i.QUANTITY;
                            Detail.UOMSchedule = i.UOM_SCHEDULE;
                            Detail.QuantityBatch = (decimal?)i.QUANTITY_BATCH;
                            Detail.SumQtyBatch = i.SUM_QUANTITY_BATCH;
                            Detail.Rounding = (int?)i.ROUNDING;
                            Detail.ExpireDate = i.EXP_DATE;
                            Detail.LPN = i.LPN_NO;
                            Detail.ServiceWorker = i.SERVICE_WORKER;
                            Detail.ScalingCode = i.SCALLING_CODE;
                            Detail.Weigher = i.WEIGHER;
                            Detail.Coordinator = i.COORDINATOR;
                            Detail.HandoverTime = i.HANDOVER_TIME;
                            Detail.CheckedByWhsSpv = i.CHECKED_BY_WHS_SPV;
                            Detail.CheckedByProdCoor = i.CHECKED_BY_PROD_COOR;
                            Detail.DeliveredBy = i.DELIVERED_BY;
                            Detail.ReceivedBy = i.RECEIVED_BY;
                            Detail.HeaderRecId = i.WIP_ISSUE_ID;
                            Detail.ItemType = ITEM;
                            //Detail. = i.BATCH_NO;
                            //Detail. = i.WIP_ISSUE_ID;
                            //Detail. = i.NO;
                            WIP_ISSUE_PPI_DETAIL_SCH.Add(Detail);
                        }
                    }

                    _Model.WIP_ISSUE_PPI_DETAIL_SCH = WIP_ISSUE_PPI_DETAIL_SCH;

                    //tblO_RawMaterialIssue_Item, tblO_RawMaterialIssue_Item_Detail
                    var NameTable = "@TableRawMaterialIssueItem";
                    var TargetTable = "tblO_RawMaterialIssue_Item";
                    string SQLDeclare;
                    string RecId;

                    WipMaterialIssueItemRM Tg = new WipMaterialIssueItemRM();
                    Type tf_target = Tg.GetType();
                    CreateDeclareMergeOneManyWipIssueRM(_Model, tf_target, NameTable, TargetTable, out SQLDeclare, out RecId);
                    _SQLDeclare = SQLDeclare;

                    int affectedRows;
                    affectedRows = await Task.FromResult(_dapper.Execute(SQLDeclare));
                    if (affectedRows > 0)
                    {
                        ErrorLog.WriteToLog("MaterialitemRM", "[" + aDiJob + " " + aErrorKey + "] - Berhasil " + _SQLDeclare);
                    }
                    else
                    {
                        ErrorLog.WriteToLog("MaterialitemRM", "[" + aDiJob + " " + aErrorKey + "] - Failed " + _SQLDeclare);
                    }
                }
                #endregion
                else

                #region issue addtional RM 2
                // Hasil Potong PPI (WIP Issue - Additional) RM
                if (SubID == "2")
                {
                    string SubItemType = ID.Substring(0, 2);
                    string ITEM = "";
                    if (SubItemType == "21")
                    {
                        ITEM = "BAHAN COATING";
                    }
                    else
                    {
                        ITEM = "BAHAN NON COATING";
                    }

                    //tblO_RawMaterialRequest_Additional, tblO_RawMaterialRequest_Additional_Detail
                    WipMaterialIssueAdditionalRM _Model = new WipMaterialIssueAdditionalRM();

                    _Model.RecId = SourceModel.RECID;
                    _Model.RecTimestamp = SourceModel.RECTIMESTAMP;
                    _Model.RecStatus = SourceModel.RECSTATUS;
                    _Model.Timestamp = SourceModel.TIMESTAMP;
                    _Model.Id = SourceModel.ID;
                    _Model.Type = SourceModel.TYPE;
                    _Model.Source = SourceModel.SOURCE;
                    _Model.OrganizationId = SourceModel.ORGANIZATION_ID;
                    _Model.Recipe = SourceModel.RECIPE;
                    _Model.BatchNumber = SourceModel.BATCH_NO;
                    _Model.PrintDate = SourceModel.PRINT_DATE;
                    _Model.UserPrinted = SourceModel.USER_PRINTED;
                    _Model.UpdateDateBBK = SourceModel.UPDATE_DATE_BBK;
                    _Model.TransactCreateDate = SourceModel.RECTIMESTAMP;
                    _Model.TransactUpdateDate = SourceModel.RECTIMESTAMP;
                    _Model.WoNumber = SourceModel.SCHEDULE_NUMBER;
                    _Model.BatchNumber = SourceModel.BATCH_NO;
                    _Model.Status = 2;
                    _Model.IsFirstTimeDownloading = true;
                    //MaterialIssueRM. = SourceModel.SCHEDULE_NUMBER;
                    //MaterialIssuePM.ProductCode = null;
                    //MaterialIssuePM.ProductName = null;
                    //MaterialIssuePM.EbrId = null;

                    List<WipMaterialIssueAdditionalDetailRM> WIP_ISSUE_PPI_DETAIL_SCH = new List<WipMaterialIssueAdditionalDetailRM>();
                    if (SourceModel.WIP_ISSUE_PPI_DETAIL_SCH.Count > 0)
                    {
                        foreach (var i in SourceModel.WIP_ISSUE_PPI_DETAIL_SCH)
                        {
                            WipMaterialIssueAdditionalDetailRM Detail = new WipMaterialIssueAdditionalDetailRM();
                            Detail.RecId = (long)i.RECID;
                            Detail.RecTimestamp = i.RECTIMESTAMP;
                            Detail.RecStatus = (short?)i.RECSTATUS;
                            Detail.TransactCreateDate = i.RECTIMESTAMP;
                            Detail.TransactUpdateDate = i.RECTIMESTAMP;
                            Detail.ItemCode = i.ITEM_CODE;
                            Detail.ItemDescription = i.ITEM_DESCRIPTION;
                            Detail.JumlahLot = i.JUMLAH_LOT;
                            Detail.PlanQTY = (decimal?)i.PLAN_QTY;
                            Detail.WipPlanQTY = (decimal?)i.WIP_PLAN_QTY;
                            Detail.Quantity = (decimal?)i.QUANTITY;
                            Detail.UOMSchedule = i.UOM_SCHEDULE;
                            Detail.QuantityBatch = (decimal?)i.QUANTITY_BATCH;
                            Detail.SumQtyBatch = i.SUM_QUANTITY_BATCH;
                            Detail.Rounding = (int?)i.ROUNDING;
                            Detail.ExpireDate = i.EXP_DATE;
                            Detail.LPN = i.LPN_NO;
                            Detail.ServiceWorker = i.SERVICE_WORKER;
                            Detail.ScalingCode = i.SCALLING_CODE;
                            Detail.Weigher = i.WEIGHER;
                            Detail.Coordinator = i.COORDINATOR;
                            Detail.HandoverTime = i.HANDOVER_TIME;
                            Detail.CheckedByWhsSpv = i.CHECKED_BY_WHS_SPV;
                            Detail.CheckedByProdCoor = i.CHECKED_BY_PROD_COOR;
                            Detail.DeliveredBy = i.DELIVERED_BY;
                            Detail.ReceivedBy = i.RECEIVED_BY;
                            Detail.HeaderRecId = i.WIP_ISSUE_ID;
                            Detail.ItemType = ITEM;

                            //Detail. = i.BATCH_NO;
                            //Detail. = i.WIP_ISSUE_ID;
                            //Detail. = i.NO;
                            WIP_ISSUE_PPI_DETAIL_SCH.Add(Detail);
                        }
                    }

                    _Model.WIP_ISSUE_PPI_DETAIL_SCH = WIP_ISSUE_PPI_DETAIL_SCH;

                    var NameTable = "@TableRawMaterialIssueAdditional";
                    var TargetTable = "tblO_RawMaterialIssue_Additional";
                    string SQLDeclare;
                    string RecId;

                    WipMaterialIssueAdditionalRM Tg = new WipMaterialIssueAdditionalRM();
                    Type tf_target = Tg.GetType();
                    CreateDeclareMergeOneManyWipIssueAdditionalRM(_Model, tf_target, NameTable, TargetTable, out SQLDeclare, out RecId);
                    _SQLDeclare = SQLDeclare;

                    int affectedRows;
                    affectedRows = await Task.FromResult(_dapper.Execute(SQLDeclare));
                    if (affectedRows > 0)
                    {
                        ErrorLog.WriteToLog("issueaddtionalRM", "[" + aDiJob + " " + aErrorKey + "] - Berhasil " + _SQLDeclare);
                    }
                    else
                    {
                        ErrorLog.WriteToLog("issueaddtionalRM", "[" + aDiJob + " " + aErrorKey + "] - Failed " + _SQLDeclare);
                    }

                }
                #endregion
                else

                #region issue return RM 3
                //// Hasil Potong PPI (WIP Issue - Return) RM
                if (SubID == "3")
                {
                    //tblO_RawMaterialRequest_Additional, tblO_RawMaterialRequest_Additional_Detail
                    WipMaterialIssueReturnRM _Model = new WipMaterialIssueReturnRM();

                    _Model.RecId = SourceModel.RECID;
                    _Model.RecTimestamp = SourceModel.RECTIMESTAMP;
                    _Model.RecStatus = SourceModel.RECSTATUS;
                    _Model.Timestamp = SourceModel.TIMESTAMP;
                    _Model.Id = SourceModel.ID;
                    _Model.Type = SourceModel.TYPE;
                    _Model.Source = SourceModel.SOURCE;
                    _Model.OrganizationId = SourceModel.ORGANIZATION_ID;
                    _Model.Recipe = SourceModel.RECIPE;
                    _Model.BatchNumber = SourceModel.BATCH_NO;
                    _Model.PrintDate = SourceModel.PRINT_DATE;
                    _Model.UserPrinted = SourceModel.USER_PRINTED;
                    _Model.UpdateDateBBK = SourceModel.UPDATE_DATE_BBK;
                    _Model.TransactCreateDate = SourceModel.RECTIMESTAMP;
                    _Model.TransactUpdateDate = SourceModel.RECTIMESTAMP;
                    _Model.WoNumber = SourceModel.SCHEDULE_NUMBER;
                    _Model.BatchNumber = SourceModel.BATCH_NO;
                    _Model.Status = 2;
                    //MaterialIssueRM. = SourceModel.SCHEDULE_NUMBER;
                    //MaterialIssuePM.ProductCode = null;
                    //MaterialIssuePM.ProductName = null;
                    //MaterialIssuePM.EbrId = null;

                    List<WipMaterialIssueReturnDetailRM> WIP_ISSUE_PPI_DETAIL_SCH = new List<WipMaterialIssueReturnDetailRM>();
                    if (SourceModel.WIP_ISSUE_PPI_DETAIL_SCH.Count > 0)
                    {
                        foreach (var i in SourceModel.WIP_ISSUE_PPI_DETAIL_SCH)
                        {
                            WipMaterialIssueReturnDetailRM detail = new WipMaterialIssueReturnDetailRM();
                            detail.RecId = (long)i.RECID;
                            detail.RecTimestamp = i.RECTIMESTAMP;
                            detail.RecStatus = (short?)i.RECSTATUS;
                            detail.TransactCreateDate = i.RECTIMESTAMP;
                            detail.TransactUpdateDate = i.RECTIMESTAMP;
                            detail.ItemCode = i.ITEM_CODE;
                            detail.ItemDescription = i.ITEM_DESCRIPTION;
                            detail.JumlahLot = i.JUMLAH_LOT;
                            detail.PlanQTY = (decimal?)i.PLAN_QTY;
                            detail.WipPlanQTY = (decimal?)i.WIP_PLAN_QTY;
                            detail.Quantity = i.QUANTITY;
                            detail.UOMSchedule = i.UOM_SCHEDULE;
                            detail.QuantityBatch = (decimal?)i.QUANTITY_BATCH;
                            detail.SumQtyBatch = i.SUM_QUANTITY_BATCH;
                            detail.Rounding = (int?)i.ROUNDING;
                            detail.ExpireDate = i.EXP_DATE;
                            detail.LPN = i.LPN_NO;
                            detail.ServiceWorker = i.SERVICE_WORKER;
                            detail.ScalingCode = i.SCALLING_CODE;
                            detail.Weigher = i.WEIGHER;
                            detail.Coordinator = i.COORDINATOR;
                            detail.HandoverTime = i.HANDOVER_TIME;
                            detail.CheckedByWhsSpv = i.CHECKED_BY_WHS_SPV;
                            detail.CheckedByProdCoor = i.CHECKED_BY_PROD_COOR;
                            detail.DeliveredBy = i.DELIVERED_BY;
                            detail.ReceivedBy = i.RECEIVED_BY;
                            detail.ItemType = "";
                            detail.HeaderRecId = SourceModel.RECID;
                            detail.HeaderId = i.WIP_ISSUE_ID;
                            WIP_ISSUE_PPI_DETAIL_SCH.Add(detail);
                        }
                    }

                    _Model.WIP_ISSUE_PPI_DETAIL_SCH = WIP_ISSUE_PPI_DETAIL_SCH;

                    var NameTable = "@TableRawMaterialIssueReturn";
                    var TargetTable = "tblO_RawMaterialIssue_Return";
                    string SQLDeclare;
                    string RecId;

                    WipMaterialIssueReturnRM Tg = new WipMaterialIssueReturnRM();
                    Type tf_target = Tg.GetType();
                    CreateDeclareMergeOneManyWipIssueReturnRM(_Model, tf_target, NameTable, TargetTable, out SQLDeclare, out RecId);
                    _SQLDeclare = SQLDeclare;

                    int affectedRows;
                    affectedRows = await Task.FromResult(_dapper.Execute(SQLDeclare));
                    if (affectedRows > 0)
                    {
                        ErrorLog.WriteToLog("issuereturnRM", "[" + aDiJob + " " + aErrorKey + "] - Berhasil " + _SQLDeclare);
                    }
                    else
                    {
                        ErrorLog.WriteToLog("issuereturnRM", "[" + aDiJob + " " + aErrorKey + "] - Failed " + _SQLDeclare);
                    }

                }
                #endregion
                else

                #region material Item PM 4
                if (SubID == "4")
                {

                    WipMaterialIssueItemPM _Model = new WipMaterialIssueItemPM();

                    _Model.RecId = SourceModel.RECID;
                    _Model.RecTimestamp = SourceModel.RECTIMESTAMP;
                    _Model.RecStatus = SourceModel.RECSTATUS;
                    _Model.Timestamp = SourceModel.TIMESTAMP;
                    _Model.Id = SourceModel.ID;
                    _Model.Type = SourceModel.TYPE;
                    _Model.Source = SourceModel.SOURCE;
                    _Model.OrganizationId = SourceModel.ORGANIZATION_ID;
                    _Model.PrintDate = SourceModel.PRINT_DATE;
                    _Model.UserPrinted = SourceModel.USER_PRINTED;
                    _Model.UpdateDateBBK = SourceModel.UPDATE_DATE_BBK;
                    _Model.TransactCreateDate = SourceModel.RECTIMESTAMP;
                    _Model.TransactUpdateDate = SourceModel.RECTIMESTAMP;
                    _Model.Status = 2;
                    _Model.Recipe = SourceModel.RECIPE;
                    _Model.BatchNumber = SourceModel.BATCH_NO;
                    _Model.WoNumber = SourceModel.SCHEDULE_NUMBER;
                    _Model.BatchNumber = SourceModel.BATCH_NO;
                    _Model.IsFirstTimeDownloading = true;

                    //_Model.ProductCode = null;
                    //_Model.ProductName = null;
                    //_Model.EbrId = null;
                    //MaterialIssueRM. = SourceModel.SCHEDULE_NUMBER;

                    List<WipMaterialIssueItemDetailPM> WIP_ISSUE_PPI_DETAIL_SCH = new List<WipMaterialIssueItemDetailPM>();
                    if (SourceModel.WIP_ISSUE_PPI_DETAIL_SCH.Count > 0)
                    {
                        foreach (var i in SourceModel.WIP_ISSUE_PPI_DETAIL_SCH)
                        {
                            WipMaterialIssueItemDetailPM detail = new WipMaterialIssueItemDetailPM();
                            detail.RecId = (long)i.RECID;
                            detail.RecTimestamp = i.RECTIMESTAMP;
                            detail.RecStatus = (short?)i.RECSTATUS;
                            detail.TransactCreateDate = i.RECTIMESTAMP;
                            detail.TransactUpdateDate = i.RECTIMESTAMP;
                            detail.ItemCode = i.ITEM_CODE;
                            detail.ItemDescription = i.ITEM_DESCRIPTION;
                            detail.JumlahLot = i.JUMLAH_LOT;
                            detail.PlanQTY = (decimal?)i.PLAN_QTY;
                            detail.WipPlanQTY = (decimal?)i.WIP_PLAN_QTY;
                            detail.Quantity = i.QUANTITY;
                            detail.UOMSchedule = i.UOM_SCHEDULE;
                            detail.QuantityBatch = (decimal?)i.QUANTITY_BATCH;
                            detail.SumQtyBatch = i.SUM_QUANTITY_BATCH;
                            detail.Rounding = (int?)i.ROUNDING;
                            detail.ExpireDate = i.EXP_DATE;
                            detail.LPN = i.LPN_NO;
                            detail.ServiceWorker = i.SERVICE_WORKER;
                            detail.ScalingCode = i.SCALLING_CODE;
                            detail.Weigher = i.WEIGHER;
                            detail.Coordinator = i.COORDINATOR;
                            detail.HandoverTime = i.HANDOVER_TIME;
                            detail.CheckedByWhsSpv = i.CHECKED_BY_WHS_SPV;
                            detail.CheckedByProdCoor = i.CHECKED_BY_PROD_COOR;
                            detail.DeliveredBy = i.DELIVERED_BY;
                            detail.ReceivedBy = i.RECEIVED_BY;
                            detail.ItemType = "WADAH";
                            detail.HeaderRecId = SourceModel.RECID;
                            detail.HeaderId = i.WIP_ISSUE_ID;

                            //detail.HeaderRecId = i.WIP_ISSUE_ID;
                            //detail. = i.WIP_ISSUE_ID;
                            //detail. = i.NO;
                            //detail. = i.BATCH_NO;

                            WIP_ISSUE_PPI_DETAIL_SCH.Add(detail);
                        }
                    }

                    _Model.WIP_ISSUE_PPI_DETAIL_SCH = WIP_ISSUE_PPI_DETAIL_SCH;

                    //tblO_RawMaterialIssue_Item, tblO_RawMaterialIssue_Item_Detail
                    var NameTable = "@TablePackagingMaterialIssueItem";
                    var TargetTable = "tblO_PackagingMaterialIssue_Item";
                    string SQLDeclare;
                    string RecId;

                    WipMaterialIssueItemPM Tg = new WipMaterialIssueItemPM();
                    Type tf_target = Tg.GetType();
                    CreateDeclareMergeOneManyWipIssuePM(_Model, tf_target, NameTable, TargetTable, out SQLDeclare, out RecId);
                    _SQLDeclare = SQLDeclare;

                    int affectedRows;
                    affectedRows = await Task.FromResult(_dapper.Execute(SQLDeclare));
                    if (affectedRows > 0)
                    {
                        ErrorLog.WriteToLog("materialItemPM", "[" + aDiJob + " " + aErrorKey + "] - Berhasil " + _SQLDeclare);
                    }
                    else
                    {
                        ErrorLog.WriteToLog("materialItemPM", "[" + aDiJob + " " + aErrorKey + "] - Failed " + _SQLDeclare);
                    }
                }
                #endregion
                else

                #region addtional PM 5
                if (SubID == "5")
                {
                    WipMaterialIssueAdditionalPM _Model = new WipMaterialIssueAdditionalPM();

                    _Model.RecId = SourceModel.RECID;
                    _Model.RecTimestamp = SourceModel.RECTIMESTAMP;
                    _Model.RecStatus = SourceModel.RECSTATUS;
                    _Model.Timestamp = SourceModel.TIMESTAMP;
                    _Model.Id = SourceModel.ID;
                    _Model.Type = SourceModel.TYPE;
                    _Model.Source = SourceModel.SOURCE;
                    _Model.OrganizationId = SourceModel.ORGANIZATION_ID;
                    _Model.Recipe = SourceModel.RECIPE;
                    _Model.BatchNumber = SourceModel.BATCH_NO;
                    _Model.PrintDate = SourceModel.PRINT_DATE;
                    _Model.UserPrinted = SourceModel.USER_PRINTED;
                    _Model.UpdateDateBBK = SourceModel.UPDATE_DATE_BBK;
                    _Model.TransactCreateDate = SourceModel.RECTIMESTAMP;
                    _Model.TransactUpdateDate = SourceModel.RECTIMESTAMP;
                    _Model.WoNumber = SourceModel.SCHEDULE_NUMBER;
                    _Model.BatchNumber = SourceModel.BATCH_NO;
                    _Model.Status = 2;
                    _Model.IsFirstTimeDownloading = true;

                    List<WipMaterialIssueAdditionalDetailPM> WIP_ISSUE_PPI_DETAIL_SCH = new List<WipMaterialIssueAdditionalDetailPM>();
                    if (SourceModel.WIP_ISSUE_PPI_DETAIL_SCH.Count > 0)
                    {
                        foreach (var i in SourceModel.WIP_ISSUE_PPI_DETAIL_SCH)
                        {
                            WipMaterialIssueAdditionalDetailPM detail = new WipMaterialIssueAdditionalDetailPM();
                            detail.RecId = (long)i.RECID;
                            detail.RecTimestamp = i.RECTIMESTAMP;
                            detail.RecStatus = (short?)i.RECSTATUS;
                            detail.TransactCreateDate = i.RECTIMESTAMP;
                            detail.TransactUpdateDate = i.RECTIMESTAMP;
                            detail.ItemCode = i.ITEM_CODE;
                            detail.ItemDescription = i.ITEM_DESCRIPTION;
                            detail.JumlahLot = i.JUMLAH_LOT;
                            detail.PlanQTY = (decimal?)i.PLAN_QTY;
                            detail.WipPlanQTY = (decimal?)i.WIP_PLAN_QTY;
                            detail.Quantity = i.QUANTITY;
                            detail.UOMSchedule = i.UOM_SCHEDULE;
                            detail.QuantityBatch = (decimal?)i.QUANTITY_BATCH;
                            detail.SumQtyBatch = i.SUM_QUANTITY_BATCH;
                            detail.Rounding = (int?)i.ROUNDING;
                            detail.ExpireDate = i.EXP_DATE;
                            detail.LPN = i.LPN_NO;
                            detail.ServiceWorker = i.SERVICE_WORKER;
                            detail.ScalingCode = i.SCALLING_CODE;
                            detail.Weigher = i.WEIGHER;
                            detail.Coordinator = i.COORDINATOR;
                            detail.HandoverTime = i.HANDOVER_TIME;
                            detail.CheckedByWhsSpv = i.CHECKED_BY_WHS_SPV;
                            detail.CheckedByProdCoor = i.CHECKED_BY_PROD_COOR;
                            detail.DeliveredBy = i.DELIVERED_BY;
                            detail.ReceivedBy = i.RECEIVED_BY;
                            detail.ItemType = "";
                            detail.HeaderRecId = SourceModel.RECID;
                            detail.HeaderId = i.WIP_ISSUE_ID;

                            //detail.HeaderRecId = i.WIP_ISSUE_ID;
                            //detail. = i.WIP_ISSUE_ID;
                            //detail. = i.NO;
                            //detail. = i.BATCH_NO;
                            WIP_ISSUE_PPI_DETAIL_SCH.Add(detail);
                        }
                    }

                    _Model.WIP_ISSUE_PPI_DETAIL_SCH = WIP_ISSUE_PPI_DETAIL_SCH;

                    var NameTable = "@TabletPackagingMaterialIssueAdditional";
                    var TargetTable = "tblO_PackagingMaterialIssue_Additional";
                    string SQLDeclare;
                    string RecId;

                    WipMaterialIssueAdditionalPM Tg = new WipMaterialIssueAdditionalPM();
                    Type tf_target = Tg.GetType();
                    CreateDeclareMergeOneManyWipIssueAdditionalPM(_Model, tf_target, NameTable, TargetTable, out SQLDeclare, out RecId);
                    _SQLDeclare = SQLDeclare;

                    int affectedRows;
                    affectedRows = await Task.FromResult(_dapper.Execute(SQLDeclare));
                    if (affectedRows > 0)
                    {
                        ErrorLog.WriteToLog("addtionalPM", "[" + aDiJob + " " + aErrorKey + "] - Berhasil " + _SQLDeclare);
                    }
                    else
                    {
                        ErrorLog.WriteToLog("addtionalPM", "[" + aDiJob + " " + aErrorKey + "] - Failed " + _SQLDeclare);
                    }
                }
                #endregion 5
                else

                #region return PM 6
                if (SubID == "6")
                {
                    //tblO_RawMaterialRequest_Additional, tblO_RawMaterialRequest_Additional_Detail
                    WipMaterialIssueReturnPM _Model = new WipMaterialIssueReturnPM();
                    _Model.RecId = SourceModel.RECID;
                    _Model.RecTimestamp = SourceModel.RECTIMESTAMP;
                    _Model.RecStatus = SourceModel.RECSTATUS;
                    _Model.Timestamp = SourceModel.TIMESTAMP;
                    _Model.Id = SourceModel.ID;
                    _Model.Type = SourceModel.TYPE;
                    _Model.Source = SourceModel.SOURCE;
                    _Model.OrganizationId = SourceModel.ORGANIZATION_ID;
                    _Model.Recipe = SourceModel.RECIPE;
                    _Model.BatchNumber = SourceModel.BATCH_NO;
                    _Model.PrintDate = SourceModel.PRINT_DATE;
                    _Model.UserPrinted = SourceModel.USER_PRINTED;
                    _Model.UpdateDateBBK = SourceModel.UPDATE_DATE_BBK;
                    _Model.TransactCreateDate = SourceModel.RECTIMESTAMP;
                    _Model.TransactUpdateDate = SourceModel.RECTIMESTAMP;
                    _Model.WoNumber = SourceModel.SCHEDULE_NUMBER;
                    _Model.BatchNumber = SourceModel.BATCH_NO;
                    _Model.Status = 2;
                    _Model.IsFirstTimeDownloading = true;

                    List<WipMaterialIssueReturnDetailPM> WIP_ISSUE_PPI_DETAIL_SCH = new List<WipMaterialIssueReturnDetailPM>();
                    if (SourceModel.WIP_ISSUE_PPI_DETAIL_SCH.Count > 0)
                    {
                        foreach (var i in SourceModel.WIP_ISSUE_PPI_DETAIL_SCH)
                        {
                            WipMaterialIssueReturnDetailPM detail = new WipMaterialIssueReturnDetailPM();
                            detail.RecId = (long)i.RECID;
                            detail.RecTimestamp = i.RECTIMESTAMP;
                            detail.RecStatus = (short?)i.RECSTATUS;
                            detail.TransactCreateDate = i.RECTIMESTAMP;
                            detail.TransactUpdateDate = i.RECTIMESTAMP;
                            detail.ItemCode = i.ITEM_CODE;
                            detail.ItemDescription = i.ITEM_DESCRIPTION;
                            detail.JumlahLot = i.JUMLAH_LOT;
                            detail.PlanQTY = (decimal?)i.PLAN_QTY;
                            detail.WipPlanQTY = (decimal?)i.WIP_PLAN_QTY;
                            detail.Quantity = i.QUANTITY;
                            detail.UOMSchedule = i.UOM_SCHEDULE;
                            detail.QuantityBatch = (decimal?)i.QUANTITY_BATCH;
                            detail.SumQtyBatch = i.SUM_QUANTITY_BATCH;
                            detail.Rounding = (int?)i.ROUNDING;
                            detail.ExpireDate = i.EXP_DATE;
                            detail.LPN = i.LPN_NO;
                            detail.ServiceWorker = i.SERVICE_WORKER;
                            detail.ScalingCode = i.SCALLING_CODE;
                            detail.Weigher = i.WEIGHER;
                            detail.Coordinator = i.COORDINATOR;
                            detail.HandoverTime = i.HANDOVER_TIME;
                            detail.CheckedByWhsSpv = i.CHECKED_BY_WHS_SPV;
                            detail.CheckedByProdCoor = i.CHECKED_BY_PROD_COOR;
                            detail.DeliveredBy = i.DELIVERED_BY;
                            detail.ReceivedBy = i.RECEIVED_BY;
                            detail.ItemType = "KEMAS";
                            detail.HeaderRecId = SourceModel.RECID;
                            detail.HeaderId = i.WIP_ISSUE_ID;

                            //detail.HeaderRecId = i.WIP_ISSUE_ID;
                            //detail. = i.WIP_ISSUE_ID;
                            //detail. = i.NO;
                            //detail. = i.BATCH_NO;
                            WIP_ISSUE_PPI_DETAIL_SCH.Add(detail);
                        }
                    }

                    _Model.WIP_ISSUE_PPI_DETAIL_SCH = WIP_ISSUE_PPI_DETAIL_SCH;


                    var NameTable = "@TabletblO_PackagingMaterialIssue_Return";
                    var TargetTable = "tblO_PackagingMaterialIssue_Return";
                    string SQLDeclare;
                    string RecId;

                    WipMaterialIssueReturnPM Tg = new WipMaterialIssueReturnPM();
                    Type tf_target = Tg.GetType();
                    CreateDeclareMergeOneManyWipIssueReturnPM(_Model, tf_target, NameTable, TargetTable, out SQLDeclare, out RecId);
                    _SQLDeclare = SQLDeclare;

                    int affectedRows;
                    affectedRows = await Task.FromResult(_dapper.Execute(SQLDeclare));
                    if (affectedRows > 0)
                    {
                        ErrorLog.WriteToLog("returnPM", "[" + aDiJob + " " + aErrorKey + "] - Berhasil " + _SQLDeclare);
                    }
                    else
                    {
                        ErrorLog.WriteToLog("returnPM", "[" + aDiJob + " " + aErrorKey + "] - Failed " + _SQLDeclare);
                    }
                }
                #endregion 6
                else

                #region packaging secondary PM 7
                if (SubID == "7")
                {
                    //tblO_RawMaterialRequest_Additional, tblO_RawMaterialRequest_Additional_Detail
                    WipMaterialIssueSecondaryPM _Model = new WipMaterialIssueSecondaryPM();

                    _Model.RecId = SourceModel.RECID;
                    _Model.RecTimestamp = SourceModel.RECTIMESTAMP;
                    _Model.RecStatus = SourceModel.RECSTATUS;
                    _Model.Timestamp = SourceModel.TIMESTAMP;
                    _Model.Id = SourceModel.ID;
                    _Model.Type = SourceModel.TYPE;
                    _Model.Source = SourceModel.SOURCE;
                    _Model.OrganizationId = SourceModel.ORGANIZATION_ID;
                    _Model.PrintDate = SourceModel.PRINT_DATE;
                    _Model.UserPrinted = SourceModel.USER_PRINTED;
                    _Model.UpdateDateBBK = SourceModel.UPDATE_DATE_BBK;
                    _Model.TransactCreateDate = SourceModel.RECTIMESTAMP;
                    _Model.TransactUpdateDate = SourceModel.RECTIMESTAMP;
                    _Model.Status = 2;
                    _Model.Recipe = SourceModel.RECIPE;
                    _Model.BatchNumber = SourceModel.BATCH_NO;
                    _Model.WoNumber = SourceModel.SCHEDULE_NUMBER;
                    _Model.BatchNumber = SourceModel.BATCH_NO;
                    _Model.IsFirstTimeDownloading = true;


                    List<WipMaterialIssueSecondaryDetailPM> WIP_ISSUE_PPI_DETAIL_SCH = new List<WipMaterialIssueSecondaryDetailPM>();
                    if (SourceModel.WIP_ISSUE_PPI_DETAIL_SCH.Count > 0)
                    {
                        foreach (var i in SourceModel.WIP_ISSUE_PPI_DETAIL_SCH)
                        {
                            WipMaterialIssueSecondaryDetailPM detail = new WipMaterialIssueSecondaryDetailPM();
                            detail.RecId = (long)i.RECID;
                            detail.RecTimestamp = i.RECTIMESTAMP;
                            detail.RecStatus = (short?)i.RECSTATUS;
                            detail.TransactCreateDate = i.RECTIMESTAMP;
                            detail.TransactUpdateDate = i.RECTIMESTAMP;
                            detail.ItemCode = i.ITEM_CODE;
                            detail.ItemDescription = i.ITEM_DESCRIPTION;
                            detail.JumlahLot = i.JUMLAH_LOT;
                            detail.PlanQTY = (decimal?)i.PLAN_QTY;
                            detail.WipPlanQTY = (decimal?)i.WIP_PLAN_QTY;
                            detail.Quantity = i.QUANTITY;
                            detail.UOMSchedule = i.UOM_SCHEDULE;
                            detail.QuantityBatch = (decimal?)i.QUANTITY_BATCH;
                            detail.SumQtyBatch = i.SUM_QUANTITY_BATCH;
                            detail.Rounding = (int?)i.ROUNDING;
                            detail.ExpireDate = i.EXP_DATE;
                            detail.LPN = i.LPN_NO;
                            detail.ServiceWorker = i.SERVICE_WORKER;
                            detail.ScalingCode = i.SCALLING_CODE;
                            detail.Weigher = i.WEIGHER;
                            detail.Coordinator = i.COORDINATOR;
                            detail.HandoverTime = i.HANDOVER_TIME;
                            detail.CheckedByWhsSpv = i.CHECKED_BY_WHS_SPV;
                            detail.CheckedByProdCoor = i.CHECKED_BY_PROD_COOR;
                            detail.DeliveredBy = i.DELIVERED_BY;
                            detail.ReceivedBy = i.RECEIVED_BY;
                            detail.ItemType = "KEMAS";
                            detail.HeaderRecId = SourceModel.RECID;
                            detail.HeaderId = i.WIP_ISSUE_ID;

                            //detail.HeaderRecId = i.WIP_ISSUE_ID;
                            //detail. = i.WIP_ISSUE_ID;
                            //detail. = i.NO;
                            //detail. = i.BATCH_NO;
                            WIP_ISSUE_PPI_DETAIL_SCH.Add(detail);
                        }
                    }

                    _Model.WIP_ISSUE_PPI_DETAIL_SCH = WIP_ISSUE_PPI_DETAIL_SCH;

                    var NameTable = "@TablePackagingMaterialIssueSecondary";
                    var TargetTable = "tblO_PackagingMaterialIssue_Secondary";
                    string SQLDeclare;
                    string RecId;

                    WipMaterialIssueSecondaryPM Tg = new WipMaterialIssueSecondaryPM();
                    Type tf_target = Tg.GetType();
                    CreateDeclareMergeOneManyWipSecondaryPM(_Model, tf_target, NameTable, TargetTable, out SQLDeclare, out RecId);
                    _SQLDeclare = SQLDeclare;

                    int affectedRows;
                    affectedRows = await Task.FromResult(_dapper.Execute(SQLDeclare));
                    if (affectedRows > 0)
                    {
                        ErrorLog.WriteToLog("returnPM", "[" + aDiJob + " " + aErrorKey + "] - Berhasil " + _SQLDeclare);
                    }
                    else
                    {
                        ErrorLog.WriteToLog("returnPM", "[" + aDiJob + " " + aErrorKey + "] - Failed " + _SQLDeclare);
                    }
                }
                #endregion 

            }
            catch (Exception ex)
            {
                ErrorLog.WriteToLog(aErrorKey, ex.Message);
            }
            finally
            {
                _dapper.Dispose();
            }
            return result;
        }

        public async Task DoWork(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                Console.WriteLine(DateTime.Now.ToString() + " Queue : " + Queue.WIPIssue);
                await _busControl.ReceiveAsync<WipIssueModel>(Queue.WIPIssue, x =>
                {
                    try
                    {
                        _ = Task.Run(() => { _ = DidJob(x); }, stoppingToken);
                    }
                    catch (Exception)
                    {
                        ErrorLog.WriteToLog(aErrorKey, "[" + aDoWork + " " + aErrorKey + "] " + x);
                    }
                });

                await Task.Delay(defaultReceiveDelay, stoppingToken);
            }

            _busControl.Dispose();
        }

        #region (WIP Item) RM
        private void CreateDeclareMergeOneManyWipIssueRM(WipMaterialIssueItemRM SourceModel, Type tf_target, string DeclareTable, string TargetTable, out string SQLMerge, out string RecID)
        {
            var SQLRelation = "";

            List<string> FieldsType = new List<string>();
            List<string> Fields = new List<string>();
            List<string> InsertValues = new List<string>();

            MergeSQL _ms = new MergeSQL();
            Type _st = SourceModel.GetType();

            FieldsType = _ms.GetFieldType(_st);
            Fields = _ms.GetFieldSQL(_st);

            String[] keys = new String[1];
            //keys[0] = "@HeaderRecId";
            keys[0] = "@HeaderId";
            InsertValues = _ms.GetValueSQL(ref SourceModel, _st, out SQLRelation, keys);

            var QueryRangeInsert = InsertValues.Where((value, index) => index >= 0 && index < InsertValues.Count).ToArray();
            var ValueNoRelasi = InsertValues.Where((value, index) => index >= 0 && index < InsertValues.Count).ToArray();

            var QueryInsert = string.Join(",", InsertValues.ToArray());
            var InsertNoRelasi = string.Join(",", QueryRangeInsert);

            var SQLHeader = _ms.MergeSQLFormat(tf_target, FieldsType, Fields, DeclareTable, TargetTable, InsertNoRelasi, QueryInsert);

            RecID = InsertNoRelasi[0].ToString();
            SQLMerge = SQLHeader + " " + SQLRelation;
            Console.WriteLine(SQLMerge);

            FieldsType.Clear();
            Fields.Clear();
            InsertValues.Clear();

            ErrorLog.WriteToLog("AppException", "[" + aDiJob + " " + aErrorKey + "] - " + SQLMerge);

        }
        #endregion

        #region (WIP Issue Additional) RM
        private void CreateDeclareMergeOneManyWipIssueAdditionalRM(WipMaterialIssueAdditionalRM SourceModel, Type tf_target, string DeclareTable, string TargetTable, out string SQLMerge, out string RecID)
        {

            var SQL = "";

            List<string> FieldsType = new List<string>();
            List<string> Fields = new List<string>();
            List<string> InsertValues = new List<string>();

            MergeSQL _ms = new MergeSQL();
            Type _st = SourceModel.GetType();

            FieldsType = _ms.GetFieldType(_st);
            Fields = _ms.GetFieldSQL(_st);

            String[] keys = new String[1];
            keys[0] = "@HeaderId";
            InsertValues = _ms.GetValueSQL(ref SourceModel, _st, out SQL, keys);

            var QueryRangeInsert = InsertValues.Where((value, index) => index >= 0 && index < InsertValues.Count).ToArray();
            var ValueNoRelasi = InsertValues.Where((value, index) => index >= 0 && index < InsertValues.Count).ToArray();

            var QueryInsert = string.Join(",", InsertValues.ToArray());
            var InsertNoRelasi = string.Join(",", QueryRangeInsert);

            var SQLHEADER = _ms.MergeSQLFormat(tf_target, FieldsType, Fields, DeclareTable, TargetTable, InsertNoRelasi, QueryInsert);

            RecID = InsertNoRelasi[0].ToString();
            SQLMerge = SQLHEADER + " " + SQL;
            Console.WriteLine(SQLMerge);

            FieldsType.Clear();
            Fields.Clear();
            InsertValues.Clear();

            ErrorLog.WriteToLog("AppException", "[" + aDiJob + " " + aErrorKey + "] - " + SQLMerge);

        }
        #endregion

        #region (WIP Issue Return) RM
        private void CreateDeclareMergeOneManyWipIssueReturnRM(WipMaterialIssueReturnRM SourceModel, Type tf_target, string DeclareTable, string TargetTable, out string SQLMerge, out string RecID)
        {

            var SQL = "";

            List<string> FieldsType = new List<string>();
            List<string> Fields = new List<string>();
            List<string> InsertValues = new List<string>();

            MergeSQL _ms = new MergeSQL();
            Type _st = SourceModel.GetType();

            FieldsType = _ms.GetFieldType(_st);
            Fields = _ms.GetFieldSQL(_st);

            String[] keys = new String[1];
            keys[0] = "@HeaderId";
            InsertValues = _ms.GetValueSQL(ref SourceModel, _st, out SQL, keys);

            var QueryRangeInsert = InsertValues.Where((value, index) => index >= 0 && index < InsertValues.Count).ToArray();
            var ValueNoRelasi = InsertValues.Where((value, index) => index >= 0 && index < InsertValues.Count).ToArray();

            var QueryInsert = string.Join(",", InsertValues.ToArray());
            var InsertNoRelasi = string.Join(",", QueryRangeInsert);

            var SQLHEADER = _ms.MergeSQLFormat(tf_target, FieldsType, Fields, DeclareTable, TargetTable, InsertNoRelasi, QueryInsert);

            RecID = InsertNoRelasi[0].ToString();
            SQLMerge = SQLHEADER + " " + SQL;
            Console.WriteLine(SQLMerge);

            FieldsType.Clear();
            Fields.Clear();
            InsertValues.Clear();

            ErrorLog.WriteToLog("AppException", "[" + aDiJob + " " + aErrorKey + "] - " + SQLMerge);

        }
        #endregion

        #region (WIP Item) PM
        private void CreateDeclareMergeOneManyWipIssuePM(WipMaterialIssueItemPM SourceModel, Type tf_target, string DeclareTable, string TargetTable, out string SQLMerge, out string RecID)
        {

            var SQLRelation = "";

            List<string> FieldsType = new List<string>();
            List<string> Fields = new List<string>();
            List<string> InsertValues = new List<string>();

            MergeSQL _ms = new MergeSQL();
            Type _st = SourceModel.GetType();

            FieldsType = _ms.GetFieldType(_st);
            Fields = _ms.GetFieldSQL(_st);

            String[] keys = new String[1];
            //keys[0] = "@HeaderRecId";
            keys[0] = "@HeaderId";
            InsertValues = _ms.GetValueSQL(ref SourceModel, _st, out SQLRelation, keys);

            var QueryRangeInsert = InsertValues.Where((value, index) => index >= 0 && index < InsertValues.Count).ToArray();
            var ValueNoRelasi = InsertValues.Where((value, index) => index >= 0 && index < InsertValues.Count).ToArray();

            var QueryInsert = string.Join(",", InsertValues.ToArray());
            var InsertNoRelasi = string.Join(",", QueryRangeInsert);

            var SQLHeader = _ms.MergeSQLFormat(tf_target, FieldsType, Fields, DeclareTable, TargetTable, InsertNoRelasi, QueryInsert);

            RecID = InsertNoRelasi[0].ToString();
            SQLMerge = SQLHeader + " " + SQLRelation;
            Console.WriteLine(SQLMerge);

            FieldsType.Clear();
            Fields.Clear();
            InsertValues.Clear();

            ErrorLog.WriteToLog("AppException", "[" + aDiJob + " " + aErrorKey + "] - " + SQLMerge);

        }
        #endregion

        #region (WIP Issue Additional) PM
        private void CreateDeclareMergeOneManyWipIssueAdditionalPM(WipMaterialIssueAdditionalPM SourceModel, Type tf_target, string DeclareTable, string TargetTable, out string SQLMerge, out string RecID)
        {

            var SQL = "";

            List<string> FieldsType = new List<string>();
            List<string> Fields = new List<string>();
            List<string> InsertValues = new List<string>();

            MergeSQL _ms = new MergeSQL();
            Type _st = SourceModel.GetType();

            FieldsType = _ms.GetFieldType(_st);
            Fields = _ms.GetFieldSQL(_st);

            String[] keys = new String[1];
            keys[0] = "@HeaderId";
            InsertValues = _ms.GetValueSQL(ref SourceModel, _st, out SQL, keys);

            var QueryRangeInsert = InsertValues.Where((value, index) => index >= 0 && index < InsertValues.Count).ToArray();
            var ValueNoRelasi = InsertValues.Where((value, index) => index >= 0 && index < InsertValues.Count).ToArray();

            var QueryInsert = string.Join(",", InsertValues.ToArray());
            var InsertNoRelasi = string.Join(",", QueryRangeInsert);

            var SQLHEADER = _ms.MergeSQLFormat(tf_target, FieldsType, Fields, DeclareTable, TargetTable, InsertNoRelasi, QueryInsert);

            RecID = InsertNoRelasi[0].ToString();
            SQLMerge = SQLHEADER + " " + SQL;
            Console.WriteLine(SQLMerge);

            FieldsType.Clear();
            Fields.Clear();
            InsertValues.Clear();

            ErrorLog.WriteToLog("AppException", "[" + aDiJob + " " + aErrorKey + "] - " + SQLMerge);

        }
        #endregion

        #region (WIP Issue Return) PM
        private void CreateDeclareMergeOneManyWipIssueReturnPM(WipMaterialIssueReturnPM SourceModel, Type tf_target, string DeclareTable, string TargetTable, out string SQLMerge, out string RecID)
        {

            var SQL = "";

            List<string> FieldsType = new List<string>();
            List<string> Fields = new List<string>();
            List<string> InsertValues = new List<string>();

            MergeSQL _ms = new MergeSQL();
            Type _st = SourceModel.GetType();

            FieldsType = _ms.GetFieldType(_st);
            Fields = _ms.GetFieldSQL(_st);

            String[] keys = new String[1];
            keys[0] = "@HeaderId";
            InsertValues = _ms.GetValueSQL(ref SourceModel, _st, out SQL, keys);

            var QueryRangeInsert = InsertValues.Where((value, index) => index >= 0 && index < InsertValues.Count).ToArray();
            var ValueNoRelasi = InsertValues.Where((value, index) => index >= 0 && index < InsertValues.Count).ToArray();

            var QueryInsert = string.Join(",", InsertValues.ToArray());
            var InsertNoRelasi = string.Join(",", QueryRangeInsert);

            var SQLHEADER = _ms.MergeSQLFormat(tf_target, FieldsType, Fields, DeclareTable, TargetTable, InsertNoRelasi, QueryInsert);

            RecID = InsertNoRelasi[0].ToString();
            SQLMerge = SQLHEADER + " " + SQL;
            Console.WriteLine(SQLMerge);

            FieldsType.Clear();
            Fields.Clear();
            InsertValues.Clear();

            ErrorLog.WriteToLog("AppException", "[" + aDiJob + " " + aErrorKey + "] - " + SQLMerge);

        }

        public async Task<BaseResponse> TestWIP(WipIssueModel SourceModel)
        {
            bool status = false;
            int affectedRows = 0;
            BaseResponse result = new BaseResponse();
            try
            {
                // Hasil Potong PPI (WIP Issue) RM
                string ID = SourceModel.ID;
                Console.WriteLine("Proces ID :" + ID);
                string SubID = ID.Substring(0, 1);

                #region Material item RM 1
                if (SubID == "1")
                {
                    string ITEM = "";

                    string SubItemType = ID.Substring(0, 2);
                    if (SubItemType == "11")
                    {
                        ITEM = "BAHAN NON COATING";
                    }
                    else
                    {
                        ITEM = "BAHAN COATING";
                    }

                    WipMaterialIssueItemRM _Model = new WipMaterialIssueItemRM();

                    _Model.RecId = SourceModel.RECID;
                    _Model.RecTimestamp = SourceModel.RECTIMESTAMP;
                    _Model.RecStatus = SourceModel.RECSTATUS;
                    _Model.Timestamp = SourceModel.TIMESTAMP;
                    _Model.Id = SourceModel.ID;
                    _Model.Type = SourceModel.TYPE;
                    _Model.Source = SourceModel.SOURCE;
                    _Model.OrganizationId = SourceModel.ORGANIZATION_ID;
                    _Model.PrintDate = SourceModel.PRINT_DATE;
                    _Model.UserPrinted = SourceModel.USER_PRINTED;
                    _Model.UpdateDateBBK = SourceModel.UPDATE_DATE_BBK;
                    _Model.TransactCreateDate = SourceModel.RECTIMESTAMP;
                    _Model.TransactUpdateDate = SourceModel.RECTIMESTAMP;
                    _Model.Status = 2;
                    _Model.Recipe = SourceModel.RECIPE;
                    _Model.BatchNumber = SourceModel.BATCH_NO;
                    _Model.WoNumber = SourceModel.SCHEDULE_NUMBER;
                    _Model.BatchNumber = SourceModel.BATCH_NO;
                    _Model.IsFirstTimeDownloading = true;

                    //_Model.ProductCode = null;
                    //_Model.ProductName = null;
                    //_Model.EbrId = null;

                    List<WipMaterialIssueItemDetailRM> WIP_ISSUE_PPI_DETAIL_SCH = new List<WipMaterialIssueItemDetailRM>();
                    if (SourceModel.WIP_ISSUE_PPI_DETAIL_SCH.Count > 0)
                    {
                        foreach (var i in SourceModel.WIP_ISSUE_PPI_DETAIL_SCH)
                        {
                            WipMaterialIssueItemDetailRM Detail = new WipMaterialIssueItemDetailRM();
                            Detail.RecId = (long)i.RECID;
                            Detail.RecTimestamp = i.RECTIMESTAMP;
                            Detail.RecStatus = (short?)i.RECSTATUS;
                            Detail.TransactCreateDate = i.RECTIMESTAMP;
                            Detail.TransactUpdateDate = i.RECTIMESTAMP;
                            Detail.ItemCode = i.ITEM_CODE;
                            Detail.ItemDescription = i.ITEM_DESCRIPTION;
                            Detail.JumlahLot = i.JUMLAH_LOT;
                            Detail.PlanQTY = (decimal?)i.PLAN_QTY;
                            Detail.WipPlanQTY = (decimal?)i.WIP_PLAN_QTY;
                            Detail.Quantity = (decimal?)i.QUANTITY;
                            Detail.UOMSchedule = i.UOM_SCHEDULE;
                            Detail.QuantityBatch = (decimal?)i.QUANTITY_BATCH;
                            Detail.SumQtyBatch = i.SUM_QUANTITY_BATCH;
                            Detail.Rounding = (int?)i.ROUNDING;
                            Detail.ExpireDate = i.EXP_DATE;
                            Detail.LPN = i.LPN_NO;
                            Detail.ServiceWorker = i.SERVICE_WORKER;
                            Detail.ScalingCode = i.SCALLING_CODE;
                            Detail.Weigher = i.WEIGHER;
                            Detail.Coordinator = i.COORDINATOR;
                            Detail.HandoverTime = i.HANDOVER_TIME;
                            Detail.CheckedByWhsSpv = i.CHECKED_BY_WHS_SPV;
                            Detail.CheckedByProdCoor = i.CHECKED_BY_PROD_COOR;
                            Detail.DeliveredBy = i.DELIVERED_BY;
                            Detail.ReceivedBy = i.RECEIVED_BY;
                            Detail.HeaderRecId = i.WIP_ISSUE_ID;
                            Detail.ItemType = ITEM;
                            //Detail. = i.BATCH_NO;
                            //Detail. = i.WIP_ISSUE_ID;
                            //Detail. = i.NO;
                            WIP_ISSUE_PPI_DETAIL_SCH.Add(Detail);
                        }
                    }

                    _Model.WIP_ISSUE_PPI_DETAIL_SCH = WIP_ISSUE_PPI_DETAIL_SCH;

                    //tblO_RawMaterialIssue_Item, tblO_RawMaterialIssue_Item_Detail
                    var NameTable = "@TableRawMaterialIssueItem";
                    var TargetTable = "tblO_RawMaterialIssue_Item";
                    string SQLDeclare;
                    string RecId;

                    WipMaterialIssueItemRM Tg = new WipMaterialIssueItemRM();
                    Type tf_target = Tg.GetType();
                    CreateDeclareMergeOneManyWipIssueRM(_Model, tf_target, NameTable, TargetTable, out SQLDeclare, out RecId);
                    _SQLDeclare = SQLDeclare;

                   
                    affectedRows = await Task.FromResult(_dapper.Execute(SQLDeclare));
                    if (affectedRows > 0)
                    {
                        ErrorLog.WriteToLog("MaterialitemRM", "[" + aDiJob + " " + aErrorKey + "] - Berhasil " + _SQLDeclare);
                        status = true;
                    }
                    else
                    {
                        ErrorLog.WriteToLog("MaterialitemRM", "[" + aDiJob + " " + aErrorKey + "] - Failed " + _SQLDeclare);
                        status = false;
                    }
                }
                #endregion
                else

                #region issue addtional RM 2
                // Hasil Potong PPI (WIP Issue - Additional) RM
                if (SubID == "2")
                {
                    string SubItemType = ID.Substring(0, 2);
                    string ITEM = "";
                    if (SubItemType == "21")
                    {
                        ITEM = "BAHAN COATING";
                    }
                    else
                    {
                        ITEM = "BAHAN NON COATING";
                    }

                    //tblO_RawMaterialRequest_Additional, tblO_RawMaterialRequest_Additional_Detail
                    WipMaterialIssueAdditionalRM _Model = new WipMaterialIssueAdditionalRM();

                    _Model.RecId = SourceModel.RECID;
                    _Model.RecTimestamp = SourceModel.RECTIMESTAMP;
                    _Model.RecStatus = SourceModel.RECSTATUS;
                    _Model.Timestamp = SourceModel.TIMESTAMP;
                    _Model.Id = SourceModel.ID;
                    _Model.Type = SourceModel.TYPE;
                    _Model.Source = SourceModel.SOURCE;
                    _Model.OrganizationId = SourceModel.ORGANIZATION_ID;
                    _Model.Recipe = SourceModel.RECIPE;
                    _Model.BatchNumber = SourceModel.BATCH_NO;
                    _Model.PrintDate = SourceModel.PRINT_DATE;
                    _Model.UserPrinted = SourceModel.USER_PRINTED;
                    _Model.UpdateDateBBK = SourceModel.UPDATE_DATE_BBK;
                    _Model.TransactCreateDate = SourceModel.RECTIMESTAMP;
                    _Model.TransactUpdateDate = SourceModel.RECTIMESTAMP;
                    _Model.WoNumber = SourceModel.SCHEDULE_NUMBER;
                    _Model.BatchNumber = SourceModel.BATCH_NO;
                    _Model.Status = 2;
                    _Model.IsFirstTimeDownloading = true;
                    //MaterialIssueRM. = SourceModel.SCHEDULE_NUMBER;
                    //MaterialIssuePM.ProductCode = null;
                    //MaterialIssuePM.ProductName = null;
                    //MaterialIssuePM.EbrId = null;

                    List<WipMaterialIssueAdditionalDetailRM> WIP_ISSUE_PPI_DETAIL_SCH = new List<WipMaterialIssueAdditionalDetailRM>();
                    if (SourceModel.WIP_ISSUE_PPI_DETAIL_SCH.Count > 0)
                    {
                        foreach (var i in SourceModel.WIP_ISSUE_PPI_DETAIL_SCH)
                        {
                            WipMaterialIssueAdditionalDetailRM Detail = new WipMaterialIssueAdditionalDetailRM();
                            Detail.RecId = (long)i.RECID;
                            Detail.RecTimestamp = i.RECTIMESTAMP;
                            Detail.RecStatus = (short?)i.RECSTATUS;
                            Detail.TransactCreateDate = i.RECTIMESTAMP;
                            Detail.TransactUpdateDate = i.RECTIMESTAMP;
                            Detail.ItemCode = i.ITEM_CODE;
                            Detail.ItemDescription = i.ITEM_DESCRIPTION;
                            Detail.JumlahLot = i.JUMLAH_LOT;
                            Detail.PlanQTY = (decimal?)i.PLAN_QTY;
                            Detail.WipPlanQTY = (decimal?)i.WIP_PLAN_QTY;
                            Detail.Quantity = (decimal?)i.QUANTITY;
                            Detail.UOMSchedule = i.UOM_SCHEDULE;
                            Detail.QuantityBatch = (decimal?)i.QUANTITY_BATCH;
                            Detail.SumQtyBatch = i.SUM_QUANTITY_BATCH;
                            Detail.Rounding = (int?)i.ROUNDING;
                            Detail.ExpireDate = i.EXP_DATE;
                            Detail.LPN = i.LPN_NO;
                            Detail.ServiceWorker = i.SERVICE_WORKER;
                            Detail.ScalingCode = i.SCALLING_CODE;
                            Detail.Weigher = i.WEIGHER;
                            Detail.Coordinator = i.COORDINATOR;
                            Detail.HandoverTime = i.HANDOVER_TIME;
                            Detail.CheckedByWhsSpv = i.CHECKED_BY_WHS_SPV;
                            Detail.CheckedByProdCoor = i.CHECKED_BY_PROD_COOR;
                            Detail.DeliveredBy = i.DELIVERED_BY;
                            Detail.ReceivedBy = i.RECEIVED_BY;
                            Detail.HeaderRecId = i.WIP_ISSUE_ID;
                            Detail.ItemType = ITEM;

                            //Detail. = i.BATCH_NO;
                            //Detail. = i.WIP_ISSUE_ID;
                            //Detail. = i.NO;
                            WIP_ISSUE_PPI_DETAIL_SCH.Add(Detail);
                        }
                    }

                    _Model.WIP_ISSUE_PPI_DETAIL_SCH = WIP_ISSUE_PPI_DETAIL_SCH;

                    var NameTable = "@TableRawMaterialIssueAdditional";
                    var TargetTable = "tblO_RawMaterialIssue_Additional";
                    string SQLDeclare;
                    string RecId;

                    WipMaterialIssueAdditionalRM Tg = new WipMaterialIssueAdditionalRM();
                    Type tf_target = Tg.GetType();
                    CreateDeclareMergeOneManyWipIssueAdditionalRM(_Model, tf_target, NameTable, TargetTable, out SQLDeclare, out RecId);
                    _SQLDeclare = SQLDeclare;

                    
                    affectedRows = await Task.FromResult(_dapper.Execute(SQLDeclare));
                    if (affectedRows > 0)
                    {
                        ErrorLog.WriteToLog("issueaddtionalRM", "[" + aDiJob + " " + aErrorKey + "] - Berhasil " + _SQLDeclare);
                        status = true;
                    }
                    else
                    {
                        ErrorLog.WriteToLog("issueaddtionalRM", "[" + aDiJob + " " + aErrorKey + "] - Failed " + _SQLDeclare);
                        status = false;
                    }

                }
                #endregion
                else

                #region issue return RM 3
                //// Hasil Potong PPI (WIP Issue - Return) RM
                if (SubID == "3")
                {
                    //tblO_RawMaterialRequest_Additional, tblO_RawMaterialRequest_Additional_Detail
                    WipMaterialIssueReturnRM _Model = new WipMaterialIssueReturnRM();

                    _Model.RecId = SourceModel.RECID;
                    _Model.RecTimestamp = SourceModel.RECTIMESTAMP;
                    _Model.RecStatus = SourceModel.RECSTATUS;
                    _Model.Timestamp = SourceModel.TIMESTAMP;
                    _Model.Id = SourceModel.ID;
                    _Model.Type = SourceModel.TYPE;
                    _Model.Source = SourceModel.SOURCE;
                    _Model.OrganizationId = SourceModel.ORGANIZATION_ID;
                    _Model.Recipe = SourceModel.RECIPE;
                    _Model.BatchNumber = SourceModel.BATCH_NO;
                    _Model.PrintDate = SourceModel.PRINT_DATE;
                    _Model.UserPrinted = SourceModel.USER_PRINTED;
                    _Model.UpdateDateBBK = SourceModel.UPDATE_DATE_BBK;
                    _Model.TransactCreateDate = SourceModel.RECTIMESTAMP;
                    _Model.TransactUpdateDate = SourceModel.RECTIMESTAMP;
                    _Model.WoNumber = SourceModel.SCHEDULE_NUMBER;
                    _Model.BatchNumber = SourceModel.BATCH_NO;
                    _Model.Status = 2;
                    //MaterialIssueRM. = SourceModel.SCHEDULE_NUMBER;
                    //MaterialIssuePM.ProductCode = null;
                    //MaterialIssuePM.ProductName = null;
                    //MaterialIssuePM.EbrId = null;

                    List<WipMaterialIssueReturnDetailRM> WIP_ISSUE_PPI_DETAIL_SCH = new List<WipMaterialIssueReturnDetailRM>();
                    if (SourceModel.WIP_ISSUE_PPI_DETAIL_SCH.Count > 0)
                    {
                        foreach (var i in SourceModel.WIP_ISSUE_PPI_DETAIL_SCH)
                        {
                            WipMaterialIssueReturnDetailRM detail = new WipMaterialIssueReturnDetailRM();
                            detail.RecId = (long)i.RECID;
                            detail.RecTimestamp = i.RECTIMESTAMP;
                            detail.RecStatus = (short?)i.RECSTATUS;
                            detail.TransactCreateDate = i.RECTIMESTAMP;
                            detail.TransactUpdateDate = i.RECTIMESTAMP;
                            detail.ItemCode = i.ITEM_CODE;
                            detail.ItemDescription = i.ITEM_DESCRIPTION;
                            detail.JumlahLot = i.JUMLAH_LOT;
                            detail.PlanQTY = (decimal?)i.PLAN_QTY;
                            detail.WipPlanQTY = (decimal?)i.WIP_PLAN_QTY;
                            detail.Quantity = i.QUANTITY;
                            detail.UOMSchedule = i.UOM_SCHEDULE;
                            detail.QuantityBatch = (decimal?)i.QUANTITY_BATCH;
                            detail.SumQtyBatch = i.SUM_QUANTITY_BATCH;
                            detail.Rounding = (int?)i.ROUNDING;
                            detail.ExpireDate = i.EXP_DATE;
                            detail.LPN = i.LPN_NO;
                            detail.ServiceWorker = i.SERVICE_WORKER;
                            detail.ScalingCode = i.SCALLING_CODE;
                            detail.Weigher = i.WEIGHER;
                            detail.Coordinator = i.COORDINATOR;
                            detail.HandoverTime = i.HANDOVER_TIME;
                            detail.CheckedByWhsSpv = i.CHECKED_BY_WHS_SPV;
                            detail.CheckedByProdCoor = i.CHECKED_BY_PROD_COOR;
                            detail.DeliveredBy = i.DELIVERED_BY;
                            detail.ReceivedBy = i.RECEIVED_BY;
                            detail.ItemType = "";
                            detail.HeaderRecId = SourceModel.RECID;
                            detail.HeaderId = i.WIP_ISSUE_ID;
                            WIP_ISSUE_PPI_DETAIL_SCH.Add(detail);
                        }
                    }

                    _Model.WIP_ISSUE_PPI_DETAIL_SCH = WIP_ISSUE_PPI_DETAIL_SCH;

                    var NameTable = "@TableRawMaterialIssueReturn";
                    var TargetTable = "tblO_RawMaterialIssue_Return";
                    string SQLDeclare;
                    string RecId;

                    WipMaterialIssueReturnRM Tg = new WipMaterialIssueReturnRM();
                    Type tf_target = Tg.GetType();
                    CreateDeclareMergeOneManyWipIssueReturnRM(_Model, tf_target, NameTable, TargetTable, out SQLDeclare, out RecId);
                    _SQLDeclare = SQLDeclare;

                   
                    affectedRows = await Task.FromResult(_dapper.Execute(SQLDeclare));
                    if (affectedRows > 0)
                    {
                        ErrorLog.WriteToLog("issuereturnRM", "[" + aDiJob + " " + aErrorKey + "] - Berhasil " + _SQLDeclare);
                        status = true;
                    }
                    else
                    {
                        ErrorLog.WriteToLog("issuereturnRM", "[" + aDiJob + " " + aErrorKey + "] - Failed " + _SQLDeclare);
                        status = false;
                    }

                }
                #endregion
                else

                #region material Item PM 4
                if (SubID == "4")
                {

                    WipMaterialIssueItemPM _Model = new WipMaterialIssueItemPM();

                    _Model.RecId = SourceModel.RECID;
                    _Model.RecTimestamp = SourceModel.RECTIMESTAMP;
                    _Model.RecStatus = SourceModel.RECSTATUS;
                    _Model.Timestamp = SourceModel.TIMESTAMP;
                    _Model.Id = SourceModel.ID;
                    _Model.Type = SourceModel.TYPE;
                    _Model.Source = SourceModel.SOURCE;
                    _Model.OrganizationId = SourceModel.ORGANIZATION_ID;
                    _Model.PrintDate = SourceModel.PRINT_DATE;
                    _Model.UserPrinted = SourceModel.USER_PRINTED;
                    _Model.UpdateDateBBK = SourceModel.UPDATE_DATE_BBK;
                    _Model.TransactCreateDate = SourceModel.RECTIMESTAMP;
                    _Model.TransactUpdateDate = SourceModel.RECTIMESTAMP;
                    _Model.Status = 2;
                    _Model.Recipe = SourceModel.RECIPE;
                    _Model.BatchNumber = SourceModel.BATCH_NO;
                    _Model.WoNumber = SourceModel.SCHEDULE_NUMBER;
                    _Model.BatchNumber = SourceModel.BATCH_NO;
                    _Model.IsFirstTimeDownloading = true;

                    //_Model.ProductCode = null;
                    //_Model.ProductName = null;
                    //_Model.EbrId = null;
                    //MaterialIssueRM. = SourceModel.SCHEDULE_NUMBER;

                    List<WipMaterialIssueItemDetailPM> WIP_ISSUE_PPI_DETAIL_SCH = new List<WipMaterialIssueItemDetailPM>();
                    if (SourceModel.WIP_ISSUE_PPI_DETAIL_SCH.Count > 0)
                    {
                        foreach (var i in SourceModel.WIP_ISSUE_PPI_DETAIL_SCH)
                        {
                            WipMaterialIssueItemDetailPM detail = new WipMaterialIssueItemDetailPM();
                            detail.RecId = (long)i.RECID;
                            detail.RecTimestamp = i.RECTIMESTAMP;
                            detail.RecStatus = (short?)i.RECSTATUS;
                            detail.TransactCreateDate = i.RECTIMESTAMP;
                            detail.TransactUpdateDate = i.RECTIMESTAMP;
                            detail.ItemCode = i.ITEM_CODE;
                            detail.ItemDescription = i.ITEM_DESCRIPTION;
                            detail.JumlahLot = i.JUMLAH_LOT;
                            detail.PlanQTY = (decimal?)i.PLAN_QTY;
                            detail.WipPlanQTY = (decimal?)i.WIP_PLAN_QTY;
                            detail.Quantity = i.QUANTITY;
                            detail.UOMSchedule = i.UOM_SCHEDULE;
                            detail.QuantityBatch = (decimal?)i.QUANTITY_BATCH;
                            detail.SumQtyBatch = i.SUM_QUANTITY_BATCH;
                            detail.Rounding = (int?)i.ROUNDING;
                            detail.ExpireDate = i.EXP_DATE;
                            detail.LPN = i.LPN_NO;
                            detail.ServiceWorker = i.SERVICE_WORKER;
                            detail.ScalingCode = i.SCALLING_CODE;
                            detail.Weigher = i.WEIGHER;
                            detail.Coordinator = i.COORDINATOR;
                            detail.HandoverTime = i.HANDOVER_TIME;
                            detail.CheckedByWhsSpv = i.CHECKED_BY_WHS_SPV;
                            detail.CheckedByProdCoor = i.CHECKED_BY_PROD_COOR;
                            detail.DeliveredBy = i.DELIVERED_BY;
                            detail.ReceivedBy = i.RECEIVED_BY;
                            detail.ItemType = "WADAH";
                            detail.HeaderRecId = SourceModel.RECID;
                            detail.HeaderId = i.WIP_ISSUE_ID;

                            //detail.HeaderRecId = i.WIP_ISSUE_ID;
                            //detail. = i.WIP_ISSUE_ID;
                            //detail. = i.NO;
                            //detail. = i.BATCH_NO;

                            WIP_ISSUE_PPI_DETAIL_SCH.Add(detail);
                        }
                    }

                    _Model.WIP_ISSUE_PPI_DETAIL_SCH = WIP_ISSUE_PPI_DETAIL_SCH;

                    //tblO_RawMaterialIssue_Item, tblO_RawMaterialIssue_Item_Detail
                    var NameTable = "@TablePackagingMaterialIssueItem";
                    var TargetTable = "tblO_PackagingMaterialIssue_Item";
                    string SQLDeclare;
                    string RecId;

                    WipMaterialIssueItemPM Tg = new WipMaterialIssueItemPM();
                    Type tf_target = Tg.GetType();
                    CreateDeclareMergeOneManyWipIssuePM(_Model, tf_target, NameTable, TargetTable, out SQLDeclare, out RecId);
                    _SQLDeclare = SQLDeclare;

                    
                    affectedRows = await Task.FromResult(_dapper.Execute(SQLDeclare));
                    if (affectedRows > 0)
                    {
                        ErrorLog.WriteToLog("materialItemPM", "[" + aDiJob + " " + aErrorKey + "] - Berhasil " + _SQLDeclare);
                        status = true;
                    }
                    else
                    {
                        ErrorLog.WriteToLog("materialItemPM", "[" + aDiJob + " " + aErrorKey + "] - Failed " + _SQLDeclare);
                        status = false;
                    }
                }
                #endregion
                else

                #region addtional PM 5
                if (SubID == "5")
                {
                    WipMaterialIssueAdditionalPM _Model = new WipMaterialIssueAdditionalPM();

                    _Model.RecId = SourceModel.RECID;
                    _Model.RecTimestamp = SourceModel.RECTIMESTAMP;
                    _Model.RecStatus = SourceModel.RECSTATUS;
                    _Model.Timestamp = SourceModel.TIMESTAMP;
                    _Model.Id = SourceModel.ID;
                    _Model.Type = SourceModel.TYPE;
                    _Model.Source = SourceModel.SOURCE;
                    _Model.OrganizationId = SourceModel.ORGANIZATION_ID;
                    _Model.Recipe = SourceModel.RECIPE;
                    _Model.BatchNumber = SourceModel.BATCH_NO;
                    _Model.PrintDate = SourceModel.PRINT_DATE;
                    _Model.UserPrinted = SourceModel.USER_PRINTED;
                    _Model.UpdateDateBBK = SourceModel.UPDATE_DATE_BBK;
                    _Model.TransactCreateDate = SourceModel.RECTIMESTAMP;
                    _Model.TransactUpdateDate = SourceModel.RECTIMESTAMP;
                    _Model.WoNumber = SourceModel.SCHEDULE_NUMBER;
                    _Model.BatchNumber = SourceModel.BATCH_NO;
                    _Model.Status = 2;
                    _Model.IsFirstTimeDownloading = true;

                    List<WipMaterialIssueAdditionalDetailPM> WIP_ISSUE_PPI_DETAIL_SCH = new List<WipMaterialIssueAdditionalDetailPM>();
                    if (SourceModel.WIP_ISSUE_PPI_DETAIL_SCH.Count > 0)
                    {
                        foreach (var i in SourceModel.WIP_ISSUE_PPI_DETAIL_SCH)
                        {
                            WipMaterialIssueAdditionalDetailPM detail = new WipMaterialIssueAdditionalDetailPM();
                            detail.RecId = (long)i.RECID;
                            detail.RecTimestamp = i.RECTIMESTAMP;
                            detail.RecStatus = (short?)i.RECSTATUS;
                            detail.TransactCreateDate = i.RECTIMESTAMP;
                            detail.TransactUpdateDate = i.RECTIMESTAMP;
                            detail.ItemCode = i.ITEM_CODE;
                            detail.ItemDescription = i.ITEM_DESCRIPTION;
                            detail.JumlahLot = i.JUMLAH_LOT;
                            detail.PlanQTY = (decimal?)i.PLAN_QTY;
                            detail.WipPlanQTY = (decimal?)i.WIP_PLAN_QTY;
                            detail.Quantity = i.QUANTITY;
                            detail.UOMSchedule = i.UOM_SCHEDULE;
                            detail.QuantityBatch = (decimal?)i.QUANTITY_BATCH;
                            detail.SumQtyBatch = i.SUM_QUANTITY_BATCH;
                            detail.Rounding = (int?)i.ROUNDING;
                            detail.ExpireDate = i.EXP_DATE;
                            detail.LPN = i.LPN_NO;
                            detail.ServiceWorker = i.SERVICE_WORKER;
                            detail.ScalingCode = i.SCALLING_CODE;
                            detail.Weigher = i.WEIGHER;
                            detail.Coordinator = i.COORDINATOR;
                            detail.HandoverTime = i.HANDOVER_TIME;
                            detail.CheckedByWhsSpv = i.CHECKED_BY_WHS_SPV;
                            detail.CheckedByProdCoor = i.CHECKED_BY_PROD_COOR;
                            detail.DeliveredBy = i.DELIVERED_BY;
                            detail.ReceivedBy = i.RECEIVED_BY;
                            detail.ItemType = "";
                            detail.HeaderRecId = SourceModel.RECID;
                            detail.HeaderId = i.WIP_ISSUE_ID;

                            //detail.HeaderRecId = i.WIP_ISSUE_ID;
                            //detail. = i.WIP_ISSUE_ID;
                            //detail. = i.NO;
                            //detail. = i.BATCH_NO;
                            WIP_ISSUE_PPI_DETAIL_SCH.Add(detail);
                        }
                    }

                    _Model.WIP_ISSUE_PPI_DETAIL_SCH = WIP_ISSUE_PPI_DETAIL_SCH;

                    var NameTable = "@TabletPackagingMaterialIssueAdditional";
                    var TargetTable = "tblO_PackagingMaterialIssue_Additional";
                    string SQLDeclare;
                    string RecId;

                    WipMaterialIssueAdditionalPM Tg = new WipMaterialIssueAdditionalPM();
                    Type tf_target = Tg.GetType();
                    CreateDeclareMergeOneManyWipIssueAdditionalPM(_Model, tf_target, NameTable, TargetTable, out SQLDeclare, out RecId);
                    _SQLDeclare = SQLDeclare;

                  
                    affectedRows = await Task.FromResult(_dapper.Execute(SQLDeclare));
                    if (affectedRows > 0)
                    {
                        ErrorLog.WriteToLog("addtionalPM", "[" + aDiJob + " " + aErrorKey + "] - Berhasil " + _SQLDeclare);
                        status = true;
                    }
                    else
                    {
                        ErrorLog.WriteToLog("addtionalPM", "[" + aDiJob + " " + aErrorKey + "] - Failed " + _SQLDeclare);
                        status = false;
                    }
                }
                #endregion 5
                else

                #region return PM 6
                if (SubID == "6")
                {
                    //tblO_RawMaterialRequest_Additional, tblO_RawMaterialRequest_Additional_Detail
                    WipMaterialIssueReturnPM _Model = new WipMaterialIssueReturnPM();
                    _Model.RecId = SourceModel.RECID;
                    _Model.RecTimestamp = SourceModel.RECTIMESTAMP;
                    _Model.RecStatus = SourceModel.RECSTATUS;
                    _Model.Timestamp = SourceModel.TIMESTAMP;
                    _Model.Id = SourceModel.ID;
                    _Model.Type = SourceModel.TYPE;
                    _Model.Source = SourceModel.SOURCE;
                    _Model.OrganizationId = SourceModel.ORGANIZATION_ID;
                    _Model.Recipe = SourceModel.RECIPE;
                    _Model.BatchNumber = SourceModel.BATCH_NO;
                    _Model.PrintDate = SourceModel.PRINT_DATE;
                    _Model.UserPrinted = SourceModel.USER_PRINTED;
                    _Model.UpdateDateBBK = SourceModel.UPDATE_DATE_BBK;
                    _Model.TransactCreateDate = SourceModel.RECTIMESTAMP;
                    _Model.TransactUpdateDate = SourceModel.RECTIMESTAMP;
                    _Model.WoNumber = SourceModel.SCHEDULE_NUMBER;
                    _Model.BatchNumber = SourceModel.BATCH_NO;
                    _Model.Status = 2;
                    _Model.IsFirstTimeDownloading = true;

                    List<WipMaterialIssueReturnDetailPM> WIP_ISSUE_PPI_DETAIL_SCH = new List<WipMaterialIssueReturnDetailPM>();
                    if (SourceModel.WIP_ISSUE_PPI_DETAIL_SCH.Count > 0)
                    {
                        foreach (var i in SourceModel.WIP_ISSUE_PPI_DETAIL_SCH)
                        {
                            WipMaterialIssueReturnDetailPM detail = new WipMaterialIssueReturnDetailPM();
                            detail.RecId = (long)i.RECID;
                            detail.RecTimestamp = i.RECTIMESTAMP;
                            detail.RecStatus = (short?)i.RECSTATUS;
                            detail.TransactCreateDate = i.RECTIMESTAMP;
                            detail.TransactUpdateDate = i.RECTIMESTAMP;
                            detail.ItemCode = i.ITEM_CODE;
                            detail.ItemDescription = i.ITEM_DESCRIPTION;
                            detail.JumlahLot = i.JUMLAH_LOT;
                            detail.PlanQTY = (decimal?)i.PLAN_QTY;
                            detail.WipPlanQTY = (decimal?)i.WIP_PLAN_QTY;
                            detail.Quantity = i.QUANTITY;
                            detail.UOMSchedule = i.UOM_SCHEDULE;
                            detail.QuantityBatch = (decimal?)i.QUANTITY_BATCH;
                            detail.SumQtyBatch = i.SUM_QUANTITY_BATCH;
                            detail.Rounding = (int?)i.ROUNDING;
                            detail.ExpireDate = i.EXP_DATE;
                            detail.LPN = i.LPN_NO;
                            detail.ServiceWorker = i.SERVICE_WORKER;
                            detail.ScalingCode = i.SCALLING_CODE;
                            detail.Weigher = i.WEIGHER;
                            detail.Coordinator = i.COORDINATOR;
                            detail.HandoverTime = i.HANDOVER_TIME;
                            detail.CheckedByWhsSpv = i.CHECKED_BY_WHS_SPV;
                            detail.CheckedByProdCoor = i.CHECKED_BY_PROD_COOR;
                            detail.DeliveredBy = i.DELIVERED_BY;
                            detail.ReceivedBy = i.RECEIVED_BY;
                            detail.ItemType = "KEMAS";
                            detail.HeaderRecId = SourceModel.RECID;
                            detail.HeaderId = i.WIP_ISSUE_ID;

                            //detail.HeaderRecId = i.WIP_ISSUE_ID;
                            //detail. = i.WIP_ISSUE_ID;
                            //detail. = i.NO;
                            //detail. = i.BATCH_NO;
                            WIP_ISSUE_PPI_DETAIL_SCH.Add(detail);
                        }
                    }

                    _Model.WIP_ISSUE_PPI_DETAIL_SCH = WIP_ISSUE_PPI_DETAIL_SCH;


                    var NameTable = "@TabletblO_PackagingMaterialIssue_Return";
                    var TargetTable = "tblO_PackagingMaterialIssue_Return";
                    string SQLDeclare;
                    string RecId;

                    WipMaterialIssueReturnPM Tg = new WipMaterialIssueReturnPM();
                    Type tf_target = Tg.GetType();
                    CreateDeclareMergeOneManyWipIssueReturnPM(_Model, tf_target, NameTable, TargetTable, out SQLDeclare, out RecId);
                    _SQLDeclare = SQLDeclare;

                   
                    affectedRows = await Task.FromResult(_dapper.Execute(SQLDeclare));
                    if (affectedRows > 0)
                    {
                        ErrorLog.WriteToLog("returnPM", "[" + aDiJob + " " + aErrorKey + "] - Berhasil " + _SQLDeclare);
                        status = true;
                    }
                    else
                    {
                        ErrorLog.WriteToLog("returnPM", "[" + aDiJob + " " + aErrorKey + "] - Failed " + _SQLDeclare);
                        status = false;
                    }
                }
                #endregion 6
                else

                #region packaging secondary PM 7
                if (SubID == "7")
                {
                    //tblO_RawMaterialRequest_Additional, tblO_RawMaterialRequest_Additional_Detail
                    WipMaterialIssueSecondaryPM _Model = new WipMaterialIssueSecondaryPM();

                    _Model.RecId = SourceModel.RECID;
                    _Model.RecTimestamp = SourceModel.RECTIMESTAMP;
                    _Model.RecStatus = SourceModel.RECSTATUS;
                    _Model.Timestamp = SourceModel.TIMESTAMP;
                    _Model.Id = SourceModel.ID;
                    _Model.Type = SourceModel.TYPE;
                    _Model.Source = SourceModel.SOURCE;
                    _Model.OrganizationId = SourceModel.ORGANIZATION_ID;
                    _Model.PrintDate = SourceModel.PRINT_DATE;
                    _Model.UserPrinted = SourceModel.USER_PRINTED;
                    _Model.UpdateDateBBK = SourceModel.UPDATE_DATE_BBK;
                    _Model.TransactCreateDate = SourceModel.RECTIMESTAMP;
                    _Model.TransactUpdateDate = SourceModel.RECTIMESTAMP;
                    _Model.Status = 2;
                    _Model.Recipe = SourceModel.RECIPE;
                    _Model.BatchNumber = SourceModel.BATCH_NO;
                    _Model.WoNumber = SourceModel.SCHEDULE_NUMBER;
                    _Model.BatchNumber = SourceModel.BATCH_NO;
                    _Model.IsFirstTimeDownloading = true;


                    List<WipMaterialIssueSecondaryDetailPM> WIP_ISSUE_PPI_DETAIL_SCH = new List<WipMaterialIssueSecondaryDetailPM>();
                    if (SourceModel.WIP_ISSUE_PPI_DETAIL_SCH.Count > 0)
                    {
                        foreach (var i in SourceModel.WIP_ISSUE_PPI_DETAIL_SCH)
                        {
                            WipMaterialIssueSecondaryDetailPM detail = new WipMaterialIssueSecondaryDetailPM();
                            detail.RecId = (long)i.RECID;
                            detail.RecTimestamp = i.RECTIMESTAMP;
                            detail.RecStatus = (short?)i.RECSTATUS;
                            detail.TransactCreateDate = i.RECTIMESTAMP;
                            detail.TransactUpdateDate = i.RECTIMESTAMP;
                            detail.ItemCode = i.ITEM_CODE;
                            detail.ItemDescription = i.ITEM_DESCRIPTION;
                            detail.JumlahLot = i.JUMLAH_LOT;
                            detail.PlanQTY = (decimal?)i.PLAN_QTY;
                            detail.WipPlanQTY = (decimal?)i.WIP_PLAN_QTY;
                            detail.Quantity = i.QUANTITY;
                            detail.UOMSchedule = i.UOM_SCHEDULE;
                            detail.QuantityBatch = (decimal?)i.QUANTITY_BATCH;
                            detail.SumQtyBatch = i.SUM_QUANTITY_BATCH;
                            detail.Rounding = (int?)i.ROUNDING;
                            detail.ExpireDate = i.EXP_DATE;
                            detail.LPN = i.LPN_NO;
                            detail.ServiceWorker = i.SERVICE_WORKER;
                            detail.ScalingCode = i.SCALLING_CODE;
                            detail.Weigher = i.WEIGHER;
                            detail.Coordinator = i.COORDINATOR;
                            detail.HandoverTime = i.HANDOVER_TIME;
                            detail.CheckedByWhsSpv = i.CHECKED_BY_WHS_SPV;
                            detail.CheckedByProdCoor = i.CHECKED_BY_PROD_COOR;
                            detail.DeliveredBy = i.DELIVERED_BY;
                            detail.ReceivedBy = i.RECEIVED_BY;
                            detail.ItemType = "KEMAS";
                            detail.HeaderRecId = SourceModel.RECID;
                            detail.HeaderId = i.WIP_ISSUE_ID;

                            //detail.HeaderRecId = i.WIP_ISSUE_ID;
                            //detail. = i.WIP_ISSUE_ID;
                            //detail. = i.NO;
                            //detail. = i.BATCH_NO;
                            WIP_ISSUE_PPI_DETAIL_SCH.Add(detail);
                        }
                    }

                    _Model.WIP_ISSUE_PPI_DETAIL_SCH = WIP_ISSUE_PPI_DETAIL_SCH;

                    var NameTable = "@TablePackagingMaterialIssueSecondary";
                    var TargetTable = "tblO_PackagingMaterialIssue_Secondary";
                    string SQLDeclare;
                    string RecId;

                    WipMaterialIssueSecondaryPM Tg = new WipMaterialIssueSecondaryPM();
                    Type tf_target = Tg.GetType();
                    CreateDeclareMergeOneManyWipSecondaryPM(_Model, tf_target, NameTable, TargetTable, out SQLDeclare, out RecId);
                    _SQLDeclare = SQLDeclare;

                  
                    affectedRows = await Task.FromResult(_dapper.Execute(SQLDeclare));
                    if (affectedRows > 0)
                    {
                        ErrorLog.WriteToLog("returnPM", "[" + aDiJob + " " + aErrorKey + "] - Berhasil " + _SQLDeclare);
                        status = true;
                    }
                    else
                    {
                        ErrorLog.WriteToLog("returnPM", "[" + aDiJob + " " + aErrorKey + "] - Failed " + _SQLDeclare);
                        status = false;
                    }
                }
                #endregion


                if (status == true)
                {
                    result.success = true;
                    result.code = Constant.codeSuccess;
                    result.message = Constant.messageSuccess;
                    result.data = affectedRows;
                }


            }
            catch (Exception ex)
            {
                ErrorLog.WriteToLog(aErrorKey, ex.Message);
                result.success = false;
                result.code = Constant.codeFailed;
                result.message = ex.Message;
                result.data = ex.Message;
            }

           

            return result;
        }
        #endregion

        #region (WIP Issue Secondary PM
        private void CreateDeclareMergeOneManyWipSecondaryPM(WipMaterialIssueSecondaryPM SourceModel, Type tf_target, string DeclareTable, string TargetTable, out string SQLMerge, out string RecID)
        {

            var SQL = "";

            List<string> FieldsType = new List<string>();
            List<string> Fields = new List<string>();
            List<string> InsertValues = new List<string>();

            MergeSQL _ms = new MergeSQL();
            Type _st = SourceModel.GetType();

            FieldsType = _ms.GetFieldType(_st);
            Fields = _ms.GetFieldSQL(_st);

            String[] keys = new String[1];
            keys[0] = "@HeaderId";
            InsertValues = _ms.GetValueSQL(ref SourceModel, _st, out SQL, keys);

            var QueryRangeInsert = InsertValues.Where((value, index) => index >= 0 && index < InsertValues.Count).ToArray();
            var ValueNoRelasi = InsertValues.Where((value, index) => index >= 0 && index < InsertValues.Count).ToArray();

            var QueryInsert = string.Join(",", InsertValues.ToArray());
            var InsertNoRelasi = string.Join(",", QueryRangeInsert);

            var SQLHEADER = _ms.MergeSQLFormat(tf_target, FieldsType, Fields, DeclareTable, TargetTable, InsertNoRelasi, QueryInsert);

            RecID = InsertNoRelasi[0].ToString();
            SQLMerge = SQLHEADER + " " + SQL;
            Console.WriteLine(SQLMerge);

            FieldsType.Clear();
            Fields.Clear();
            InsertValues.Clear();

            ErrorLog.WriteToLog("AppException", "[" + aDiJob + " " + aErrorKey + "] - " + SQLMerge);

        }


        #endregion

    }
}
