﻿using MaterialService.API.Helpers;
using MaterialService.API.Services.Models;
using MaterialService.API.Services.RabbitMQ.Subscriber.Werum.SemiFinishedGoodTransfer.Model;
using System.Threading;
using System.Threading.Tasks;

namespace MaterialService.API.Services.RabbitMQ.Subscriber.Werum.SemiFinishedGoodTransfer.Interfaces
{
    public interface ISemiFinishedGoodTransfer
    {
        Task DoWork(CancellationToken stoppingToken);

        Task<BaseResponse> DidJob(EbridgingBppModel SourceModel);
    }
}
