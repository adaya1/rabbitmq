﻿using System;

namespace MaterialService.API.Services.RabbitMQ.Subscriber.Werum.SemiFinishedGoodTransfer.Model
{
    public class EbridgingBppDetail
    {
		public long TransactId { get; set; }
		public DateTime? TransactCreateDate { get; set; }
		public DateTime? TransactUpdateDate { get; set; }
		public long HeaderId { get; set; }
		public string Checker { get; set; }
		public string LineName { get; set; }
		public string WorkCenter { get; set; }
		public DateTime? PrintDate { get; set; }
		public int Number { get; set; }
		public string MasterBoxNumber { get; set; }
		public string LpnNumber { get; set; }
		public DateTime? ExpireDate { get; set; }
		public DateTime? ManufacturingDate { get; set; }
		public decimal MasterBoxQty { get; set; }
		public decimal CoreBoxQty { get; set; }
		public decimal PackagingQty { get; set; }
		public string PrimaryUomQty { get; set; }

	}
}
