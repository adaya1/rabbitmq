﻿using MaterialService.API.Services.RabbitMQ.Subscriber.Werum.MasterBox.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace MaterialService.API.Services.RabbitMQ.Subscriber.Werum.MasterBox
{
    public class MasterBoxPackaging : BackgroundService
    {
        private readonly ILogger<MasterBoxPackaging> _logger;
        public IServiceProvider _Services;

        public MasterBoxPackaging(IServiceProvider services, ILogger<MasterBoxPackaging> logger)
        {
            _logger = logger;
            _Services = services;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation(
                "Consume Scoped Service Hosted Service running.");

            await DoWork(stoppingToken);
        }

        private async Task DoWork(CancellationToken stoppingToken)
        {
            _logger.LogInformation(
                "Consume Scoped Service Hosted Service is working.");

            try
            {
                using (var scope = _Services.CreateScope())
                {
                    var scopedProcessingService =
                        scope.ServiceProvider
                            .GetRequiredService<IMasterBox>();

                    await scopedProcessingService.DoWork(stoppingToken);

                    scope.Dispose();
                }
            }
            catch
            {

            }

        }

        public override async Task StopAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation(
                "Consume Scoped Service Hosted Service is stopping.");

            await base.StopAsync(stoppingToken);
        }

    }
}