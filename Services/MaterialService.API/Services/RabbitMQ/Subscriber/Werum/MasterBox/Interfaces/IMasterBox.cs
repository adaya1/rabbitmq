﻿using MaterialService.API.Helpers;
using MaterialService.API.Services.RabbitMQ.Subscriber.Werum.MasterBox.Model;
using System.Threading;
using System.Threading.Tasks;

namespace MaterialService.API.Services.RabbitMQ.Subscriber.Werum.MasterBox.Interfaces
{
    public interface IMasterBox
    {
        Task DoWork(CancellationToken stoppingToken);

        Task<BaseResponse> DidJob(EbridgingFinishedGoodsCheckWeighing SourceModel);
    }
}
