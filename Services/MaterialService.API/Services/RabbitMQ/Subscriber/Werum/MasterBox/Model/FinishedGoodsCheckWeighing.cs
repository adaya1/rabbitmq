﻿using System;
using System.Collections.Generic;

namespace MaterialService.API.Services.RabbitMQ.Subscriber.Werum.MasterBox.Model
{
    public class FinishedGoodsCheckWeighing
    {
		public long TransactId { get; set; }
		public DateTime? TransactCreateDate { get; set; }
		public DateTime? TransactUpdateDate { get; set; }
		public long EbrId { get; set; }
		public string TimeStamp {get;set;}
		public string Id { get; set; }
		public string Type { get; set; }
		public string Source { get; set; }
		public string WoNumber { get; set; }
		public string MoWerum { get; set; }
		public decimal? LoLimit { get; set; }
		public decimal? HiLimit { get; set; }
		public string BatchNumber { get; set; }
		public string MbrNumber { get; set; }
		public string ProductCode { get; set; }
		public string ProductName { get; set; }
		public int? Reprint { get; set; }
		public short? LastApprovalSequence { get; set; }
		public bool IsDownloading { get; set; }
		public int? ProcessingMasterBoxId { get; set; }
		public DateTime? ExpireDate { get; set; }
		public int? ContentsPerCarton { get; set; }
		public int? MasterBoxRecipe { get; set; }
		public int? SecondaryPackRecipe { get; set; }
		public DateTime? ManufacturingDate { get; set; }
		public decimal? BatchSizeTeoritis { get; set; }
		public string OrganizationId { get; set; }
		public decimal? PerPieceAmount { get; set; }

		public List<FinishedGoodsCheckWeighingIngredient> FINISHED_GOODS_CHECK_WEIG_INGRED { get; set; }

	}

}
