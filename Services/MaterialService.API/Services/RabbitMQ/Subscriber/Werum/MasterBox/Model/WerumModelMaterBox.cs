﻿using System;

namespace MaterialService.API.Services.RabbitMQ.Subscriber.Werum.MasterBox.Model
{
    public class WerumModelMaterBox
    {
        public string MO_WERUM { get; set; }
        public string BO_ORACLE  { get; set; }
        public string LOT_NUMBER { get; set; }
        public int MB_ID { get; set; }
        public int PALLET_NO  { get; set; }
        public string COMPLETE { get; set; }
        public decimal MB_WEIGH { get; set; }
        public decimal MB_WEIGH_REVISED  { get; set; }
        public string UOM { get; set; }
        public int LOOSE_CB   { get; set; }
        public int LOOSE_UNT { get; set; } 
        public string WEIGHER  { get; set; }
        public string  PACKER  { get; set; }
        public string SCALE_ID  { get; set; }
        public DateTime? DATETIME { get; set; }
        public decimal LO_LIMIT  { get; set; }
        public decimal HI_LIMIT { get;  set; }
        public int MB_PER_PALLET { get; set; }
        public int CB_PER_MB  { get; set; }
        public int UNT_PER_CB { get; set; }
        public int MB_IPC  { get; set; }
        public int PCT_TOLL  { get; set; }
        public string CHECKER { get; set; }
        public string NAMA_LINE { get; set; }
        public string WORK_CENTER { get; set; }
        public string NO_BPP { get; set; }
        public string TGL_BPP { get; set; }
        public string KODE_PRODUK { get; set; }
        public string NAMA_PRODUK { get; set; }
        public string BATCH_NO { get; set; }
        public DateTime? EXP_DATE { get; set; }
        public DateTime? MFG_DATE { get; set; }
    }
}
