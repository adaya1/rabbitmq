﻿using System;

namespace MaterialService.API.Services.RabbitMQ.Subscriber.Werum.MasterBox.Model
{
    public class EbridgingFinishedGoodsCheckWeighingIngredient
    {
		public long TransactId { get; set; }
		public DateTime? TransactCreateDate { get; set; }
		public DateTime? TransactUpdateDate { get; set; }
		public long HeaderId { get; set; }
		public string MbNumber { get; set; }
		public int MbNumberChecked { get; set; }
		public string PalletNumber { get; set; }
		public string Complete { get; set; }
		public decimal MbWeigh { get; set; }
		public decimal MbWeighRevised { get; set; }
		public int LooseCb { get; set; }
		public int LooseUnit { get; set; }
		public string Checker { get; set; }
		public string Weigher { get; set; }
		public string Packer { get; set; }
		public string ScaleId { get; set; }
		public DateTime? WeighingDate { get; set; }
		public int MbPerPallet { get; set; }
		public int CbPerMb { get; set; }
		public int UnitPerCb { get; set; }
		public int MbIpc { get; set; }
		public int PctToll { get; set; }
		public int Reprint { get; set; }
		public decimal LoLimit { get; set; }
		public decimal HiLimit { get; set; }
		public string Description { get; set; }
		public int MbId { get; set; }
		public string Uom { get; set; }
		public string LotNumber { get; set; }
		public bool IsChecked { get; set; }
		public DateTime? IsCheckedDate { get; set; }
		public string IsCheckedBy { get; set; }
	}
}
