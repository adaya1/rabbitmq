﻿using System;
using System.Collections.Generic;

namespace MaterialService.API.Services.RabbitMQ.Subscriber.Werum.FinishedGoodTransfer.Model
{
    public class FBpp
    {
		public long TransactId { get; set; }
		public DateTime? TransactCreateDate { get; set; }
		public DateTime? TransactUpdateDate { get; set; }
		public long EbrId { get; set; }
		public string Id { get; set; }
		public string Type { get; set; }
		public string Source { get; set; }
		public string WoNumber { get; set; }
		public string MbrNumber { get; set; }
		public string BatchNumber { get; set; }
		public decimal BatchSizeTeoritis { get; set; }
		public string BatchSizeTeoritisUOM { get; set; }
		public string ProductCode { get; set; }
		public string ProductName { get; set; }
		public short ProductType { get; set; }
		public string Recipe { get; set; }
		public string RecipeVersion { get; set; }
		public string WorkCenter { get; set; }
		public string ProductLine { get; set; }
		public string BppNumber { get; set; }
		public DateTime? BppDate { get; set; }
		public DateTime? PrintDate { get; set; }
		public string ProductionSign { get; set; }
		public DateTime? ProductionSignDate { get; set; }
		public string ProductionSpvSign { get; set; }
		public DateTime? ProductionSpvSignDate { get; set; }
		public bool IsIntermediate { get; set; }
		public short LastApprovalSequence { get; set; }
		public bool IsDownloading { get; set; }
		public string OrganizationId { get; set; }
		public string RecipeSource { get; set; }


		public List<FBppDetail> BPP_DETAIL { get; set; }
	}

}
