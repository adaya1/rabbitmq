﻿using Master.Api.Helpers;
using MaterialService.API.Helpers;
using MaterialService.API.Helpers.Dapper;
using MaterialService.API.Services.RabbitMQ.Subscriber.Werum.FinishedGoodTransfer.Interfaces;
using MaterialService.API.Services.RabbitMQ.Subscriber.Werum.FinishedGoodTransfer.Model;
using Messagebus;
using MessageBus;
using Microsoft.Extensions.Logging;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Queue = Messagebus.Queue;

namespace MaterialService.API.Services.RabbitMQ.Subscriber.Werum.FinishedGoodTransfer.DALS
{
    public class FinishedGoodTransferProcessingService : IFinishedGoodTransfer
    {
        private readonly ILogger _logger;
        private readonly IBus _busControl;
        private readonly IDapper _dapper;

        private readonly string aErrorKey = "FinishedGoodTransfer-BPP";
        private readonly string aDoWork = "DoWork";
        private readonly string aDiJob = "DidJob";
        private string _SQLDeclare;
        private string _SQLMerge;
        private readonly int defaultReceiveDelay;

        public FinishedGoodTransferProcessingService(ILogger<FinishedGoodTransferProcessingService> logger, IDapper dapper)
        {
            _logger = logger;
            _busControl = RabbitHutch.CreateBus();
            _dapper = dapper;
            defaultReceiveDelay = 10000;
        }

        public async Task DoWork(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    //IBus _busControl = RabbitHutch.CreateBus();
                    //Console.WriteLine(Queue.MasterSpecificationValidityRule);
                    await _busControl.ReceiveAsync<EbridgingFBpp>(Queue.WerumFinishedGoodTransfer, x =>
                    {
                        try
                        {
                            _ = Task.Run(() => { _ = DidJob(x); }, stoppingToken);
                        }
                        catch (Exception ex)
                        {
                            ErrorLog.WriteToLog(aErrorKey, "[" + aDoWork + " " + aErrorKey + "] " + ex.Message + " " + x);
                        }
                    });
                }
                catch
                {
                    //Console.WriteLine(aDoWork + " " + aErrorKey);
                }
                finally
                {
                    //_busControl.Dispose();
                }

                await Task.Delay(defaultReceiveDelay, stoppingToken);
            }

            _busControl.Dispose();
        }


        public async Task<BaseResponse> DidJob(EbridgingFBpp SourceModel)
        {
            BaseResponse result = new BaseResponse();
            var _SQL = "";
            try
            {
                ErrorLog.WriteToLog(aErrorKey, Newtonsoft.Json.JsonConvert.SerializeObject(SourceModel));

                //tblO_Spec_ValRule_Header, tblO_Spec_ValRule_Detail
                var NameTable = "@TableBpp";
                var TargetTable = "tblW_BPP";
                string SQLDeclare;

                FBpp Tg = new FBpp();
                Type tf_target = Tg.GetType();
                CreateDeclareMergeOneMany(SourceModel, tf_target, NameTable, TargetTable, out SQLDeclare);
                _SQLDeclare = SQLDeclare;
                int affectedRows;

                // result.data = 
                affectedRows = await Task.FromResult(_dapper.Execute(SQLDeclare));
                if (affectedRows > 0)
                {
                    ErrorLog.WriteToLog(aErrorKey, "[" + aDiJob + " " + aErrorKey + "] - Berhasil " + _SQLDeclare);
                }
                else
                {
                    ErrorLog.WriteToLog(aErrorKey, "[" + aDiJob + " " + aErrorKey + "] - Failed " + _SQLDeclare);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteToLog(aErrorKey, "[" + aDiJob + " " + aErrorKey + "] " + ex.Message + " " + _SQL);
            }
            finally
            {
                _dapper.Dispose();
            }
            return result;
        }


        private void CreateDeclareMergeOneMany(EbridgingFBpp SourceModel, Type tf_target, string DeclareTable, string TargetTable, out string SQLMerge)
        {

            var SQLMasterRecipeStepMaterialAsociation = "";

            List<string> FieldsType = new List<string>();
            List<string> Fields = new List<string>();
            List<string> InsertValues = new List<string>();

            MergeSQL _ms = new MergeSQL();
            Type _st = SourceModel.GetType();

            FieldsType = _ms.GetFieldType(_st);
            Fields = _ms.GetFieldSQL(_st);

            String[] keys = new String[1];
            keys[0] = "";
            InsertValues = _ms.GetValueSQL(ref SourceModel, _st, out SQLMasterRecipeStepMaterialAsociation, keys);

            var QueryRangeInsert = InsertValues.Where((value, index) => index >= 0 && index < InsertValues.Count).ToArray();
            var ValueNoRelasi = InsertValues.Where((value, index) => index >= 0 && index < InsertValues.Count).ToArray();

            var QueryInsert = string.Join(",", InsertValues.ToArray());
            var InsertNoRelasi = string.Join(",", QueryRangeInsert);

            var SQLRECIPEHEADER = _ms.MergeSQLFormat(tf_target, FieldsType, Fields, DeclareTable, TargetTable, InsertNoRelasi, QueryInsert);
            ErrorLog.WriteToLog(aErrorKey, "header:" + SQLRECIPEHEADER);

            SQLMerge = SQLRECIPEHEADER + " " + SQLMasterRecipeStepMaterialAsociation;

            FieldsType.Clear();
            Fields.Clear();
            InsertValues.Clear();

            ErrorLog.WriteToLog(aErrorKey, "[" + aDiJob + " " + aErrorKey + "] - " + SQLMerge);
        }

    }


    //private readonly ILogger _logger;
    //private readonly IBus _busControl;
    //private readonly IDapper _dapper;


    //private readonly string aErrorKey = "MasterSpecificationValRule";
    //private readonly string aDoWork = "DoWork";
    //private readonly string aDiJob = "DidJob";
    //private string _SQLDeclare;
    //private string _SQLMerge;

    //private readonly int defaultReceiveDelay;

    //public MasterSpecificationValRuleProcessingService(ILogger<MasterSpecificationValRuleProcessingService> logger, IDapper dapper)
    //{
    //    _logger = logger;
    //    _busControl = RabbitHutch.CreateBus();
    //    _dapper = dapper;
    //    defaultReceiveDelay = 10000;
    //}

    //public async Task DoWork(CancellationToken stoppingToken)
    //{

    //    while (!stoppingToken.IsCancellationRequested)
    //    {
    //        try
    //        {
    //            //IBus _busControl = RabbitHutch.CreateBus();
    //            //Console.WriteLine(Queue.MasterSpecificationValidityRule);
    //            await _busControl.ReceiveAsync<EbridgingMasterSpecificationValRuleHeader>(Queue.MasterSpecificationValidityRule, x =>
    //            {
    //                try
    //                {
    //                    _ = Task.Run(() => { _ = DidJob(x); }, stoppingToken);
    //                }
    //                catch (Exception ex)
    //                {
    //                    ErrorLog.WriteToLog(aErrorKey, "[" + aDoWork + " " + aErrorKey + "] " + ex.Message + " " + x);
    //                }
    //            });
    //        }
    //        catch
    //        {
    //            //Console.WriteLine(aDoWork + " " + aErrorKey);
    //        }
    //        finally
    //        {
    //            //_busControl.Dispose();
    //        }

    //        await Task.Delay(defaultReceiveDelay, stoppingToken);
    //    }

    //    _busControl.Dispose();
    //}

    //public async Task<BaseResponse> DidJob(EbridgingMasterSpecificationValRuleHeader SourceModel)
    //{
    //    BaseResponse result = new BaseResponse();
    //    var _SQL = "";
    //    try
    //    {
    //        ErrorLog.WriteToLog(aErrorKey, Newtonsoft.Json.JsonConvert.SerializeObject(SourceModel));

    //        //tblO_Spec_ValRule_Header, tblO_Spec_ValRule_Detail
    //        var NameTable = "@TableMasterSpecificationValRuleHeader";
    //        var TargetTable = "tblO_Spec_ValRule_Header";
    //        string SQLDeclare;

    //        MasterSpecificationValRuleHeader Tg = new MasterSpecificationValRuleHeader();
    //        Type tf_target = Tg.GetType();
    //        CreateDeclareMergeOneMany(SourceModel, tf_target, NameTable, TargetTable, out SQLDeclare);
    //        _SQLDeclare = SQLDeclare;
    //        int affectedRows;

    //        // result.data = 
    //        affectedRows = await Task.FromResult(_dapper.Execute(SQLDeclare));
    //        if (affectedRows > 0)
    //        {
    //            ErrorLog.WriteToLog(aErrorKey, "[" + aDiJob + " " + aErrorKey + "] - Berhasil " + _SQLDeclare);
    //        }
    //        else
    //        {
    //            ErrorLog.WriteToLog(aErrorKey, "[" + aDiJob + " " + aErrorKey + "] - Failed " + _SQLDeclare);
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        ErrorLog.WriteToLog(aErrorKey, "[" + aDiJob + " " + aErrorKey + "] " + ex.Message + " " + _SQL);
    //    }
    //    finally
    //    {
    //        _dapper.Dispose();
    //    }
    //    return result;
    //}

    //private void CreateDeclareMergeOneMany(EbridgingMasterSpecificationValRuleHeader SourceModel, Type tf_target, string DeclareTable, string TargetTable, out string SQLMerge)
    //{

    //    var SQLMasterRecipeStepMaterialAsociation = "";

    //    List<string> FieldsType = new List<string>();
    //    List<string> Fields = new List<string>();
    //    List<string> InsertValues = new List<string>();

    //    MergeSQL _ms = new MergeSQL();
    //    Type _st = SourceModel.GetType();

    //    FieldsType = _ms.GetFieldType(_st);
    //    Fields = _ms.GetFieldSQL(_st);
    //    InsertValues = _ms.GetValueSQL(ref SourceModel, _st, out SQLMasterRecipeStepMaterialAsociation);

    //    var QueryRangeInsert = InsertValues.Where((value, index) => index >= 0 && index < InsertValues.Count).ToArray();
    //    var ValueNoRelasi = InsertValues.Where((value, index) => index >= 0 && index < InsertValues.Count).ToArray();

    //    var QueryInsert = string.Join(",", InsertValues.ToArray());
    //    var InsertNoRelasi = string.Join(",", QueryRangeInsert);

    //    var SQLRECIPEHEADER = _ms.MergeSQLFormat(tf_target, FieldsType, Fields, DeclareTable, TargetTable, InsertNoRelasi, QueryInsert);
    //    ErrorLog.WriteToLog(aErrorKey, "header:" + SQLRECIPEHEADER);

    //    SQLMerge = SQLRECIPEHEADER + " " + SQLMasterRecipeStepMaterialAsociation;

    //    FieldsType.Clear();
    //    Fields.Clear();
    //    InsertValues.Clear();

    //    ErrorLog.WriteToLog(aErrorKey, "[" + aDiJob + " " + aErrorKey + "] - " + SQLMerge);
    //}
}
