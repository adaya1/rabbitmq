﻿using MaterialService.API.Services.RabbitMQ.Subscriber.Werum.FinishedGoodTransfer.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace MaterialService.API.Services.RabbitMQ.Subscriber.Werum.FinishedGoodTransfer
{
    public class FinishedGoodTransferPackaging : BackgroundService
    {
        private readonly ILogger<FinishedGoodTransferPackaging> _logger;
        public IServiceProvider _Services;

        public FinishedGoodTransferPackaging(IServiceProvider services, ILogger<FinishedGoodTransferPackaging> logger)
        {
            _logger = logger;
            _Services = services;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation(
                "Consume Scoped Service Hosted Service running.");

            await DoWork(stoppingToken);
        }

        private async Task DoWork(CancellationToken stoppingToken)
        {
            _logger.LogInformation(
                "Consume Scoped Service Hosted Service is working.");

            try
            {
                using (var scope = _Services.CreateScope())
                {
                    var scopedProcessingService =
                        scope.ServiceProvider
                            .GetRequiredService<IFinishedGoodTransfer>();

                    await scopedProcessingService.DoWork(stoppingToken);

                    scope.Dispose();
                }
            }
            catch
            {

            }

        }

        public override async Task StopAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation(
                "Consume Scoped Service Hosted Service is stopping.");

            await base.StopAsync(stoppingToken);
        }

    }
}