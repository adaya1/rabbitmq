﻿using MaterialService.API.Helpers;
using MaterialService.API.Services.RabbitMQ.Subscriber.Werum.FinishedGoodTransfer.Model;
using System.Threading;
using System.Threading.Tasks;

namespace MaterialService.API.Services.RabbitMQ.Subscriber.Werum.FinishedGoodTransfer.Interfaces
{
    public interface IFinishedGoodTransfer
    {
        Task DoWork(CancellationToken stoppingToken);

        Task<BaseResponse> DidJob(EbridgingFBpp SourceModel);
    }
}
