﻿namespace MaterialService.API.Extensions
{
    public static class DecimalExtension
    {
        public static string ToDefaultDecimalFormat(this decimal value)
        {
            return value.ToString("N2");
        }

        public static string ToDefaultCurrency(this decimal currency)
        {
            return currency.ToString("#,##0.00");
        }
    }
}
