﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MaterialService.API.Extensions
{
    public static class ExceptionExtension
    {
        public static bool IsRefusingConnectionException(this Exception exception)
        {
            return exception.Message == "No connection could be made because the target machine actively refused it.";
        }

        public static string FormatMessage(this Exception exception)
        {
            //return $"Message: {exception.Message} <br /> Stack Trace : {exception.StackTrace}";
            return exception.Message;
        }
    }
}
