﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

namespace MaterialService.API.Extensions
{
    public static class ObjectExtension
    {
        public static T Clone<T>(this T obj)
        {
            return (T)JsonConvert.DeserializeObject(JsonConvert.SerializeObject(obj));
        }

        public static T DeepCopy<T>(this T obj) where T : class
        {
            if (obj == null)
                return null;

            Type type = obj.GetType();

            // If the type is a value type or string, return the object itself
            if (type.IsValueType || type == typeof(string))
                return obj;

            // If the type is an array, create a new array and deep copy each element
            if (type.IsArray)
            {
                var array = obj as Array;
                var copiedArray = Array.CreateInstance(type.GetElementType(), array.Length);
                for (int i = 0; i < array.Length; i++)
                {
                    copiedArray.SetValue(DeepCopy(array.GetValue(i)), i);
                }
                return (T)Convert.ChangeType(copiedArray, type);
            }

            // If the type is a class or a list, create a new instance and deep copy each property or element
            if (type.IsClass || type.IsGenericType)
            {
                T copiedObj = (T)Activator.CreateInstance(type);
                PropertyInfo[] properties = type.GetProperties();

                if (properties != null)
                {
                    for (int i = 0; i < properties.Length; i++)
                    {
                        PropertyInfo property = properties[i];
                        if (property.CanRead && property.CanWrite)
                        {
                            var propertyValue = property.GetValue(obj);
                            if (propertyValue != null)
                            {
                                if (property.PropertyType.IsGenericType && property.PropertyType.GetGenericTypeDefinition() == typeof(List<>))
                                {
                                    var list = propertyValue as IList;
                                    if (list != null)
                                    {
                                        IList copiedList = (IList)Activator.CreateInstance(property.PropertyType);
                                        foreach (var item in list)
                                        {
                                            copiedList.Add(DeepCopy(item));
                                        }
                                        property.SetValue(copiedObj, copiedList);
                                    }
                                }
                                else
                                {
                                    property.SetValue(copiedObj, DeepCopy(propertyValue));
                                }
                            }
                        }
                    }
                }
                return copiedObj;
            }

            throw new ArgumentException("Unsupported type: " + type.Name);
        }
    }
}
