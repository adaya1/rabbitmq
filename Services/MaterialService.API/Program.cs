using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MaterialService.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }


        public static IHostBuilder CreateHostBuilder(string[] args) =>
                     Host.CreateDefaultBuilder(args)
                         .ConfigureWebHostDefaults(webBuilder =>
                         {
#if DEBUG
                             //var baseurl = System.Environment.GetEnvironmentVariable("EBRSettings:ApplicationUrl");
                             //webBuilder.UseStartup<Startup>().UseUrls(baseurl);
                             webBuilder.UseContentRoot(Directory.GetCurrentDirectory())
                             .UseIISIntegration()
                             .UseStartup<Startup>();
#else
                                //var url = Environment.GetEnvironmentVariable("EBRSettings:ApplicationUrl");
                                webBuilder.UseContentRoot(Directory.GetCurrentDirectory())
                                .UseIISIntegration()
                                .UseStartup<Startup>();
#endif

                         });
    }
}
