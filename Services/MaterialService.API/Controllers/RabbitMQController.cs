﻿using MaterialService.Api.Services.RabbitMQ.Publishers.JobProcess.Interfaces;
using MaterialService.Api.Services.RabbitMQ.Publishers.SendJobList.Models;
using MaterialService.API.Helpers;
using MaterialService.API.Services.Models.Base;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace MaterialService.API.Controllers
{
    public class RabbitMQController : Controller
    {
        private readonly IJobProcess mainInterface;

        public RabbitMQController(IJobProcess _mainInterface)
        {
            mainInterface = _mainInterface;
        }

        [HttpPost]
        [Route("Publisher-Test")]
        public async Task<IActionResult> PublisherTest([FromBody] BppBaseModel args)
        {
            try
            {
                return Ok(await mainInterface.PublishTest(args));
            }
            catch (Exception ex)
            {
                return BadRequest(new BaseResponse()
                {
                    success = false,
                    code = Constant.codeFailed,
                    message = ex.Message,
                    row = 0,
                    data = null
                });
            }
        }
    }
}
