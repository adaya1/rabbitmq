using ServiceDiscovery.Config;
using HealthChecks.UI.Client;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using MaterialService.API.Helpers.Database;
using MaterialService.API.Middlewares;
using System;
using MaterialService.API.Helpers.Dapper;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Http;
using MaterialService.API.Services.RabbitMQ.Subscriber.Oracle.Wip.Interfaces;
using MaterialService.API.Services.RabbitMQ.Subscriber.Oracle.Wip.DALS;
using MaterialService.API.Services.RabbitMQ.Subscriber.Oracle.Wip;


namespace MaterialService.API
{
    public class Startup
    {
        private string version = "";

        public Startup(IConfiguration configuration)
        {
            DateTime dt = DateTime.Now;
            Console.WriteLine("Start :  " + dt.ToString());
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            Console.WriteLine(version);
            
            // Redis Configuration
            services.AddStackExchangeRedisCache(options =>
            {
                options.Configuration = Configuration.GetValue<string>("CacheSettings:Redis.Host");
            });

            services.AddControllers();
            services.AddControllers().AddNewtonsoftJson(options =>
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            );

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "RABBITMQ", Version = version });
                c.AddSecurityDefinition("Bearer",
                new OpenApiSecurityScheme
                {
                    Description = "Untuk authorization header nya menggunakan bearer token dan untuk keperluan development atau testing bearer token didapat dengan mengexecute API DUMMYAUTH tapi untuk production token nya di dapat dari ONEKALBE HEADER. Untuk penggunaan di swagger di kolom value Contoh: 'Bearer tokenAnda'",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            }
                        },
                        Array.Empty<string>()
                    }
                });
            });

            services.AddHealthChecks()
                    .AddRedis(Configuration["CacheSettings:Redis.Host"], "Redis Health", HealthStatus.Degraded);

            services.AddCors(options =>
            {
                options.AddPolicy("ServicePolicy",
                    builder => builder.WithOrigins(FsSecurityLoader.GetAllowedOrigin())
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials()
                    .SetIsOriginAllowedToAllowWildcardSubdomains());
            });

            services.AddRouting(r => r.SuppressCheckForUnhandledSecurityMetadata = true);
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Latest);
            services.AddConsulConfig(Configuration);
            services.AddSingleton<IDapper, Dapperr>();

            // publisher
            //services.addsingleton<isendreportmaterialadditional, sendreportmaterialadditionaldal>();//publishing : ipublishing
            //services.addsingleton<isendreport, sendreportdal>();//publishing : ipublishing
            //services.addsingleton<isendreportbpp, sendreportbppdal>();//publishing : ipublishing
            //services.addsingleton<isendreportreturn, sendreportreturndal>();//publishing : ipublishing
            //services.addsingleton<isendreportmaterialissuepm, sendreportmaterialissuepmdal>();//publishing : ipublishing
            //services.addsingleton<isendreportmaterialissuerm, sendreportmaterialissuermdal>();//publishing : ipublishing
            //services.addsingleton<isendreportmasterbox, sendreportmasterboxdal>();//publishing : ipublishing
            //services.addsingleton<isendreportmaterialproducthandoverkemas, sendreportmaterialproducthandoverkemasdal>();//publishing : ipublishing
            //services.addsingleton<isendreportmaterialproducthandoverolah, sendreportmaterialproducthandoverolahdal>();//publishing : ipublishing

            // Consumer
            services.AddHostedService<WipPackaging>();
            services.AddScoped<IWip, WipProcessingServices>();

        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            app.UsePathBase(PathString.Empty);
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
#if DEBUG
            app.UseSwagger();
#else

            var isHttp = Environment.GetEnvironmentVariable("Scheme:isHttp");
            var isHttps = Environment.GetEnvironmentVariable("Scheme:isHttps");

            app.UseSwagger(c =>
            {
                List<OpenApiServer> a = new List<OpenApiServer>();// { new OpenApiServer { Url = $"{httpReq.Scheme}://{httpReq.Host.Value}{basePath}" } }
                a.Add(new OpenApiServer { Url = $"{isHttp}" });
                a.Add(new OpenApiServer { Url = $"{isHttps}" });
                c.PreSerializeFilters.Add((swagger, httpReq) =>
                {
                    swagger.Servers = a;
                });
            });
#endif


            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "RABBIT MQ v1"));

            app.UseMiddleware<SecurityMiddleware>();

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors("ServicePolicy");

            app.Use((context, next) =>
            {
                context.Items["__CorsMiddlewareInvoked"] = true;
                return next();
            });

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHealthChecks("/hc", new HealthCheckOptions()
                {
                    Predicate = _ => true,
                    ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
                });
            });

            //app.UseConsul(Configuration);
        }
    }
}
