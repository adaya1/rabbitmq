﻿using System.ComponentModel;

namespace MaterialService.API.Enums
{
    public enum PackagingMaterialRequestReturnTypeEnum : short
    {
        [Description("n/a")]
        NotAvailable = 0,

        [Description("PS")]
        PS = 1,

        [Description("PD")]
        PD = 2
    }
    public enum PackagingMaterialRequestAdditionalTypeEnum : short
    {
        [Description("n/a")]
        NotAvailable = 0,

        [Description("PS")]
        PS = 1,

        [Description("PD")]
        PD = 2
    }
}
