﻿using System.ComponentModel;

namespace MaterialService.API.Enums
{
    public enum MaterialIssueStatusEnum : short
    {
        [Description("n/a")]
        NotAvailable,

        [Description("Downloading")]
        Downloading,

        [Description("Pending")]
        Pending,

        [Description("Approved")]
        Approved
    }
}
