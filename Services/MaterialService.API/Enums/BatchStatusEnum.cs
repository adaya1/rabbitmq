﻿using System.ComponentModel;

namespace MaterialService.API.Enums
{
    public enum BatchStatusEnum : short
    {
        [Description("Cancelled Batch")]
        Cancelled = -1,

        [Description("Pending Batch")]
        Pending = 1,

        [Description("WIP Batch")]
        WIP = 2,

        [Description("Completed Batch")]
        Completed = 3,

        [Description("Closed Batch")]
        Closed = 4
    }
}
