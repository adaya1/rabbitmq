﻿using System.ComponentModel;

namespace MaterialService.API.Enums
{
    public enum StatusEnum : short
    {
        [Description("Pending")]
        Pending = 0,

        [Description("Approved by PPIC")]
        ApprovedByPPIC = 1,

        [Description("Need QA Manager Approval")]
        NeedQAManagerApproval = 2,

        [Description("Approved by QA")]
        ApprovedByQA = 3,

        [Description("Approved by QA Manager")]
        ApprovedByQAManager = 4,

        [Description("Distributed")]
        Distributed = 5,

        [Description("Rejected")]
        Rejected = 6,

        [Description("Cancelled")]
        Cancelled = 7,

        [Description("Repopulate Needed")]
        RepopulateNeeded = 8
    }
}
