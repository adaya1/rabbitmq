﻿using System.ComponentModel;

namespace MaterialService.API.Enums
{
    public enum BppProductTypeEnum : short
    {
        [Description("Pending")]
        Default,

        [Description("Kimia")]
        Kimia,

        [Description("Wadah")]
        Wadah,

        [Description("Kemas")]
        Kemas,

        [Description("Intermediate")]
        Intermediate,

        [Description("Ruahan")]
        Ruahan,

        [Description("Produk")]
        Produk,

        [Description("Lain")]
        Lain
    }
}
