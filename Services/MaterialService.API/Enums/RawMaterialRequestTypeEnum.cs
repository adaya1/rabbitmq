﻿using System.ComponentModel;

namespace MaterialService.API.Enums
{
    public enum RawMaterialRequestReturnTypeEnum : short
    {
        [Description("n/a")]
        NotAvailable = 0,

        [Description("PS")]
        PS = 1,

        [Description("PD")]
        PD = 2
    }
    public enum RawMaterialRequestAdditionalTypeEnum : short
    {
        [Description("n/a")]
        NotAvailable = 0,

        [Description("PS")]
        PS = 1,

        [Description("PD")]
        PD = 2
    }
}
