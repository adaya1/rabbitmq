﻿using System.ComponentModel;

namespace MaterialService.API.Enums
{
    public enum RawMaterialRequestAdditionalStatusEnum : short
    {
        [Description("Draft")]
        Draft = 1,

        [Description("Submitted")]
        Submitted = 2,

        [Description("Approved")]
        Approved = 3,

        [Description("Rejected")]
        Rejected = 4
    }

    public enum RawMaterialRequestReturnStatusEnum : short
    {
        [Description("Draft")]
        Draft = 1,

        [Description("Submitting")]
        Submitting = 2,

        [Description("Awaiting Approval")]
        AwaitingApproval = 3,

        [Description("Approved")]
        Approved = 4,

        [Description("Rejected")]
        Rejected = 5,

        [Description("Reset")]
        Reset = 6
    }
}
