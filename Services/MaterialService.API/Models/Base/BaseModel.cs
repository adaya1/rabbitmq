﻿using System;

namespace MaterialService.API.Services.Models.Base
{
    public class BaseModel
    {
        public virtual long? RecId { get; set; }
        public virtual DateTime? RecTimestamp { get; set; }
        public virtual short? RecStatus { get; set; }
    }
}