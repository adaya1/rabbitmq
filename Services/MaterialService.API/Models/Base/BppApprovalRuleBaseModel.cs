﻿using System;

namespace MaterialService.API.Services.Models.Base
{
    public class BppApprovalRuleBaseModel
    {
        public int TransactId { get; set; }
        public DateTime? TransactCreateDate { get; set; }
        public DateTime? TransactUpdateDate { get; set; }
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public short Sequence { get; set; }
    }
}
