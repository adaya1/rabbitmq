﻿using System;

namespace MaterialService.API.Services.Models.Base
{
	public class FinishedGoodsIssueBaseModel : TransactBaseModel
	{
		public long? RecId { get; set; }
		public DateTime? RecTimestamp { get; set; }
		public short? RecStatus { get; set; }
		public string Type { get; set; }
		public string Source { get; set; }
		public string Id { get; set; }
		public long? EbrId { get; set; }
		public string WoNumber { get; set; }
		public string BatchNumber { get; set; }
		public string ProductCode { get; set; }
		public string ProductName { get; set; }
		public int? OrganizationId { get; set; }
		public DateTime? UpdateDateBBK { get; set; }
		public string Recipe { get; set; }
		public DateTime? PrintDate { get; set; }
		public string UserPrinted { get; set; }
		public string Timestamp { get; set; }
		public short? LastApprovalSequence { get; set; }
		public short Status { get; set; }
		public bool? IsFirstTimeDownloading { get; set; }
	}
}
