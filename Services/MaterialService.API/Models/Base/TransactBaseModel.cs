﻿using System;

namespace MaterialService.API.Services.Models.Base
{
    public class TransactBaseModel
    {
        public virtual long TransactId { get; set; }
        public virtual DateTime TransactCreateDate { get; set; }
        public virtual DateTime? TransactUpdateDate { get; set; }
    }
}
