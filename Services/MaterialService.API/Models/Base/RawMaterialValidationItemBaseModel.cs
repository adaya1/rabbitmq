﻿using System;
using Newtonsoft.Json;

using MaterialService.API.Services.Models;

namespace MaterialService.API.Services.Models.Base
{
    public class RawMaterialValidationItemBaseModel
	{
		
		[JsonProperty("BUNDLE_ID")]
		public long? BundleId { get; set; }

		[JsonProperty("BO_ORACLE")]
		public string WoNumber { get; set; }

		[JsonProperty("MO_WERUM")]
		public string MoWerum { get; set; }

		[JsonProperty("SFO_WERUM")]
		public int? SfoWerum { get; set; }

		[JsonProperty("OP_CLASS")]
		public string OpClass { get; set; }

		[JsonProperty("MATERIAL_CODE")]
		public string MaterialCode { get; set; }

		[JsonProperty("MATERIAL_NAME")]
		public string MaterialName { get; set; }

		[JsonProperty("BOM_SEQ")]
		public int? BomSeq { get; set; }

		[JsonProperty("SUB_LOT")]
		public int? SubLot { get; set; }

		[JsonProperty("DISP_STEP")]
		public int? DispStep { get; set; }

		[JsonProperty("LOT_NUMBER")]
		public string LotNumber { get; set; }

		[JsonProperty("WEIGHER")]
		public string Weigher { get; set; }

		[JsonProperty("WEIGHING_DATE")]
		public DateTime? WeighingDate { get; set; }

		[JsonProperty("VAL1_CHECKER")]
		public string ValidationChecker1 { get; set; }

		[JsonProperty("VAL1_DATE")]
		public DateTime? ValidationCheckDate1 { get; set; }

		[JsonProperty("VAL2_CHECKER")]
		public string ValidationChecker2 { get; set; }

		[JsonProperty("VAL2_DATE")]
		public DateTime? ValidationCheckDate2 { get; set; }

        [JsonProperty("QUANTITY")]
        public decimal? Quantity { get; set; }

        [JsonProperty("UOM")]
        public string Uom { get; set; }

        
	}
}
