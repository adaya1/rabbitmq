﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MaterialService.API.Services.Models.Base
{
    public class BppDetailBaseModel : TransactBaseModel
    {
		public virtual long? HeaderId { get; set; }
		public virtual string Checker { get; set; }
		public virtual string LineName { get; set; }
		public virtual string WorkCenter { get; set; }
		public virtual DateTime? PrintDate { get; set; }
		public virtual int? Number { get; set; }
		public virtual string MasterBoxNumber { get; set; }
		public virtual string LpnNumber { get; set; }
		public virtual DateTime? ExpireDate { get; set; }
		public virtual DateTime? ManufacturingDate { get; set; }
		public virtual decimal? MasterBoxQty { get; set; }
		public virtual decimal? CoreBoxQty { get; set; }
		public virtual decimal? PackagingQty { get; set; }
		public virtual string PrimaryUomQty { get; set; }
	}
}
