﻿using System;

namespace MaterialService.API.Services.Models.Base
{
	public class PackagingMaterialIssueDetailBaseModel : TransactBaseModel
	{
		public long? RecId { get; set; }
		public DateTime? RecTimestamp { get; set; }
		public short? RecStatus { get; set; }
		public long? HeaderId { get; set; }
		public string ItemCode { get; set; }
		public string ItemDescription { get; set; }
		public string ItemType { get; set; }
		public int? JumlahLot { get; set; }
		public decimal? PlanQTY { get; set; }
		public decimal? WipPlanQTY { get; set; }
		public decimal? Quantity { get; set; }
		public string UOMSchedule { get; set; }
		public string LotNumber { get; set; }
		public decimal? QuantityBatch { get; set; }
		public int? Rounding { get; set; }
		public DateTime? ExpireDate { get; set; }
		public string LPN { get; set; }
		public string ServiceWorker { get; set; }
		public string ScalingCode { get; set; }
		public string Weigher { get; set; }
		public string Coordinator { get; set; }
		public DateTime? HandoverTime { get; set; }
		public string OprProdCheck { get; set; }
		public string DeliveredBy { get; set; }
		public DateTime? DeliveredDate { get; set; }
		public string ReceivedBy { get; set; }
		public decimal? StandardUsage { get; set; }
		public string CheckedByProdCoor { get; set; }
		public string CheckedByWhsSpv { get; set; }
		public int? No { get; set; }
		public decimal? SumQtyBatch { get; set; }
	}
}