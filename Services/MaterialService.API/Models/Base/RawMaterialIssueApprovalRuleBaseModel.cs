﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MaterialService.API.Services.Models.Base
{
    public class RawMaterialIssueApprovalRuleBaseModel
    {
        public int TransactId { get; set; }
        public DateTime? TransactCreateDate { get; set; }
        public DateTime? TransactUpdateDate { get; set; }
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public short Sequence { get; set; }
    }
}
