﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MaterialService.API.Services.Models.Base
{
    public class BppApprovalBaseModel : TransactBaseModel
	{
		public long HeaderId { get; set; }
		public int ApprovalRuleId { get; set; }
		public long? UserId { get; set; }
		public string UserFullName { get; set; }
		public DateTime? ApproveDate { get; set; }
	}
}
