﻿namespace MaterialService.API.Services.Models.Base
{
	public class PackagingMaterialRequestReturnDetailBaseModel : TransactBaseModel
	{
		public virtual long HeaderId { get; set; }
		public virtual string MaterialCode { get; set; }
		public virtual string MaterialName { get; set; }
		public virtual decimal? LotQc { get; set; }
		public virtual decimal? Quantity { get; set; }
		public virtual string Uom { get; set; }
		public virtual short? ReturnType { get; set; }
		public virtual decimal? UsageStandardPerBatch { get; set; }
		public virtual string UsageStandardPerBatchUom { get; set; }
		public virtual decimal? PercentageToStandardPerBatch { get; set; }
		public virtual string Description { get; set; }
		public virtual bool? IsDeleted { get; set; }
		public virtual string SubInventory { get; set; }
	}

	public class PackagingMaterialRequestAdditionalDetailBaseModel : TransactBaseModel
	{
		public virtual long HeaderId { get; set; }
		public virtual string MaterialCode { get; set; }
		public virtual string MaterialName { get; set; }
		public virtual decimal? Quantity { get; set; }
		public virtual string Uom { get; set; }
		public virtual short? AdditionalType { get; set; }
		public virtual decimal? UsageStandardQty { get; set; }
		public virtual string UsageStandardUom { get; set; }
		public virtual string Description { get; set; }
		public virtual bool? IsDeleted { get; set; }
	}
}