﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MaterialService.API.Services.Models.Base
{
	public class FinishedGoodsCheckWeighingBaseModel : TransactBaseModel
	{
		public virtual long EbrId { get; set; }
		public virtual string TimeStamp { get; set; }
		public virtual string Id { get; set; }
		public virtual string Type { get; set; }
		public virtual string Source { get; set; }
		public virtual string WoNumber { get; set; }
		public virtual string MoWerum { get; set; }
		public virtual decimal? LoLimit { get; set; }
		public virtual decimal? HiLimit { get; set; }
		public virtual string BatchNumber { get; set; }
		public virtual string MbrNumber { get; set; }
		public virtual string ProductCode { get; set; }
		public virtual string ProductName { get; set; }
		public virtual decimal? BatchSizeTeoritis { get; set; }
		public virtual int? MasterBoxRecipe { get; set; }
		public virtual int? SecondaryPackRecipe { get; set; }
		public virtual DateTime? ExpireDate { get; set; }
		public virtual DateTime? ManufacturingDate { get; set; }
		public virtual int? Reprint { get; set; }
		public virtual short? LastApprovalSequence { get; set; }
		public virtual bool? IsDownloading { get; set; }
		public virtual long? ProcessingMasterBoxId { get; set; }
		public virtual int? ContentsPerCarton { get; set; }
        public virtual decimal? PerPieceAmount { get; set; }
    }

	public class FinishedGoodsCheckWeighingIngredientBaseModel : TransactBaseModel
	{
		public virtual long HeaderId { get; set; }
		public virtual string MbNumber { get; set; }
		public virtual int? MbNumberChecked { get; set; }
		public virtual string PalletNumber { get; set; }
		public virtual string Complete { get; set; }
		public virtual decimal? MbWeigh { get; set; }
		public virtual decimal? MbWeighRevised { get; set; }
		public virtual int? LooseCb { get; set; }
		public virtual int? LooseUnit { get; set; }
		public virtual string Checker { get; set; }
		public virtual string Weigher { get; set; }
		public virtual string Packer { get; set; }
		public virtual string ScaleId { get; set; }
		public virtual DateTime? WeighingDate { get; set; }
		public virtual int? MbPerPallet { get; set; }
		public virtual int? CbPerMb { get; set; }
		public virtual int? UnitPerCb { get; set; }
		public virtual int? MbIpc { get; set; }
		public virtual int? PctToll { get; set; }
		public virtual int? Reprint { get; set; }
		public virtual decimal? LoLimit { get; set; }
		public virtual decimal? HiLimit { get; set; }
		public virtual string Description { get; set; }
		public virtual string LotNumber { get; set; }
        public virtual bool IsChecked { get; set; }
        public virtual DateTime IsCheckedDate { get; set; }
        public virtual string IsCheckedBy { get; set; }
    }

	public class FinishedGoodsCheckWeighingApprovalRuleBaseModel
	{
		public virtual int TransactId { get; set; }
		public virtual DateTime TransactCreateDate { get; set; }
		public virtual DateTime? TransactUpdateDate { get; set; }
		public virtual int RoleId { get; set; }
		public virtual string RoleName { get; set; }
		public virtual short Sequence { get; set; }
	}

	public class FinishedGoodsCheckWeighingApprovalBaseModel
	{
		public virtual int TransactId { get; set; }
		public virtual DateTime TransactCreateDate { get; set; }
		public virtual DateTime? TransactUpdateDate { get; set; }
		public virtual long HeaderId { get; set; }
		public virtual int ApprovalRuleId { get; set; }
		public virtual int? UserId { get; set; }
		public virtual string UserFullName { get; set; }
		public virtual DateTime? ApproveDate { get; set; }
	}
}