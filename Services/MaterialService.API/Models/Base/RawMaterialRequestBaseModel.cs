﻿using System;

namespace MaterialService.API.Services.Models.Base
{
	public class RawMaterialRequestReturnBaseModel : TransactBaseModel
	{
		public virtual long EbrId { get; set; }
		public virtual string WoNumber { get; set; }
		public virtual string DeviationNumber { get; set; }
		public virtual string ProductCode { get; set; }
		public virtual string ProductName { get; set; }
		public virtual string DestinationWarehouse { get; set; }
		public virtual string Description { get; set; }
		public virtual short? LastApprovalSequence { get; set; }
		public virtual bool? IsDownloading { get; set; }
		public virtual short? Status { get; set; }
		public virtual DateTime? RejectedDate { get; set; }
		public virtual int? RejectedByUserId { get; set; }
		public virtual string RejectedByUserFullName { get; set; }
		public virtual int? RejectOpenedByUserId { get; set; }
		public virtual string RejectOpenedByUserFullName { get; set; }
		public string BatchNumber { get; set; }
		public string MbrNumber { get; set; }
		public string OrgId { get; set; }
		public string RecipeSource { get; set; }
		public string StepOrSubstep { get; set; }

    }

	public class RawMaterialRequestAdditionalBaseModel : TransactBaseModel
	{
		public virtual long EbrId { get; set; }
		public virtual string WoNumber { get; set; }
		public virtual string DeviationNumber { get; set; }
		public virtual string ProductCode { get; set; }
		public virtual string ProductName { get; set; }
		public virtual string DestinationWarehouse { get; set; }
		public virtual string Description { get; set; }
		public virtual short? LastApprovalSequence { get; set; }
		public virtual bool? IsDownloading { get; set; }
		public virtual short? Status { get; set; }
		public virtual DateTime? RejectedDate { get; set; }
		public virtual int? RejectedByUserId { get; set; }
		public virtual string RejectedByUserFullName { get; set; }
		public virtual bool? HasAdditionalMaterial { get; set; }
		public string BatchNumber { get; set; }
		public string Type { get; set; }
		public virtual string StepOrSubstep { get; set; }
		public virtual bool MaterialFromGiver { get; set; }
		public string MbrNumber { get; set; }
		public decimal? BatchSizeTeoritis { get; set; }
		public string BatchSizeUOM { get; set; }
        public string OrgId { get; set; }
        public string RecipeSource { get; set; }
    }
}
