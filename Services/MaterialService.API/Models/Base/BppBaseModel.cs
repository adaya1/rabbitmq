﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MaterialService.API.Services.Models.Base
{
    public class BppBaseModel : TransactBaseModel
	{
		public virtual string Message { get; set; }
    }
}
