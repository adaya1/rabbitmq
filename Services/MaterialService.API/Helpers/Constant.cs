﻿namespace MaterialService.API.Helpers
{
    public class Constant
    {
        public const string messageSuccess = "Berhasil";
        public const string messageFailed = "Gagal";
        public const string messageExist = "Data sudah ada";
        public const string messageNotExist = "Data tidak ditemukan";
        public const string codeSuccess = "1";
        public const string codeFailed = "-1";
        public const string codeDataNotValid = "422 ";
        public const string messageDataNotValid = "Data tidak valid";

        public const string createData = "Create";
        public const string updateData = "Update";
        public const string deleteData = "Delete";
        public const string errorUpdate = "Error Update";

        public const string invalidTypeRequest = "Invalid request type!";

        public const string SuccessSaveMessage = "Berhasil disimpan";
        public const string SuccessSubmitMessage = "Berhasil disubmit";
        public const string SuccessDeleteMessage = "Berhasil dihapus";
        public const string SuccessGenerateMessage = "Berhasil generate";
        public const string SuccessApproveMessage = "Berhasil approve data";

        public const string FailedSaveMessage = "Gagal disimpan";
        public const string FailedSubmitMessage = "Gagal disubmit";
        public const string FailedDeleteMessage = "Gagal dihapus";

        public const string NotAvailable = "n/a";
    }
}