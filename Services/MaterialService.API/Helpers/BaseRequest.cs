﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MaterialService.API.Helpers
{
    public class BaseRequest
    {
        [Required]
        public string user_name { get; set; }
        [Required]
        public string organization_id { get; set; }
        [Required]
        public string type { get; set; }
    }
}
