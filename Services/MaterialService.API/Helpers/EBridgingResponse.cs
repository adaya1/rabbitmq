﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MaterialService.API.Helpers
{
    public class EBridgingResponse
    {
        public string Message { get; set; }
        public string Code { get; set; }

        [JsonProperty("info")]
        public ResponseInfo Info { get; set; }

        [JsonProperty("result")]
        public object Result { get; set; }

        public class ResponseInfo
        {
            [JsonProperty("timestamp")]
            public long Timestamp { get; set; }

            [JsonProperty("id")]
            public long Id { get; set; }

            [JsonProperty("type")]
            public string Type { get; set; }

            [JsonProperty("source")]
            public string Source { get; set; }

        }
    }
}
