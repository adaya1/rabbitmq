﻿namespace MaterialService.API.Helpers
{
    public class BaseResponse
    {
        public bool success { get; set; } 
        public string code { get; set; } 
        public string message { get; set; } 
        public int row { get; set; }
        public object data { get; set; }
    }

    public class BaseResponseWIP
    {
        public bool success { get; set; }
        public string Code { get; set; }
        public string TrxTimestamp { get; set; }
        public string TrxMessage_Id { get; set; }
        public string TrxType { get; set; }
        public string Satellite { get; set; }
        public string ItemKey { get; set; }
        public string UserName { get; set; }
        public object Log { get; set; }

    }
}
