﻿namespace MaterialService.API.Helpers
{
    public class OracleBaseResponse
    {
        public string Message { get; set; } 
        public string code { get; set; } 
        public object Info { get; set; }
        public object result { get; set; }

    }
}
