﻿namespace MaterialService.API.Helpers
{
    public enum TableEnum
    {
        job_title,
        roles,
        role_access,
        organization_id,
        system_settings,

        user_group_inv_penyimpangan,
        user_group_lap_penyimpangan,
        user_group_ma,
        user_group_mwqt,        
        user_group_spec,
        user_group_wqt,
        user_group_protokol_stabilita,
        user_group_inv_stabilita,

        pic_wqt,
        pic_wqt_stabilita,
        pic_wqt_pereaksi
    }

}
