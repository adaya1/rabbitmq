﻿using Microsoft.Extensions.Configuration;
using System;

namespace MaterialService.API.Helpers
{
    public class EnvAppConfiguration
    {
        private readonly IConfiguration configuration;

        public EnvAppConfiguration(IConfiguration _configuration)
        {
            configuration = _configuration;
        }

        public string serviceWerum()
        {
#if DEBUG
            string Url = configuration["UrlService:Werum"];
#else
            string Url = Environment.GetEnvironmentVariable("UrlService:Werum");
#endif
            return Url;
        }


        public string serviceSampleWip()
        {
#if DEBUG
            string Url = configuration["UrlService:SampleWIP"];
#else
            string Url = Environment.GetEnvironmentVariable("UrlService:SampleWIP");
#endif
            return Url;
        }

        public string serviceMaster()
        {

#if DEBUG
            string Url = configuration["UrlService:Master"];
#else
            string Url = Environment.GetEnvironmentVariable("UrlService:Master");
#endif
            return Url;
        }

        public string serviceMBR()
        {
#if DEBUG
            string Url = configuration["UrlService:MBR"];
#else
            string Url = Environment.GetEnvironmentVariable("UrlService:MBR");
#endif
            return Url;
        }

        public string serviceBatchHeader()
        {
#if DEBUG
            string Url = configuration["UrlService:BatchHeader"];
#else
            string Url = Environment.GetEnvironmentVariable("UrlService:BatchHeader");
#endif
            return Url;
        }

        public string serviceMaterial()
        {
#if DEBUG
            string Url = configuration["UrlService:Material"];
#else
            string Url = Environment.GetEnvironmentVariable("UrlService:Material");
#endif
            return Url;
        }

        public string serviceEBR()
        {
#if DEBUG
            string Url = configuration["UrlService:EBR"];
#else
            string Url = Environment.GetEnvironmentVariable("UrlService:EBR");
#endif
            return Url;
        }

        public string serviceDeviation()
        {
#if DEBUG
            string Url = configuration["UrlService:Deviation"];
#else
            string Url = Environment.GetEnvironmentVariable("UrlService:Deviation");
#endif
            return Url;
        }

        public string serviceSampling()
        {
#if DEBUG
            string Url = configuration["UrlService:Sampling"];
#else
            string Url = Environment.GetEnvironmentVariable("UrlService:Sampling");
#endif
            return Url;
        }

        public string serviceReport()
        {
#if DEBUG
            string Url = configuration["UrlService:Report"];
#else
            string Url = Environment.GetEnvironmentVariable("UrlService:Report");
#endif
            return Url;
        }

        public string serviceEbriding()
        {
#if DEBUG
            string Url = configuration["UrlService:Ebriding"];
#else
            string Url = Environment.GetEnvironmentVariable("UrlService:Ebriding");
#endif
            return Url;
        }


        public string serviceLogin()
        {
#if DEBUG
            string Url = configuration["EBRSettings:UrlLogin"];
#else
            string Url = Environment.GetEnvironmentVariable("EBRSettings:UrlLogin");
#endif
            return Url;
        }

        public string serviceWebsiteSLUG()
        {
#if DEBUG
            string Url = configuration["EBRSettings:WebsiteSLUG"];
#else
            string Url = Environment.GetEnvironmentVariable("EBRSettings:WebsiteSLUG");
#endif
            return Url;
        }

        public string serviceAllowCors()
        {
#if DEBUG
            string AllowCors = configuration["ServicePolicy:AllowedOrigin"];
#else
            string AllowCors = Environment.GetEnvironmentVariable("ServicePolicy:AllowedOrigin");
#endif
            return AllowCors;
        }

        public string serviceAllowHost()
        {
#if DEBUG
            string AllowHost = configuration.GetValue(typeof(string), "AllowedHosts") as string;
#else
            string AllowHost = Environment.GetEnvironmentVariable("ServicePolicy:AllowedHosts");
#endif
            return AllowHost;
        }

        public string serviceOracleAppKey()
        {
#if DEBUG
            string AppKey = configuration["Oracle:AppKey"];
#else
            string AppKey = Environment.GetEnvironmentVariable("Oracle:AppKey");
#endif
            return AppKey;
        }

        public string serviceEbridgingAppKey()
        {
#if DEBUG
            string AppKey = configuration["Ebridging:AppKey"];
#else
            string AppKey = Environment.GetEnvironmentVariable("Ebridging:AppKey");
#endif
            return AppKey;
        }


        public string serviceAuditTrail()
        {
#if DEBUG
            string Url = configuration["UrlService:AuditTrail"];
#else
            string Url = Environment.GetEnvironmentVariable("UrlService:AuditTrail");
#endif
            return Url;
        }


        public string serviceApiKeyEBR()
        {
#if DEBUG
            string Url = configuration["AppKeys:EBR"];
#else
            string Url = Environment.GetEnvironmentVariable("AppKeys:EBR");
#endif
            return Url;
        }

        public string serviceDummyDataModeRMValidationItem()
        {
#if DEBUG
            string Url = configuration["DummyDataMode:RMValidationItem"];
#else
            string Url = Environment.GetEnvironmentVariable("DummyDataMode:RMValidationItem");
#endif
            return Url;
        }

    }
}
