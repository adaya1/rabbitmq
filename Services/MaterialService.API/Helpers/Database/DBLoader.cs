﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.FileProviders;
using System;
using System.IO;

namespace MaterialService.API.Helpers.Database
{
    public class DBLoader
    {
        public static string GetConnstring()
        {

#if DEBUG
            var FileProvider = new PhysicalFileProvider(
                Path.Combine(Directory.GetCurrentDirectory(), "")).Root;

            var MysqlConnstring = new ConfigurationBuilder().AddJsonFile(FileProvider + "appsettings.json").Build().GetSection("ConnectionStrings");

            return MysqlConnstring["Database.Host"];

#else
              // ENV
            string Server = Environment.GetEnvironmentVariable("MST_DB_SERVER");
            string Db = Environment.GetEnvironmentVariable("MST_DB_NAME");
            string User = Environment.GetEnvironmentVariable("MST_DB_USER");
            string Password = Environment.GetEnvironmentVariable("MST_DB_PASS");
            string Connstring = $@"Data Source={Server};Initial Catalog={Db};Persist Security Info=True;User ID={User};Password={Password};TrustServerCertificate=True";
            Console.WriteLine("Connstring" + Connstring);
            return Connstring;


#endif


        }
    }
}
