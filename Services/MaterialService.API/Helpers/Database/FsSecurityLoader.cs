﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.FileProviders;
using System;
using System.IO;

namespace MaterialService.API.Helpers.Database
{
    public class FsSecurityLoader
    {
        public static string[] GetAllowedOrigin()
        {


#if DEBUG
            var FileProvider = new PhysicalFileProvider(
                Path.Combine(Directory.GetCurrentDirectory(), "")).Root;

            var FsKeyEndtoEnd = new ConfigurationBuilder().AddJsonFile(FileProvider + "appsettings.json").Build().GetSection("ServicePolicy");
            string[] allowedOrigin = FsKeyEndtoEnd["AllowedOrigin"].Split(',');
            return allowedOrigin;
#else
            var Origin = Environment.GetEnvironmentVariable("ServicePolicy:AllowedOrigin");
            var OriginMaster = Environment.GetEnvironmentVariable("ServicePolicy:AllowedOriginMaster");
            var OriginMBR = Environment.GetEnvironmentVariable("ServicePolicy:AllowedOriginMBR");
            var OriginMaterial = Environment.GetEnvironmentVariable("ServicePolicy:AllowedOriginMaterial");
            var OriginBatchHeader = Environment.GetEnvironmentVariable("ServicePolicy:AllowedOriginBatchHeader");
            var OriginEbr = Environment.GetEnvironmentVariable("ServicePolicy:AllowedOriginEBR");
            var OriginDeviation = Environment.GetEnvironmentVariable("ServicePolicy:AllowedOriginDeviation");
            var OriginSampling = Environment.GetEnvironmentVariable("ServicePolicy:AllowedOriginSampling");
            var OriginReport = Environment.GetEnvironmentVariable("ServicePolicy:AllowedOriginReport");

            var FsKeyEndtoEnd = Environment.GetEnvironmentVariable("ServicePolicy:AllowedOrigin");
            //var x = FsKeyEndtoEnd.Replace('"', ' ');

            var del = ",";
            string x = string.Concat(OriginMaster, del, OriginMBR, del, OriginMaterial, del, OriginBatchHeader, del, OriginEbr, del, OriginDeviation, del, OriginSampling, del, OriginReport);

            string[] allowedOrigin = x.Split(',');
            return allowedOrigin;
#endif
        }
    }
}