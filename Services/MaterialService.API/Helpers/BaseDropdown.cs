﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MaterialService.API.Helpers
{
    public class BaseDropdown
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
