﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;

namespace MaterialService.API.Helpers
{
    public class Utility
    {        
        public static DataTable GetDataTableFromObjects(object[] objects)
        {
            if (objects != null && objects.Length > 0)
            {
                Type t = objects[0].GetType();
                DataTable dt = new DataTable(t.Name);
                foreach (PropertyInfo pi in t.GetProperties())
                {
                    dt.Columns.Add(new DataColumn(pi.Name));
                }
                foreach (var o in objects)
                {
                    DataRow dr = dt.NewRow();
                    foreach (DataColumn dc in dt.Columns)
                    {
                        dr[dc.ColumnName] = o.GetType().GetProperty(dc.ColumnName).GetValue(o, null);
                    }
                    dt.Rows.Add(dr);
                }
                return dt;
            }
            return null;
        }

        public static DataTable ListToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        public static T ConvertObject<T>(object obj)
        {
            if (obj == null)
                return default(T);

            T targetInstance = (T)Activator.CreateInstance(typeof(T));

            List<PropertyInfo> objPropertyInfos = obj.GetType().GetProperties().ToList();
            for (int i = 0; i < objPropertyInfos.Count; i++)
            {
                PropertyInfo objProp = objPropertyInfos[i];

                List<PropertyInfo> targetPropertyInfos = targetInstance.GetType().GetProperties().ToList();
                for (int j = 0; j < targetPropertyInfos.Count; j++)
                {
                    PropertyInfo targetProp = targetPropertyInfos[j];

                    if (objProp.Name == targetProp.Name && objProp.GetType() == targetProp.GetType())
                    {
                        if (targetProp.CanWrite)
                            targetProp.SetValue(targetInstance, objProp.GetValue(obj, new object[] { }));
                    }
                }
            }

            return targetInstance;
        }

        public static string GetRandomInitialName()
        {
            string[] initialNames = new string[10] { "AAA", "ABA", "ACA", "BAD", "BAC", "PAA", "CAC", "CCA", "STA", "MPP" };

            Random random = new Random();
            return initialNames[random.Next(0, 9)];
        }
    }
}
