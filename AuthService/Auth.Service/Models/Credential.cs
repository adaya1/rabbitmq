﻿using LiteDB;

namespace Auth.Service.Models
{
    public class Credential
    {
        [BsonId]
        public int id { get; set; }
        public int IdAkun { get; set; } = 1;
        public string Token { get; set; }
    }
}
