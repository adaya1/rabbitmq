﻿namespace Auth.Service.Models
{
    public class DummyAkun
    {
        public int IdAkun { get; set; } = 1;
        public string Email { get; set; } = "dummy@gmail.com";
        public string Token { get; set; }
    }
}
