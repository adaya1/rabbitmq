﻿using LiteDB;
using Microsoft.IdentityModel.Tokens;
using Auth.Service.Helpers.Database;
using Auth.Service.Models;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace Mwqt.API.Middlewares
{
    public class WebToken
    {
        public string GenerateJSONWebToken(DummyAkun userInfo, string Key, string Issuer)
        {
            try
            {
                var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Key));
                var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

                var claims = new[] {
                    new Claim(JwtRegisteredClaimNames.Sub, userInfo.IdAkun.ToString()),
                    new Claim(JwtRegisteredClaimNames.Email, userInfo.Email),
                    new Claim("ID", userInfo.IdAkun.ToString()),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                };

                var token = new JwtSecurityToken(Issuer,
                    Issuer,
                    claims,
                    expires: DateTime.Now.AddHours(18),
                    signingCredentials: credentials);

                var getToken = new JwtSecurityTokenHandler().WriteToken(token);

                Credential payload = new Credential();
                payload.Token = getToken;

                this.UpdateToken(payload);

                return getToken;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        public bool UpdateToken(Credential payload)
        {
            var connectionString = new ConnectionString(@"" + DbLocalInitial.configPath + DbLocalInitial.DbName)
            {
                Password = DbLocalInitial.DbPass,
                Connection = ConnectionType.Shared
            };

            using (var db = new LiteDatabase(connectionString))
            {
                var col = db.GetCollection<Credential>("security_system");

                var isExit = col.Find(x => x.IdAkun == payload.IdAkun).FirstOrDefault();

                if (isExit != null)
                {
                    isExit.IdAkun = payload.IdAkun;
                    isExit.Token = payload.Token;
                    
                    col.Update(isExit);

                    return true;
                }
                else
                {
                    var NewData = new Credential
                    {
                        IdAkun = payload.IdAkun,
                        Token = payload.Token
                    };

                    col.Insert(NewData);

                    return true;
                }
            }
        }
    }
}
