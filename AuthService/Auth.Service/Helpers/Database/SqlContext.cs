﻿using System.Data;

namespace Auth.Service.Helpers.Database
{
    public class SqlContext
    {
        private EDbConnectionTypes _dbType;
        public string ConnectionString { get; set; }

        public SqlContext()
        {
            string connectionString = DBLoader.GetConnstring();
            this.ConnectionString = connectionString;
        }

        public IDbConnection GetConnection()
        {
            return DbConnectionFactory.GetDbConnection(_dbType, ConnectionString);
        }
    }
}
