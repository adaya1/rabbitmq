﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.FileProviders;
using System.IO;

namespace Auth.Service.Helpers.Database
{
    public class FsSecurityLoader
    {
        public static string[] GetAllowedOrigin()
        {
            var FileProvider = new PhysicalFileProvider(
                Path.Combine(Directory.GetCurrentDirectory(), "")).Root;

            var FsKeyEndtoEnd = new ConfigurationBuilder().AddJsonFile(FileProvider + "appsettings.json").Build().GetSection("ServicePolicy");

            string[] allowedOrigin = FsKeyEndtoEnd["AllowedOrigin"].Split(',');

            return allowedOrigin;
        }
    }
}
