﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.FileProviders;
using System.IO;

namespace Auth.Service.Helpers.Database
{
    public class DBLoader
    {
        public static string GetConnstring()
        {
            var FileProvider = new PhysicalFileProvider(
                Path.Combine(Directory.GetCurrentDirectory(), "")).Root;

            var MysqlConnstring = new ConfigurationBuilder().AddJsonFile(FileProvider + "appsettings.json").Build().GetSection("ConnectionStrings");

            return MysqlConnstring["Database.Host"];
        }
    }
}
