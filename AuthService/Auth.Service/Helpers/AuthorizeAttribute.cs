﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Auth.Service.Models;
using System;

namespace Auth.Service.Helpers
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class AuthorizeAttribute : Attribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var user = (Credential)context.HttpContext.Items["User"];
            if (user == null || string.IsNullOrEmpty(user.Token))
            {
                context.Result = new JsonResult(new BaseResponse()
                {
                    success = false,
                    code = "99",
                    message = "Maaf, anda tidak memiliki akses.",
                    row = 0,
                    data = null
                })
                { StatusCode = StatusCodes.Status401Unauthorized };
            }
        }
    }
}
