﻿namespace Auth.Service.Helpers
{
    public class BaseResponse
    {
        public bool success { get; set; }
        public string code { get; set; }
        public string message { get; set; }
        public int row { get; set; }
        public object data { get; set; }
    }
}
