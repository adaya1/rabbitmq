﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Messagebus
{
    public class Queue
    {
        public static string Processing { get; } = "StartProcessingQueue";


        // JOBLIST
        //public static string JobListDone { get; } = "Ebridging-M-JobList-Done";

        // WIP Oracle
        public static string WIPIssue { get; } = "Ebridging-WIP-Issue";

        public static string ListActualMaterial { get; } = "Ebridging-List-Actual-Material";

        // werum
        public static string WerumFinishedGoodTransfer { get; } = "Werum-FinishedGoodTransfer";

        public static string WerumMasterBox { get; } = "Werum-MasterBox";

        public static string WerumMaterialValidasi { get; } = "Werum-MaterialValidasi";

        public static string WerumSemiFinishedGoodTransfer { get; } = "Werum-SemiFinishedGoodTransfer";
        public static string OracleWIPIssue { get; set; }
    }
}
