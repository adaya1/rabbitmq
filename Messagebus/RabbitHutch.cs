﻿using Messagebus;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MessageBus
{
    public class RabbitHutch
    {
        private static ConnectionFactory _factory;
        private static IConnection _connection;
        private static IModel _channel;

        ///ENV Configuration
        //public void Dispose()
        //{
        //    _channel.Close();
        //    _connection.Close();
        //    _channel.Dispose();
        //    _connection = null;
        //}

        public static IBus CreateBus()
        {
#if DEBUG
            try
            {



                _factory = new ConnectionFactory
                {
                    HostName = "45.76.183.12",
                    //Port = 5672,
                    VirtualHost = "/",
                    UserName = "aspdev",
                    Password = "aspdev@#2021",
                    DispatchConsumersAsync = true
                };
                _factory.ClientProvidedName = "app:EBR.SERVICE";
                _connection = _factory.CreateConnection();
                _channel = _connection.CreateModel();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : " + e);
            }
            finally
            {
                //_connection.Dispose();
                //_channel?.Dispose();
            }

            if (_connection != null)
            {
                Console.WriteLine("RBMQ Open : True");
                //_connection.Dispose();
                //_channel?.Dispose();
            }
            else
            {
                Console.WriteLine("RBMQ Open : False");
            }
#else
            string _Server = Environment.GetEnvironmentVariable("MST_RABBIT_HOST");
            string _Port = Environment.GetEnvironmentVariable("MST_RABBIT_PORT");
            string _UserName = Environment.GetEnvironmentVariable("MST_RABBIT_USER");
            string _Password = Environment.GetEnvironmentVariable("MST_RABBIT_PASS");

            try
            {

                _factory = new ConnectionFactory
                {
                    HostName = _Server,
                    Port = int.Parse(_Port),
                    VirtualHost = "/",
                    UserName = _UserName,
                    Password = _Password,
                    DispatchConsumersAsync = true
                };
                _factory.ClientProvidedName = "app:MATERIAL.API";
                _connection = _factory.CreateConnection();
                _channel = _connection.CreateModel();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : " + e);
            }

            if (_connection != null)
            {
                // Console.WriteLine("RBMQ Open : True");
            }
            else
            {
                // Console.WriteLine("RBMQ Open : False");
            }
#endif

            return new RabbitBus(_channel, _connection);
        }
    }
}
