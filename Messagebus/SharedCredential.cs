﻿using System;

namespace Messagebus
{
    public class SharedCredential
    {
        public string IdAkun { get; set; } = "1";
        public string Token { get; set; }
        public DateTime expiredTime { get; set; }

    }
}
