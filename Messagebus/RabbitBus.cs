﻿using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Messagebus
{
    public class RabbitBus : IBus
    {
        private readonly IModel _channel;
        private readonly IConnection _connection;


        public RabbitBus(IModel channel, IConnection connection)
        {
            _channel = channel;
            _connection = connection;
        }

        public async Task SendAsync<T>(string queue, T message)
        {
            await Task.Run(() =>
            {
                _channel.QueueDeclare(queue, true, false, false);

                var properties = _channel.CreateBasicProperties();
                properties.Persistent = false;

                var output = JsonConvert.SerializeObject(message);
                _channel.BasicPublish(string.Empty, queue, null, Encoding.UTF8.GetBytes(output));
                //_channel.Dispose();
            });
        }

        public Task ReceiveAsync<T>(string queue, Action<T> onMessage)
        {
            try
            {
                if (_channel != null) {
                    _channel.QueueDeclare(queue, true, false, false);
                    var consumer = new AsyncEventingBasicConsumer(_channel);
                    consumer.Received += async (s, e) =>
                    {
                        try
                        {
                            string jsonSpecified = Encoding.UTF8.GetString(e.Body.Span);
                            object readData = JsonConvert.DeserializeObject(jsonSpecified);
                            Console.WriteLine("rd:" + readData.ToString());
                            TModel printer = new TModel();
                            var data = printer.Print(readData);
                            string jsonText = data.ToString();
                        // var jsonText = JsonConvert.SerializeObject(data, Formatting.Indented);
                        Console.WriteLine(jsonText);
                            var item = JsonConvert.DeserializeObject<T>(jsonText);

                            onMessage(item);

                            await Task.Yield();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }

                    };
                    var tag = _channel.BasicConsume(queue, true, "", true, false, new Dictionary<string, object>(), consumer);
                    _channel.BasicCancel(tag);
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _channel.Close();
            _connection.Close();
        }
    }
}

