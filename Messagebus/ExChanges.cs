﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Messagebus
{
    public class ExChanges
    {
        // Publisher MBR to Master.API
        // public static string MbrFlaggingRecipeWorkOut { get; } = "_MBR-FlaggingRecipeWorkOut";s
        // public static string MbrFlaggingRecipePackaging { get; } = "_MBR-FlaggingRecipePackaging";


        public static string _EbridgingMaterialServiceBpp { get; } = "Ebridging-MTRL-R-BppApprove";

        // FinishedGoodsCheckWeighing
        public static string _EbridgingFinishedGoodsCheckWeighing { get; } = "Ebridging-MTRL-R-MasterBox";

        // Raw Material Issue
        public static string _EbridgingRawMaterialIssue_ApproveItem { get; } = "Ebridging-MTRL-R-RawMaterialIssueRM-ApproveItem";

        public static string _EbridgingRawMaterialIssue_ApproveAdditional { get; } = "Ebridging-MTRL-R-RawMaterialIssueRM-ApproveAddiitonal";

        public static string _EbridgingRawMaterialIssue_ApproveReturn { get; } = "Ebridging-MTRL-R-RawMaterialIssueRM-ApproveReturn";

        // Raw Material Request
        public static string _EbridgingRawMaterialRequest_ApproveAdditional { get; } = "Ebridging-MTRL-R-RawMaterialRequestRM-ApproveAddiitonal";

        public static string _EbridgingRawMaterialRequest_ApproveReturn { get; } = "Ebridging-MTRL-R-RawMaterialRequestRM-ApproveReturn";

        // pakaging material request
        public static string _EbridgingPackagingMaterialRequest_ApproveReturn { get; } = "Ebridging-MTRL-R-PackagingMaterialRequestPM-ApproveReturn";

        public static string _EbridgingPackagingMaterialRequest_ApproveAdditional { get; } = "Ebridging-MTRL-R-PackagingMaterialRequestPM-ApproveAdditional";

        // pakaging material issue
        public static string _EbridgingPackagingMaterialIssue_AppoveItem { get; } = "Ebridging-MTRL-R-PackagingMaterialIssuePM-ApproveItem";
        
        public static string _EbridgingPackagingMaterialIssue_AppoveSecondary { get; } = "Ebridging-MTRL-R-PackagingMaterialIssuePM-AppoveSecondary";
        
        public static string _EbridgingPackagingMaterialIssue_ApproveAdditional { get; } = "Ebridging-MTRL-R-PackagingMaterialIssuePM-ApproveAdditional";
        
        public static string _EbridgingPackagingMaterialIssue_ApproveReturn { get; } = "Ebridging-MTRL-R-PackagingMaterialIssuePM-ApproveReturn";

        public static string _EbridgingPackagingMaterialProductHandoverKemas { get; } = "Ebridging-MTRL-R-MaterialProductHandoverKemas";

        public static string _EbridgingRawMaterialProductHandoverOlah { get; } = "Ebridging-MTRL-R-MaterialProductHandoverOlah";

        //public static string _EbridgingPackagingMaterialRequest_ApproveAdditional { get; } = "Ebridging-EBR-R-ReturnMaterial";

        //public static string _EbridgingRawMaterialRequest_ApproveReturn { get; } = "Ebridging-EBR-R-ReturnMaterial";

        // public static string _EbridgingRawMaterialRequest_ApproveAdditional { get; } = "Ebridging-EBR-R-AdditionalMaterial";

        //  public static string _EbridgingRawMaterialRequest { get; } = "Ebridging-EBR-R-ReturnMaterial";

        //  public static string _EbridgingRawRackagingMaterialReturn { get; } = "Ebridging-MTRL-RawRackagingMaterial-ApproveReturn";

        // public static string _EbridgingRawPackagingMaterialAdditional { get; } = "Ebridging-EBR-R-AdditionalMaterial";

        //public static string _EbridgingRawmaterialIssueAdditionalItemReturn { get; } = "Ebridging-EBR-R-ReturnMaterialIssueRM";

        // public static string _EbridgingRawmaterialIssueAdditionalItemReturnSec { get; } = "Ebridging-EBR-R-ReturnMaterialIssuePM";

        //public static string _EbridgingRawmaterialIssueApproveItem { get; } = "Ebridging-MTRL-MaterialIssue-ApprovalItem";

        //public static string _EbridgingRawmaterialHandOverOlah { get; } = "Ebridging-MTRL-MaterialIssue-HandOverOlah";

        //public static string _EbridgingRawmaterialHandOverKemas { get; } = "Ebridging-MTRL-MaterialIssue-HandOverKemas";

        // REPORt
        public static string ApproveReport { get; } = "Ebridging-MBR-R-Apporve";

        // JOBLIST
        public static string MaterialJobListCreate { get; } = "Ebridging-M-JobList-Create";

        public static string MaterialJobListDone { get; } = "Ebridging-M-JobList-Done";

        // DASHBOARD
        public static string DashboardLeadTimePeyiapanKemasan { get; } = "Ebridging-EBR-D-LeadTimePeyiapanKemasan";

        public static string DashboardLeadTimeBPP { get; } = "Ebridging-EBR-D-LeadTimeBPPOlah";

        public static string DashboardLeadTimeHoldingTimeVal1 { get; } = "Ebridging-EBR-D-HoldingTimeVal1";

        public static string DashboardLeadTimeHoldingTimeVal2 { get; } = "Ebridging-EBR-D-HoldingTimeVal2";

        public static string DashboardLeadTimeProcessingStartDateOlah { get; } = "Ebridging-EBR-D-LeadTimeProsesStartDateOlah";

        public static string DashboardLeadTimeProcessingEndDateKemas { get; } = "Ebridging-EBR-D-LeadTimeProsesEndDateKemas";

      

    }
}
